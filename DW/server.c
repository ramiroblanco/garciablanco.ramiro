#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#define PROLE 3
#define VIDA 10
#define CHECK 1

typedef struct tarea
{
    int proc;
    int init;
    
}TAREA;

TAREA hijos [PROLE];
int numchld=0;

static void reapChild(int sig) { /* the handler. sig is unused. */
    int i,pid;
    pid= waitpid(-1, NULL, 0); /* wait for any one child and reap it */
  
    for (i=0;i<PROLE;i++)
    {
        if (hijos[i].proc==pid)
            hijos[i].proc=0;
    }
    numchld--;
 
  
}

static void handALRM (int sig) { /* the handler. sig is unused. */
  
    int i=0;
    for (i=0;i<PROLE;i++)
    {
        if (hijos[i].proc!=0)
            if((time(NULL)-hijos[i].init )> (VIDA))
            {
                printf("kill\n");
                kill (hijos[i].proc,SIGKILL);
            }
    }
    alarm (CHECK);
    
    
    
    
}

int main()
{
    // char local_ip_address[] = "localhost";
    int local_port = 8010;
    int max_connections = 10;
    char message[] = "Hello, world!";
    int	aux; 
    int aleatorio;
    int i=0,id;

    pid_t child_pid;
	signal(SIGCHLD, reapChild); /* register the handler */
	signal(SIGALRM, handALRM); /* register the handler */
     // -- variables auxiliares para el manejo del system call de socket --
    int	sockaux;
    struct sockaddr_in my_addr;
    int newfd; 	/* Por este socket duplicado del inicial se transaccionará*/
    struct sockaddr_in their_addr;  /* Contendra la direccion IP y número de puerto del cliente */
	unsigned int sin_size = sizeof(struct sockaddr_in);

    // -- 1) crear un socket --
	if ((sockaux = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{
		fprintf(stderr, "Error en función socket. Código de error %s\n", strerror(sockaux));
		return -1;
	}

    // eliminar timeout
    //int reuse  = 1;
	//setsockopt(sockaux, SOL_SOCKET, SO_REUSEADDR, &reuse , sizeof(reuse)); 

    // -- 2) asignamos valores a la estructura my_addr --
    
    // -- familia de sockets INET para UNIX  --
	my_addr.sin_family = AF_INET;		
	
    // -- convierte el entero formato PC a entero formato network --
    my_addr.sin_port = htons(local_port);	
	
    // -- automaticamente usa la IP local (todas las que tenga) --
    my_addr.sin_addr.s_addr = INADDR_ANY;	
	
    // -- rellena con ceros el resto de la estructura  --
    bzero(&(my_addr.sin_zero), 8);



    // -- 3) con la estructura sockaddr_in completa, se declara en el Sistema que este 
    //       proceso escuchará pedidos por la IP y el port definidos --
	if ( (aux = bind (sockaux, (struct sockaddr *) &my_addr, sizeof(struct sockaddr))) == -1)
	{
		fprintf(stderr, "Error en función bind. Código de error %s\n", strerror(aux));
		return -1;
	}
	
    // -- 4) Habilitamos el socket para recibir conexiones, con una cola de conexiones 
    //       en espera que tendrá como máximo el tamaño especificado en max_connections --
	//       listen es 'non bloking'
    if ((aux = listen (sockaux, max_connections)) == -1)
	{
		fprintf(stderr, "Error en función listen. Código de error %s\n", strerror(aux));
		return -1;
    }

   

   while(1){

	    // -- 5) se espera por conexiones, accept es 'bloking'  
        // -- Extrae el primer pedido de conexion de la cola de conexiones
        // -- pendientes del socket s, le asocia un nuevo socket que es un
        // -- duplicado de s y le reserva un nuevo file descriptor --

	    if ((newfd = accept(sockaux, (struct sockaddr *)&their_addr, &sin_size)) == -1)
	    {
		    fprintf(stderr, "Error en función accept. Código de error %s\n", strerror(newfd));
		    return -1;
	    }
	    else
	    {
		    printf  ("server:  conexión desde:  %s\n", inet_ntoa(their_addr.sin_addr));
            if (numchld < PROLE)
            {
                if((id=fork())==0)
                {
                    // -- 6) escribo en el socket  --
                    srand(getpid());
                    while (1)
                    {
                        aleatorio = rand();
                        if (write (newfd, &aleatorio , sizeof (aleatorio)) == -1)
                        {
                            perror("Error escribiendo mensaje en socket");
                            exit (1);
                        }
                        sleep (1);
                        
                    }
                }
                else
                {
                    alarm (CHECK);	
                    numchld++;
                    i=0;
                    while (hijos[i].proc!=0)
                        i++;
                    hijos[i].proc=id;
                    hijos[i].init=time(NULL);
                    
                    printf("hijo %d",hijos[i].proc);
                    
                }
            }
            else
                printf("conexion rechazada\n");
	    }
   }

  
    
	return 0;
}
