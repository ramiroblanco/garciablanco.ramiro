#include "header.h"


/*------ TEORIA -----------
    
    
    1-F
    2-V
    3-V
    4-F
    5-V
    6-F
    7-V
    8-V
    9-F
    10-V
    11-V
    12-V
    13-F
    14-V
    15-F
    16-V
    17-F
    18-V
    19-V
    20-F
 
 */



int main (int argc, char ** argv)
{
    char ** archivo1;
    char ** archivo2;
    
    
    FILE * fp1, *fp2;
    char readbuf[BUFFSIZE];
    int cant1=0,cant2=0,count=0;
    int i;
    
    
    if (argc < 3)
    {
        printf ("faltan argumentos\n");
        exit (1);
        
    }
    // abro archivos
    
    fp1 = fopen (argv[1],"r");
    fp2 = fopen (argv[2],"r");
    
    // dummy read para contar lineas
    while (fgets(readbuf, BUFFSIZE, fp1) != NULL)
        cant1 ++;
    
    archivo1 = malloc (cant1 * sizeof(char *));
    
    
    while (fgets(readbuf, BUFFSIZE, fp2) != NULL)
        cant2 ++;
    
    archivo2 = malloc (cant2 * sizeof(char *));
    
    //vuelvo cursores al inicio
    fseek (fp1,0,SEEK_SET);
    fseek (fp2,0,SEEK_SET);

    // levanto archivos a memoria
    while (fgets(readbuf, BUFFSIZE, fp1) != NULL)
    {
        archivo1[count]=malloc(strlen(readbuf)+1);
        strcpy(archivo1[count],readbuf);
        count++;        
    }
    

    count =0;
    
    while (fgets(readbuf, BUFFSIZE, fp2) != NULL)
    {
        archivo2[count]=malloc(strlen(readbuf)+1);
        strcpy(archivo2[count],readbuf);
        count++;        
    }

    
    //me guardo el menor
    if (cant1<cant2)
        count=cant1;
    else
        count=cant2;
    
    count --;
    for (;count>=0;count--)
    {
        archivo1[count][strcspn(archivo1[count],"\n")] = '\0';
        archivo2[count][strcspn(archivo2[count],"\n")] = '\0';
        printf("%d",count);
        printf(": ");
        printf("%s",archivo1[count]);
        printf(" = ");
        printf("%s\n",archivo2[count]);
        
    }
    
    for (i=0;i<cant1;i++)
        free(archivo1[i]);
    free (archivo1);
        
    for (i=0;i<cant2;i++)
        free(archivo2[i]);
    free (archivo2);
    
    
    fclose (fp1);
    fclose (fp2);
 
    return 0;
}
