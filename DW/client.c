#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>

// -- El puerto donde se conectará, lado servidor --
#define PORT 8010	

// -- máxima cantidad de bytes que puede recibir en una transacción --
#define MAXDATASIZE 4096 

int conectar (char* hostname, int port);

int main(int argc, char * argv[])
{
	int sockfd;  /*File Descriptor para sockets*/
	int numbytes;/*Contendrá el número de bytes recibidos por read () */
	char buf[MAXDATASIZE];  /* Buffer donde se reciben los datos de read ()*/
    int aleatorio;
    // -- Tratamiento de la línea de comandos --
	if (argc < 3)
	{
		fprintf(stderr,"uso: %s hostname port\n",argv [0]);
		exit(1);
        }

    // -- conectarse al servidor --
	sockfd = conectar (argv[1], atoi(argv[2]));

    // -- recibimos los datos del servidor --
	while( read (sockfd, &aleatorio, sizeof(aleatorio))!= -1)
	{
        printf("recibido: %d\n",aleatorio);
		
	}

  
    // -- vevolvemos recursos al sistema --
	close(sockfd);


	return 0;
}


int conectar (char* hostname, int port)
{

	int	sockfd;
	struct hostent *he;	            /* Se utiliza para convertir el nombre del host a su dirección IP */
	struct sockaddr_in their_addr;  /* dirección del server donde se conectará */

	// -- convertimos el nombre del host a su dirección IP  --
	if ((he = gethostbyname ((const char *) hostname)) == NULL)
	{
		fprintf(stderr, "Error en Nombre de Host");
		exit(1);
	}
 
    // -- creamos el socket  --
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("Error en creación de socket");
		exit(1);
	}

    // -- establecemos their_addr con la direccion del server --
	their_addr.sin_family = AF_INET;
	their_addr.sin_port = htons(port);
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);

    // -- intentamos conectarnos con el servidor  --
	if (connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1)
	{
		fprintf(stderr, "Error tratando de conectar al server");
		exit(1);
	}
	return sockfd;
}
