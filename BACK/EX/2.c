#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>

#define PROLE 10
#define VIDA 20
#define ALARM_INTERVAL 5


typedef struct Tarea  {
	int proc;
	int init;
	int newfd;
} TAREA;

TAREA vector_tareas[PROLE] = {0,0};

int gen_child(int newfd) {
	int i=0;
	char str[2];
	while(1){
		if(i>111) i=0;
		str[0] = '\0';
		sprintf(str, "%i\n", i);
		if (write (newfd, str, 1) == -1)
		{
			perror("Error escribiendo mensaje en socket");
			exit (1);
		}
		sleep(1);
		i++;
	}

}

void alarm_chk() {

	for(int i=0;i<PROLE;i++) {
		if(vector_tareas[i].proc != 0) {
			if(time(NULL)-vector_tareas[i].init > VIDA) {
			close(vector_tareas[i].newfd);
			kill(vector_tareas[i].proc, SIGTERM);
			vector_tareas[i].proc = 0;
			vector_tareas[i].init = 0;
			wait(NULL);
			}
		}
	}

	alarm(ALARM_INTERVAL);

}

int main(void) {

	int i;
	int c_id;
	time_t now;

	signal(SIGALRM, alarm_chk);
	signal(SIGCHLD, alarm_chk);

	// char local_ip_address[] = "localhost";
	int local_port = 8010;
	int max_connections = 10;
	char message[] = "Hello, world!";
	int aux;

	int sockaux;
	struct sockaddr_in my_addr;
	int newfd;
	struct sockaddr_in their_addr;
	unsigned int sin_size = sizeof(struct sockaddr_in);

	if ((sockaux = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		fprintf(stderr, "Error en función socket. Código de error %s\n", strerror(sockaux));
		return -1;
	}

	// eliminar timeout
	int reuse  = 1;
	setsockopt(sockaux, SOL_SOCKET, SO_REUSEADDR, &reuse , sizeof(reuse));

	my_addr.sin_family = AF_INET;

	my_addr.sin_port = htons(local_port);

	my_addr.sin_addr.s_addr = INADDR_ANY;

	bzero(&(my_addr.sin_zero), 8);

	if ( (aux = bind (sockaux, (struct sockaddr *) &my_addr, sizeof(struct sockaddr))) == -1)
	{
		fprintf(stderr, "Error en función bind. Código de error %s\n", strerror(aux));
		return -1;
	}


	if ((aux = listen (sockaux, max_connections)) == -1)
	{
		fprintf(stderr, "Error en función listen. Código de error %s\n", strerror(aux));
		return -1;
	}

	alarm(ALARM_INTERVAL);

	while(1){

		// -- 5) se espera por conexiones, accept es 'bloking'  
		// -- Extrae el primer pedido de conexion de la cola de conexiones
		// -- pendientes del socket s, le asocia un nuevo socket que es un
		// -- duplicado de s y le reserva un nuevo file descriptor --

		if ((newfd = accept(sockaux, (struct sockaddr *)&their_addr, &sin_size)) == -1)
		{
			fprintf(stderr, "Error en función accept. Código de error %s\n", strerror(newfd));
			return -1;
		}
		else
		{
			printf  ("server:  conexión desde:  %s\n", inet_ntoa(their_addr.sin_addr));
			i=0;
			do {
				printf("_%i\n",i);
				//printf("_%i\n",vector_tareas[i].proc);
				if(vector_tareas[i].proc == 0) break;
				i++;
			}while(i<PROLE); 

			if(i<PROLE) {
				c_id = fork();
				if (c_id == 0) {

					gen_child(newfd);
				//close(newfd);
				} else {
					time(&now);
					vector_tareas[i].proc = c_id;
					vector_tareas[i].init = time(NULL);
					vector_tareas[i].newfd = newfd;
				}
			}
		}
	}


	return 0;


}
