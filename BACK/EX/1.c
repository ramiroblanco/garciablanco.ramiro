#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_L 100
#define MAX_LINES 100

void to_upper(char *str) {

	int len = strlen(str);
	for(int i=0; i<len;i++) 
		if(str[i] < 123 && str[i] > 96)
			str[i]-=32;
	return;
}

int main(int argc, char **argv) {

	int c;
	int lineas1 = 0;
	int lineas2 = 0;
	int lineas_menor;

	FILE *f1 = fopen(argv[1], "r");
	FILE *f2 = fopen(argv[2], "r");

	char *lineas_f1[MAX_LINES] = {NULL};
	char *lineas_f2[MAX_LINES] = {NULL};

	if(!f1 || !f2) { printf("Error abriendo archivos!\n"); exit(1);}

        lineas_f1[0] = calloc(MAX_LINE_L, sizeof(char));
        lineas_f2[0] = calloc(MAX_LINE_L, sizeof(char));

        while((c = getc(f1)) != EOF) {
                if(c == '\n') {
			lineas_f1[lineas1][strlen(lineas_f1[lineas1])] = '\0';
			lineas_f1[++lineas1] = calloc(MAX_LINE_L, sizeof(char));
		}else{
			lineas_f1[lineas1][strlen(lineas_f1[lineas1])] = c;
		}

	}
	fclose(f1);

        while((c = getc(f2)) != EOF) {
                if(c == '\n') {
			lineas_f2[lineas2][strlen(lineas_f2[lineas2])] = '\0';
			lineas_f2[++lineas2] = calloc(MAX_LINE_L, sizeof(char));
		}else{
			lineas_f2[lineas2][strlen(lineas_f2[lineas2])] = c;
		}

	}

	lineas_menor = (lineas1 > lineas2) ? lineas1 : lineas2;
	for(int i=0; i < lineas_menor;i++) {
		to_upper(lineas_f1[i]);
		to_upper(lineas_f2[i]);
		printf("LINEA %i: %s = %s\n",i, lineas_f1[i],lineas_f2[i]);
	}
	fclose(f2);
}
