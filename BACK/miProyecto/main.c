#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "database.h"

#define FILE_NAME "main.c"

#define DATABASE_NAME "db.dat"

int main() {

	ST_PERSONA p = {1, "Juan", 30, 1.72f, 70.0f};
	ST_PERSONA q;

	int id = 1;
	FILE *database = NULL;

	//Abrir db
	database = db_open(DATABASE_NAME);
	if(database == NULL) {
		printf("Error: %s db_open %s\n",FILE_NAME,DATABASE_NAME);
		exit(1);
	}

	//Escribir a db
	printf("x: %i %i \n",DATABASE_WRITE_OK, write_record(database, &p));
	if(write_record(database, &p) != DATABASE_WRITE_OK) {
		printf("Error: %s write_record %s\n",FILE_NAME,DATABASE_NAME);
	}

	//Buscar ID db
	if(read_record(database, id, &q) == DATABASE_READ_FOUND) {
		printf("Error: %s read_record %s\n",FILE_NAME,DATABASE_NAME);
	} else {
		printf("%s\n", q.nombre);
	}


	//Cerrar db
	if(db_close(database)) {
		printf("Error: %s db_close %s\n",FILE_NAME,DATABASE_NAME);
	}

	return 0;
}
