#include <stdio.h>

#define STR_NAME_LEN 50

#define DATABASE_WRITE_FAIL 0
#define DATABASE_WRITE_OK 1

#define DATABASE_READ_FAIL 0
#define DATABASE_READ_OK 1

#define DATABASE_READ_FOUND 2
#define DATABASE_READ_NOTFOUND 3


struct persona {

	int id;
	char nombre[STR_NAME_LEN];
	int edad;
	float altura,peso;

};

typedef struct persona ST_PERSONA;

//CRUD
FILE *db_open(char *file_name);
int db_close(FILE *fp);
int write_record(FILE *fp, ST_PERSONA *p);
int read_record(FILE *fp, int id, ST_PERSONA *p);

