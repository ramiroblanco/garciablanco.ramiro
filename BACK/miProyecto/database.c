#include "database.h"

FILE *db_open(char *file_name) {

	FILE *fp;
	fp = fopen(file_name, "a+");
	return fp;

}

int db_close(FILE *fp) {

	return fclose(fp);

}

int write_record(FILE *fp, ST_PERSONA *p) {

	//TODO verificaciones (archivo, estructura)	
	//
	if(fseek(fp, 0, SEEK_END) != 0) {
		//TODO	
		return DATABASE_WRITE_FAIL;
	}
	if(fwrite(p, sizeof(ST_PERSONA), 1, fp) == 0) {
		//TODO
		return DATABASE_WRITE_FAIL;
	}

	return DATABASE_WRITE_OK;
}

int read_record(FILE *fp, int id, ST_PERSONA *p) { //busca por el campo id, suponemos que el id es unico y los registros estan desordenados

	ST_PERSONA persona_tmp;
	int r;

	//TODO realizar las verificaciones
	
	if(fseek(fp, 0, SEEK_SET) != 0) {
		//TODO	
		return DATABASE_READ_FAIL;
	}

	while( (r = fread(&persona_tmp, sizeof(ST_PERSONA), 1, fp)) != 0) {
		if(persona_tmp.id == id) {
			//copio estructura
			*p = persona_tmp;
			return DATABASE_READ_FOUND;
		}
	
	}
	return DATABASE_READ_NOTFOUND;
	
}
