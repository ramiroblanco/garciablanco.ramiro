#include <stdio.h>
#include <string.h>
#include <math.h>

#define MAX_CHAR 100

int base2(char num[]);
int base8(char num[]);
int base16(char num[]);
void to_bin_oct_hex(char num[]);

int tobin_divisiones(int cantidad, char *num);


int main() {

	char num[MAX_CHAR];
	char num2[MAX_CHAR];
	int result;
	/*
	printf("binario: ");
	scanf(" %s", num);
	result = base2(num);
	if(result < 0)
		printf("Error base2\n");
	else
		printf("base2 --> %i\n",result);

        printf("octal: ");
        scanf(" %s", num);
        result = base8(num);
        if(result < 0)
                printf("Error base8\n");
        else
                printf("base8 --> %i\n",result);

        printf("hexa: ");
        scanf(" %s", num);
        result = base16(num);
        if(result < 0)
                printf("Error base16\n");
        else
                printf("base16 --> %i\n",result);

	to_bin_oct_hex("12341324");
		*/
	tobin_divisiones(123456, num2);

}

int base2(char num[]) {
	int leng = ( (int) strlen(num) ) - 1;
	int value = 0;
	int number = 0;
	for(int posi = leng ;posi>=0;posi--) {
		number = (int) num[posi] - 48;
		//printf("___%i %i %i \n",number,leng-posi,number * (int) pow(2,(leng-posi)));
		if(number > 1)	
			return -1;
		value = value + ( number * (int) pow(2,leng-posi) );
	}
	return value;
}

int base8(char num[]) {
        int leng = ( (int) strlen(num) ) - 1;
        int value = 0;
        int number = 0;
        for(int posi = leng ;posi>=0;posi--) {
                number = (int) num[posi] - 48;
                //printf("___%i %i %i \n",number,leng-posi,number * (int) pow(8,(leng-posi)));
                if(number > 1)
                        return -1;
                value = value + ( number * (int) pow(8,leng-posi) );
        }
        return value;
}


int base16(char num[]) {
        int leng = ( (int) strlen(num) ) - 1;
        int value = 0;
        int number = 0;
        for(int posi = leng ;posi>=0;posi--) {
               	number = (int) num[posi];
		if( number >= 65)
                	number = number - 55;
		else
                	number = number - 48;
                //printf("___%i %i %i \n",number,leng-posi,number * (int) pow(8,(leng-posi)));
                if(number > 16)
                        return -1;
                value = value + ( number * pow(16,leng-posi) );
        }
        return value;
}

void to_bin_oct_hex(char num[]) {
        int leng = ( (int) strlen(num) ) - 1;
        int number = 0;
	printf("Binario______________\n");
        for(int posi = leng ;posi>=0;posi--) {
                number = (int) num[posi] - 48;
                printf("%i * 2 ^ %i = %i \n",number,leng-posi,number * (int) pow(2,(leng-posi)));
                //value = value + ( number * pow(16,leng-posi) );
        }
	printf("Octal______________\n");
	        for(int posi = leng ;posi>=0;posi--) {
                number = (int) num[posi] - 48;
                printf("%i * 8 ^ %i = %i \n",number,leng-posi,number * (int) pow(8,(leng-posi)));
                //value = value + ( number * pow(16,leng-posi) );
        }

	printf("Hexadecimal______________\n");
                for(int posi = leng ;posi>=0;posi--) {
                number = (int) num[posi];
                if( number >= 65)
                        number = number - 55;
                else
                        number = number - 48;
                printf("%i * 16 ^ %i = %i \n",number,leng-posi,number * (int) pow(16,(leng-posi)));
                //value = value + ( number * pow(16,leng-posi) );
        }

        return;

}



int tobin_divisiones(int cantidad, char *num) {

	int c = 0;
	int result = cantidad;

	while(1) {
		if(result% 2)
			num[c] = 1;
		else
			num[c] = 0;

		printf(">> %d\n",num[c]);
		result = result / 2;
		c++;	
		if(result < 2) {
			if(result>=1)
				num[c] = 1;
			else
				num[c] = 0;
			break;
		}
		/*printf("_r: %d\n", result);
		printf("_d: %d\n", result % 1); */
	}

	printf("Result (%i): ",c);
	for(; c>=0;c--)
		printf("%i", num[c]);

	printf("\n");
	return 0;
}
