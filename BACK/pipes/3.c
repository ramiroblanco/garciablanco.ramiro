#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

int enviarInvertido(int fd, char *fileName);

int main(void) {

        int fd;
        char buffer[100];
        int pid;

        if( (pid = fork()) ) {
                //padre printf("padre\n\n");
		mkfifo("./fifo",0666);
		fd=open("./fifo", O_WRONLY);
		enviarInvertido(fd,"test.txt");
                close(fd);
        } else {
                //hijo printf("hijo\n\n");
		fd=open("./fifo", O_RDONLY);
                read(fd,buffer,5);
                printf("Read: %s\n",buffer);
                close(fd);
        }


	return 0;
}

int enviarInvertido(int fd, char *fileName) {

	int len=0;
	char c;
	FILE *fp;
	char buffer[100] = "";
	char buffer_inv[100] = "";

        if ((fp = fopen(fileName, "r")) == NULL) {
		printf("no se pudo abrir el archivo\n");
                return 0;
        }

        if(fseek(fp, 0, SEEK_SET) != 0) {
                printf("No se pudo ubicar al principio.\n");
        }

        while((c = getc(fp)) != EOF) { 
		len = strlen(buffer);
		buffer[len] = c;
	}
	buffer[++len] = '\0';
	printf("Contenido: %s\n",buffer);

	while(len >= 0) {
		len--;
		strncat(buffer_inv, &buffer[len], 1); 
	}
	write(fd,buffer_inv,5);
	wait(0);


	return 1;	

}
