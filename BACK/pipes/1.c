#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


int main (void) {

	int fd[2];
	char buf[10];
	int pid;
	if(pipe(fd)) exit(1);

	if(pid = fork()) {
		//padre
		close(fd[0]);
		write(fd[1],"hola",5);
		wait(0);
	} else {
		//hijo
		close(fd[1]);
		read(fd[0],buf,5);
		printf("%s\n",buf);
	}

	return 0;
}
