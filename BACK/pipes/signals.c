#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>


void handler(int signal_number) {
	printf("SIGUSR1 received!\n");
}


int main(void) {
	struct sigaction sa;
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = handler;

	sigaction(SIGUSR1, &sa, NULL);

	pid_t pid;

	pid = getpid();

	printf("pid=%d\n", pid);

	while(1) {
		sleep(2);
	
	}


	return 0;

}
