#include <stdio.h>
#include <stdlib.h>

struct st_data {

	int id;
	char desc[16];

};

typedef struct st_data ST_DATA;

int main() {

	int r;

	ST_DATA data = {1, "valor 1\n"};

	//struct FILE
	FILE *fp;

	fp = fopen("test.dat", "w");

	if(fp == NULL) {
		printf("no se pudo abrir archivo\n");
		exit(1);
	}

	r = fwrite(&data,sizeof(ST_DATA),1 ,fp);
	printf("Bloques escritos: %i\n", r);
	
	fclose(fp);

	return 0;
}
