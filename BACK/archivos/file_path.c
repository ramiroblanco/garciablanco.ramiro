#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 *escriba un programa el cual reciba por linea de comandos un path a un archivo, y genero un nuevo archivo en la misma ruta con el volcado hexadecimal en ascii de su contenido. investigue la funcion fprintf
 */

int main(int argc, char **argv) {

	FILE *i_file;
	FILE *o_file;

	i_file = fopen(argv[1], "r");

        if(i_file == NULL) {
                printf("No se pudo abrir archivo de entrada.\n");
                exit(1);
        }

	strcat(argv[1], ".hex");

	o_file = fopen(argv[1], "w");
        if(o_file == NULL) {
                printf("No se pudo abrir archivo de salida.\n");
                exit(1);
        }

	char c;
	int count;
	//while ((c = fgetc(i_file)) != EOF) {
	while (fread(&c,1,1,i_file)) {
		fprintf(o_file, "%02x", c);
		//printf("%c %u\n",c,c);
		count++;
		if(count % 2 == 0) fprintf(o_file, " "); 
		if(count > 15) {count = 0;fprintf(o_file, "\n");}
	}

	fclose(i_file);
	fclose(o_file);
	return 0;
}

