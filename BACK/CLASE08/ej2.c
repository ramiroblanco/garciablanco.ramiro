#include <stdio.h>
#include <string.h>

#define NUMERO 0
#define MAIL 1
#define TARJETA 2
#define OTRO 3

int contar_strings(char **s);

int es_numerico(char *s);

int es_email(char *s);

int es_tarjeta(char *s);

void ordenar_strings(char **s);

void imprimir_strings(char **s);

void ordenar(char **s, int *tipos, int tipo);


int main(int argc, char **argv) {

	if(contar_strings(argv) < 2)
		printf("No hay argumentos!\n");
	else
		ordenar_strings(argv);

}


int contar_strings(char **s) {

	int i = 0;

	while(s[i]) i++;
	
	return i;
}

int es_numerico(char *s) {

	int length = strlen(s);
	int i = 0;

	for(;i<length;i++) if(s[i] < 48 || s[i] > 57 || s[i] == 46) return -1;

	return 1;

}

int es_email(char *s) {
	
	int length = strlen(s);
	int i = 0;
	int at_check = 0;
        int point_check = 0;

	for(;i<length;i++) {
		if(s[i] == 64) at_check++;
		if(s[i] == 46) point_check++;
	}

	if(at_check != 1)
		return -1;
	if(point_check < 1)
		return -1;
	return 1;

}


int es_tarjeta(char *s) {

	char *tarjetas[] = {"XXXX-XXXX-XXXX-XXXX" /*VISA*/,
		 	      "XXXX-XXXXXX-XXXXX" /*AMEX*/,
	 			NULL /*Fin*/ };
	int i;
	int t_count = 0;
	int length_t;
	int length_s = strlen(s);

	while(tarjetas[t_count]) {

		length_t = strlen(tarjetas[t_count]);
		
		if(length_s == length_t) {

			for(i = 0; i < length_t; i++){

				if(tarjetas[t_count][i] == 45 && s[i] !=45) continue; 
				else if(s[i] < 48 || s[i] > 57) continue;
				
				return 0;
			}
		}
		t_count++;
	}
	return 1;
}


void ordenar_strings(char **s) {

	int length = contar_strings(s);
	int i = 0;

	int tipos[length];


	for(;i<length;i++) {
		printf("_%s_",s[i]);
		printf("%i\n",es_tarjeta(s[i]));

		if(es_numerico(s[i]) > 0) tipos[i] = NUMERO;

		else if(es_email(s[i]) > 0) tipos[i] = MAIL;

		else if(es_tarjeta(s[i]) > 0) tipos[i] = TARJETA;

		else tipos[i] = OTRO;

	}

	/*

	for(i = 1;i<length;i++){ //COMIENZA EN EL SEGUNDO elemento
		if(tipos[i] == NUMERO) {
			numeros[numeros_c] = (float) s[i];
		}else if(tipos[i] == MAIL) {
			mails[mails_c] = s[i];
		}
		}else if(tipos[i] == TARJETA) {
			tarjetas[tarjetas_c] = s[i];
		}else{
			otros[otros_c] = s[i];
		}
	}
*/

}


void ordenar(char **s, int *tipos, int tipo) {

	int i = 0;

	while(s[i]) {
		
		if(tipos[i] != tipo) continue;
	
	//	for(i = 0; i < )
		i++;
	}


}


