/*
 *
 *  gcc -o DOOM DOOM.c -lncursesw -lm -D_XOPEN_SOURCE_EXTENDED
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include <sys/time.h>
#include <unistd.h>  /* only for sleep() */
#include <math.h>
#include <locale.h>

#define MAP_SIZE_X 16
#define MAP_SIZE_Y 16
#define MAP_POS_X 4
#define MAP_POS_Y 4

#define SCREEN_W 1024 
#define SCREEN_H 250 
#define HORIZ 100 
#define WALL_H 200 
#define FOV 90
#define MAX_SIGHT 8 
#define USER_START_X 8 
#define USER_START_Y 8 

float timedifference_msec(struct timeval t0, struct timeval t1)
{
	return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

int kbhit(void)
{
int ch = getch();

	if (ch != ERR) {
		ungetch(ch);
		return 1;
	} else return 0; 
}

int main(int argc, char **argv) {

	WINDOW *mywindow;

	setlocale(LC_ALL, "");

	/* initialize curses */
	initscr();
	cbreak();
	noecho();
	nodelay(stdscr, TRUE);
	curs_set(FALSE);

	if (has_colors() == FALSE) {
		endwin();
		printf("La terminal no soporta color!\n");
		exit(1);
	}
	start_color();
	init_pair(1, COLOR_BLACK, COLOR_BLACK);
	init_pair(2, COLOR_YELLOW, COLOR_BLACK);
	init_pair(3, COLOR_RED, COLOR_BLACK);
	init_pair(4, COLOR_RED, COLOR_MAGENTA);



	char map[MAP_SIZE_X][MAP_SIZE_Y] = {
		"################",
		"#    #   #     #",
		"#    #   #     #",
		"#              #",
		"#              #",
		"#   #####      #",
		"#  #           #",
		"# #            #",
		"##             #",
		"#              #",
		"#       ###    #",
		"#          #   #",
		"#          #   #",
		"#  ########    #",
		"#              #",
		"################",
	};


	//printf("_ %c",map[13][15]);
	//return 0;

	struct timeval start, stop;
	double secs = 0;

	clear();

	printw("DOOM\n");

	int live = 1;

	int scr_w;

	char stats[50];
	char buf[30];
	float user_angle = 0.0f;
	int max_sight = MAX_SIGHT;
	float it_sight;
	int it_map_x;
	int it_map_y;
	float it_x;
	float it_y;
	float user_x = USER_START_X;
	float user_y = USER_START_Y;
	float it_wall;
	double it_angle;
	int it_color;
	
	float fov_slices = (float) FOV / SCREEN_W;
	const wchar_t wall = 0x2588;

	while(live) {
	
		gettimeofday(&start, NULL);
		//clear();
		secs = (double)(stop.tv_usec - start.tv_usec) / 1000000 + (double)(stop.tv_sec - start.tv_sec);
		//printw("_%d\n",timedifference_msec(start, stop));
		//printw("_%d",timedifference_msec(start,stop));
		
		//CLEAR/FLOOR
		attron(COLOR_PAIR(3));
		attron(A_BOLD);
		for(scr_w =  0 ; scr_w < SCREEN_W; scr_w++) 
			for(it_wall = 0;it_wall < HORIZ;it_wall++) {
				it_color = it_wall / 5;
				switch((int) it_color) {
				case 0:
					mvaddwstr( it_wall, scr_w, L"\u2588");
					mvaddwstr( it_wall, scr_w, L"\u2588");
					break;
				case 1:
					mvaddwstr( it_wall, scr_w, L"\u2593");
					mvaddwstr( it_wall, scr_w, L"\u2593");
					break;
				case 2:
					mvaddwstr( it_wall, scr_w, L"\u2592");
					mvaddwstr( it_wall, scr_w, L"\u2592");
					break;
				case 3:
					mvaddwstr( it_wall, scr_w, L"\u2591");
					mvaddwstr( it_wall, scr_w, L"\u2591");
					break;
				default:
					mvaddch( it_wall, scr_w, ' ');
					mvaddch( it_wall, scr_w, ' ');
				}
			}

		attroff(COLOR_PAIR(3));
		
		attron(COLOR_PAIR(2));
		attron(A_BOLD);
		for(scr_w =  0 ; scr_w < SCREEN_W; scr_w++) 
			for(it_wall = HORIZ;it_wall < SCREEN_H;it_wall++) {
				it_color = ( it_wall - HORIZ )/ 5;
				switch((int) it_color) {
				case 4:
					mvaddwstr( it_wall, scr_w, L"\u2588");
					mvaddwstr( it_wall, scr_w, L"\u2588");
					break;
				case 3:
					mvaddwstr( it_wall, scr_w, L"\u2593");
					mvaddwstr( it_wall, scr_w, L"\u2593");
					break;
				case 2:
					mvaddwstr( it_wall, scr_w, L"\u2592");
					mvaddwstr( it_wall, scr_w, L"\u2592");
					break;
				case 1:
					mvaddwstr( it_wall, scr_w, L"\u2591");
					mvaddwstr( it_wall, scr_w, L"\u2591");
					break;
				default:
					mvaddch( it_wall, scr_w, ' ');
					mvaddch( it_wall, scr_w, ' ');
				}
				}
		attroff(COLOR_PAIR(2));
		

		//WALLS
		for(scr_w =  SCREEN_W/2 ; scr_w >= (SCREEN_W/2*-1); scr_w--) {

			it_angle = (double) user_angle + (atanf((double)scr_w/SCREEN_W/2)*180)/M_PI;
			//it_angle = scr_w/SCREEN_W/2;
			//gcvt(it_angle , 6, buf );
			//gcvt((int) (1*sin(user_angle + (scr_w  * fov_slices))), 6, buf );
			//printw("\n%s ",buf);
			//mvaddstr( SCREEN_H + scr_w, 120, buf);
			//gcvt((float) scr_w , 6, buf );
			//mvaddstr( SCREEN_H + scr_w, 150, buf);
	
			for(it_sight = 0;it_sight <= max_sight;it_sight=it_sight + 0.1) {
				
				it_y = (float) it_sight*sin(it_angle*M_PI/180) + user_y;
				it_x = (float) it_sight*cos(it_angle*M_PI/180) + user_x;
				

				if(map[(int) it_x][(int) it_y] == 35) {
					attron(COLOR_PAIR(1));
					attron(A_BOLD);
					for(it_wall = 0;it_wall < (float) WALL_H/2/it_sight;it_wall=it_wall+1) {
						switch((int) it_sight) {
						case 0:
							mvaddwstr( HORIZ + it_wall, scr_w + 120, L"\u2588");
							mvaddwstr( HORIZ - it_wall, scr_w + 120, L"\u2588");
							break;
						case 1:
							mvaddwstr( HORIZ + it_wall, scr_w + 120, L"\u2588");
							mvaddwstr( HORIZ - it_wall, scr_w + 120, L"\u2588");
							break;
						case 2:
							mvaddwstr( HORIZ + it_wall, scr_w + 120, L"\u2593");
							mvaddwstr( HORIZ - it_wall, scr_w + 120, L"\u2593");
							break;
						case 3:
							mvaddwstr( HORIZ + it_wall, scr_w + 120, L"\u2592");
							mvaddwstr( HORIZ - it_wall, scr_w + 120, L"\u2592");
							break;
						case 4:
							mvaddwstr( HORIZ + it_wall, scr_w + 120, L"\u2591");
							mvaddwstr( HORIZ - it_wall, scr_w + 120, L"\u2591");
							break;
						default:
							mvaddch( HORIZ + it_wall, scr_w + 120, ' ');
							mvaddch( HORIZ - it_wall, scr_w + 120, ' ');
						}


					}
					attroff(COLOR_PAIR(1));
					break;
				}else
				mvaddch( it_y + MAP_POS_Y, it_x + MAP_POS_X , '.');
			
			}

		}

		//MAP
		for(it_map_x=0;it_map_x < MAP_SIZE_X;it_map_x++) 
			for(it_map_y=0;it_map_y < MAP_SIZE_Y;it_map_y++) {
				if(map[it_map_x][it_map_y] == 35 ) 
				mvaddch( MAP_POS_Y + it_map_y, MAP_POS_X + it_map_x, map[it_map_x][it_map_y]);
			}		
		//STATS
		stats[0] = '\0'; 
		strcat(stats, "Angle: ");
		gcvt((float) user_angle, 6, buf);
		strcat(stats, buf);
		strcat(stats, " X: ");
		gcvt((float) user_x, 6, buf);
		strcat(stats, buf);
		strcat(stats, " Y: ");
		gcvt((float) user_y, 6, buf);
		strcat(stats, buf);
		mvaddstr(0, 0, "                                         ");
		mvaddstr(0, 0, stats);


			//printw("%s\n",map[1]);
		mvaddch(user_y + MAP_POS_Y, user_x + MAP_POS_X,'X');

	
		if (kbhit()) {
			//printw("Key pressed! It was: %d\n", getch());
			switch(getch()) {
				case 119:
					it_y = (float) sin(user_angle*M_PI/180) + user_y;
					it_x = (float) cos(user_angle*M_PI/180) + user_x;
				        if( it_x > 1 && it_x < MAP_SIZE_X && it_y < MAP_SIZE_Y && it_y > 1 ) { 
					user_x = it_x;
					user_y = it_y;
					}
					break;
				case 115:

					it_y = (float) user_y - 2*sin(user_angle*M_PI/180);
					it_x = (float) user_x - 2*cos(user_angle*M_PI/180);
				        if( it_x > 1 && it_x < MAP_SIZE_X && it_y < MAP_SIZE_Y && it_y > 1 ) { 
					user_x = it_x;
					user_y = it_y;
					}
					break;
				case 100:
					user_angle = user_angle + 5; 
					if(user_angle > 360) user_angle = user_angle - 360;
					if(user_angle < 0) user_angle = user_angle + 360;
					break;
				case 97:
					user_angle = user_angle - 5; 
					if(user_angle > 360) user_angle = user_angle - 360;
					if(user_angle < 0) user_angle = user_angle + 360;
					break;
			}


		} else {
			//printw("No key pressed yet...\n");
		}
		usleep(30000);
		gettimeofday(&stop, NULL);
	}

	endwin();

	return 0;

}

