#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct autos
{
    char marca [20];
    char modelo [20];
    char color [20];
    int peso;
    int anio;
};


/* Criterios */
#define MARCA      0
#define MODELO     1
#define COLOR      2
#define PESO       3
#define ANIO       4

/*Formatos*/
#define CSV      0
#define RAW      1


struct autos * cargar_registro (int fd, char formato);
struct autos * nuevo_registro (void);
void ordenar_registros (struct autos **,char criterio);
void guardar_registros (struct autos **, char formato);
void remover_duplicados (struct autos **);


/*
 Equipos de desarrollo:
 Andrenacci - Sanchez -- cargar_registro
 Limachi - Yampa Potosi -- nuevo_registro
 Gallardo - Luparia -- ordenar_registros
 Allemand - Garcia M -- guardar_registros
 Cuda - Garcia Blanco -- remover_duplicados
 Lugones - Vega -- Main
 
 */
int main (void)
{
	struct autos *_autos[100] = {NULL};

        _autos[0] = malloc(sizeof(struct autos));

        strcpy(_autos[0]->marca, "marca_0");
        strcpy(_autos[0]->modelo, "modelo_0");
        strcpy(_autos[0]->color, "color_0");
        _autos[0]->peso = 0;
        _autos[0]->anio = 0;

        _autos[1] = malloc(sizeof(struct autos));

        strcpy(_autos[1]->marca, "marca_1");
        strcpy(_autos[1]->modelo, "modelo_1");
        strcpy(_autos[1]->color, "color_1");
        _autos[1]->peso = 1;
        _autos[1]->anio = 1;


        _autos[2] = malloc(sizeof(struct autos));

        strcpy(_autos[2]->marca, "marca_2");
        strcpy(_autos[2]->modelo, "modelo_2");
        strcpy(_autos[2]->color, "color_2");
        _autos[2]->peso = 2;
        _autos[2]->anio = 2;


        _autos[3] = malloc(sizeof(struct autos));

        strcpy(_autos[3]->marca, "marca_3");
        strcpy(_autos[3]->modelo, "modelo_3");
        strcpy(_autos[3]->color, "color_3");
        _autos[3]->peso = 3;
        _autos[3]->anio = 3;


        _autos[4] = malloc(sizeof(struct autos));

        strcpy(_autos[4]->marca, "marca_3");
        strcpy(_autos[4]->modelo, "modelo_3");
        strcpy(_autos[4]->color, "color_3");
        _autos[4]->peso = 3;
        _autos[4]->anio = 3;


        _autos[5] = malloc(sizeof(struct autos));

        strcpy(_autos[5]->marca, "marca_1");
        strcpy(_autos[5]->modelo, "modelo_1");
        strcpy(_autos[5]->color, "color_1");
        _autos[5]->peso = 1;
        _autos[5]->anio = 1;

	printf("pri\n");
	remover_duplicados(_autos);
	printf("segu\n");
	remover_duplicados(_autos);



    return 0;
}


void remover_duplicados (struct autos **autos) {

	int i,j,c;
	for(i=0;i<100;i++) {

		if(autos[i] == NULL) continue;

		for(j=0;j<100;j++) {
			c=0;
			if(autos[j] == NULL) continue;
			if(i==j)continue;
			if(!strcmp(autos[i]->marca, autos[j]->marca)) c++;
			if(!strcmp(autos[i]->modelo, autos[j]->modelo)) c++;
			if(!strcmp(autos[i]->color, autos[j]->color)) c++;
			if(autos[i]->peso ==  autos[j]->peso) c++;
			if(autos[i]->anio ==  autos[j]->anio) c++;
			if(c > 4) {
				//printf("IGUAL!!!! i: %s j: %s \n",autos[i]->marca,autos[j]->marca);
				free(autos[j]);		
				autos[j] = NULL;
			}
		
		}
	
	}	

}


