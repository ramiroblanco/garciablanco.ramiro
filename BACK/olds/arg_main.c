#include <stdio.h>

void print_string_as_char( char *str);

int main(int argc, char* argv[] ){

    int i;

    printf("La cantidad de argumentos es argc = %d\n", argc);
    for(i=0;i<argc;i++){
        printf("argv[%d] -> dir -> %p referencia -> %p -> %10s -> ", i, &argv[i], argv[i],argv[i]);
        print_string_as_char(argv[i]);
        printf("\n");
    }

    printf("\n");
   
    return 0;
}

void print_string_as_char( char *str){
    while(*str != 0){
        printf("|'%c'| ", *str);
        str++;
    }
    printf("|'\\0'|");
}

