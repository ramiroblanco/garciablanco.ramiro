/*
 * Resolver la funcion es_tringulo_v1 utilizando "operadores logicos"
 * Preguntas:
 *      - 
 */

#include<stdio.h>

#define VERDADERO   1
#define FALSO       0

int es_triangulo_v1 (int,int,int);
int es_triangulo_v2 (int,int,int);



int main (void)
{
    int lado1=4;
    int lado2=5;
    int lado3=6;
    int resultado;
    
    resultado = es_triangulo_v1(lado1,lado2,lado3);
    if (resultado == VERDADERO)
    {
        printf("los valores se corresponden con un triangulo\n");
    }
    else
    {
        printf("los valores NO se corresponden con un triangulo\n");
    }
    
    resultado = es_triangulo_v2(lado1,lado2,lado3);
    if (resultado == VERDADERO)
    {
        printf("los valores se corresponden con un triangulo\n");
    }
    else
    {
        printf("los valores NO se corresponden con un triangulo\n");
    }
    return 0;    
    
}


int es_triangulo_v1 (int lado1 ,int lado2 ,int lado3) {
    
    int resultado =FALSO;
    
    if( (lado1 + lado2) > lado3)
    {
        if (lado2 + lado3 > lado1)
        {
            if (lado1 + lado3 > lado2)
            {
                resultado = VERDADERO;
            }
        }
    }
    
    return resultado;
}

int es_triangulo_v2 (int lado1 ,int lado2 ,int lado3) {
    int resultado =FALSO;

    if( lado1 + lado2 > lado3 && 
        lado2 + lado3 > lado1 && 
        lado1 + lado3 > lado2 )
    {
                resultado = VERDADERO;
    }
    return resultado;
}


