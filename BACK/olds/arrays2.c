#include <stdio.h>

#define MAX 5
#define MAX2 6

int valores[MAX] = {1,1,1,2,1};
int valores2[MAX2] = {1,1,1,2,1,1};
int out;

int array_find(int *, int,int);
int array_equal(int *, int, int *, int);
void array_fill(int *, int, int);
void array_copy(int *, int, int *, int);
float array_average (int *, int);

int main() {

	// ARRAY_FIND
	printf("Ingrese valor a buscar: ");
	int buscado;
	scanf(" %i", &buscado);
	out = array_find(valores,MAX,buscado);
	if(out) printf("El valor se encuentra en el indice %i\n", out);
	else printf("El valor no se encuentra en el array!\n");

	// ARRAY_EQUAL
	if(array_equal(valores,MAX,valores2,MAX2) > -1) printf("Arrays iguales.\n");
	else printf("Arrays distintos.\n");

	// ARRAY_AVERAGE
	printf("Promedio: %.2f\n",array_average(valores,MAX));

	// ARRAY_COPY
	array_copy(valores,MAX,valores2,MAX2);

	// ARRAY_FILL
	array_fill(valores,MAX, 5);

	for(int i;i<MAX;i++)
		printf("Indice %i: valor:%i\n",i,valores[i]);

	return 0;
}

int array_find(int *array, int size, int buscado) {
	int out = -1;
	for(int i=0;i<size;i++) 
		if(array[i] == buscado) {
			out = i;
			break;
		}
	return out;
}
int array_equal(int *array, int size, int *array_2, int size_2) {
	int out = 0;
	if(size == size_2) {
		for(int i=0;i<size;i++) 
			if(array[i] != array_2[i]){
				out = -1;
				break;
			}
	} else 
		out = -1;
	
	return out;
} 
void array_fill(int *array, int size, int valor) {
	for(int i=0;i<size;i++) 
		array[i] = valor;
	
	return;
}
void array_copy(int *array, int size, int *array_copy, int size_copy) {
	if(size != size_copy) 
		printf("Tamanos diferentes!\n");
	else	
		for(int i=0;i<size;i++) 
			array_copy[i] = array[i];
	return;
}

float array_average(int *array, int size) {
	int suma = 0;
	for(int i=0;i<size;i++) 
		suma = suma + array[i];
	
	return (float) suma/size;
}
