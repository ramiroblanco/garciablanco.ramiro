/*
 * Realizar un programa que permita la carga de valores enteros por teclado.
 * Luego de ingresar el valor mostrar un mensaje por pantalla que pida confirmar
 * al usuario si desea cargar otro valor ingresando los caracteres 'S' o 'N'.
 * Mostrar al final la "suma" y el "promedio" de los valores ingresados.
 * Solo se pueden utilizar las siguientes funciones:
 *      printf
 *      scanf
 *      for/while/do while
 *
 */

#include <stdio.h>

//proto
double promedio(int,int);


int main(){
    int input;
    int suma = 0;
    int contador = 0;
    char cont;
    while (cont != 'N' && cont != 'n') 
    {
      printf("Ingrese un numero: ");
      scanf(" %i", &input);
      suma = suma + input;
      contador++;
        do {
          printf("Ingresar otro numero?(S/N) ");
          scanf(" %c", &cont);
  	  if(cont == 'N' || cont == 'n')
	  {
		printf("Salgo. Suma: %i Promedio: %.2lf\n",suma,promedio(suma,contador)); 
		break;
	  }
	} while(cont != 'S' && cont != 's');
    }
    return 0;
}

double promedio(int suma_total,int cantidad){
        return(suma_total/cantidad);
}

