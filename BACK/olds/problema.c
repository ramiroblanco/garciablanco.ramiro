#include <stdio.h>
#include <string.h>

#define NAME_LEN 64
#define MAX_ENTRIES 10

void vacia_buffer(void);
int ingresar(char nombre_apellido[][NAME_LEN], int* edad, int c);
void imprimir_mayores_sesenta(char nombre_apellido[][NAME_LEN], int edad[MAX_ENTRIES], int max);
void imprimir_menores_sesenta(char nombre_apellido[][NAME_LEN], int edad[MAX_ENTRIES], int max);
void imprimir_mayores_promedio(char nombre_apellido[][NAME_LEN], int edad[MAX_ENTRIES], int max);

int main() {
	char nombre_apellido[MAX_ENTRIES][NAME_LEN];
	int edad[MAX_ENTRIES];

	int c=0;
	int out;
	do {
		out = ingresar(nombre_apellido, edad, c);
		if(out==0)c++;
	} while(c < MAX_ENTRIES && out == 0);
	imprimir_mayores_sesenta(nombre_apellido, edad, c);
	imprimir_menores_sesenta(nombre_apellido, edad, c);
	imprimir_mayores_promedio(nombre_apellido, edad, c);
	return 0;
}

int ingresar(char nombre_apellido[][NAME_LEN], int* edad, int c) {

	int o=0;
        printf("Ingrese el nombre y apellido: ");
	char temp[NAME_LEN];
        scanf(" %s", temp);
	if(strcmp(temp,"fin") == 0)
		o = 1;
	else {
		strcpy(nombre_apellido[c],temp);
		//vacia_buffer();
        	printf("Ingrese la edad: ");
        	scanf(" %i", &edad[c]);
		//vacia_buffer();
		printf("N: %s edad: %d\n",nombre_apellido[c], edad[c]);
	}
	return o;
}

void imprimir_mayores_sesenta(char nombre_apellido[][NAME_LEN], int edad[MAX_ENTRIES], int max) {
	printf("Mayores de 60:\n");
	for(int i=0; i < max; i++) {
		if(edad[i] > 60)
			printf("%d %s %d\n",i,nombre_apellido[i],edad[i] );
	}
}
void imprimir_menores_sesenta(char nombre_apellido[][NAME_LEN], int edad[MAX_ENTRIES], int max) {
	printf("Menores de 60:\n");
	for(int i=0; i < max; i++) {
		if(edad[i] <= 60)
			printf("%d %s %d\n",i,nombre_apellido[i],edad[i] );
	}
}
void imprimir_mayores_promedio(char nombre_apellido[][NAME_LEN], int edad[MAX_ENTRIES], int max) {
	int suma=0;
	int c=0;
	for(int i=0; i < max; i++) {
		if(edad[i] > 60){
			suma=suma+edad[i];
			c++;
		}
	}
	printf("Promedio mayores 60: %.2f\n",suma,c, (float) suma/c);
}
void vacia_buffer(void) {
	int ch;
	while((ch = getchar()) != '\n' && ch != EOF);
}
