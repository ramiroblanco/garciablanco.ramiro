#include <stdio.h>

void my_strcopy(char*, char*);
int my_strlen(char*);
void my_strcut(char*,int);
void my_strcat(char*,char*);
void my_strncat(char*,char*,int);

int main() {

	char str[] = "hello world";

	// MY_STRLEN
	printf("MY_STRLEN\n");
	printf("str %s\n", str);
	printf("length str %i\n\n", my_strlen(str));

	// MY_STRCOPY
	printf("MY_STRCOPY\n");
	char str1[my_strlen(str)];
	my_strcopy(str,str1);
	printf("str1 %s\n", str1);
	printf("length str1 %i\n\n", my_strlen(str1));

	// MY_STRCAT
	char str2[] = "string2";
	printf("MY_STRCAT\n");
	my_strcat(str,str2);
	printf("\n");

        // MY_STRNCAT
        printf("MY_STRNCAT ingresa un numero (999 sale).\n");
        int cut;
        while(cut != 999) {
                scanf(" %i",&cut);
                my_strncat(str,str2,cut);
        }

	// MY_STRCUT
	printf("MY_STRCUT ingresa un numero (999 sale).\n");
	int l;
	while(l != 999) {
		scanf(" %i",&l);
		my_strcut(str1,l);
	}
	return 0;
	
}

int my_strlen(char* str) {
	int c=0;
	while (str[c] != '\0')
		c++;
	return c;
}

void my_strcopy(char* str, char* str1) {
	int i;
	for(i=0; str[i]!='\0';i++) 
		str1[i]=str[i];
	str1[i]='\0';
}

void my_strcut(char* str,int l) {
	char str1[l];
	int i;
	for(i=0;i<l;i++)
		str1[i]=str[i];
	str1[i]='\0';
	printf("string cut(%i): %s\n",l,str1);
}

void my_strcat(char* str,char* str1) {
	int length = my_strlen(str)+my_strlen(str1);
	int i = my_strlen(str);
	for(int c=0; i<length; c++) {
		i++;
		str[i]=str1[c];
		printf("_%i",i);
	}
	str[i]='\0';

	printf("string cat(%i): %s\n",length,str);
}

void my_strncat(char* str,char* str1, int cut) {

	char str_result[cut];

	int i;
	for(i=0;i<my_strlen(str) && i<cut;i++)
		str_result[i]=str[i];

	//str1[i]='\0';
	//printf("_____%s\n",str_result);
	int c=0;
	while(i<=cut){
	//str_result[i]=str1[c];
		c++;
		i++;
	}

	str1[i]='\0';

	printf("string ncat(%i): %s\n",cut,str_result);
}
