/*
 * Realizar un programa que permita la carga de valores enteros por teclado.
 * Luego de ingresar el valor mostrar un mensaje por pantalla que pida confirmar
 * al usuario si desea cargar otro valor ingresando los caracteres 'S' o 'N'.
 * Mostrar al final el valor "minimo" y el valor "maximo" de los valores ingresados.
 * Solo se pueden utilizar las siguientes funciones:
 *      printf
 *      scanf
 *      for/while/do while
 *
 */

#include <stdio.h>
#include <limits.h>

double promedio(int,int);

int main(){
    int input;
    int suma = 0;
    int contador = 0;
    int maximo = INT_MIN;
    int minimo = INT_MAX;
    char continuar;
    do
    {
      printf("Ingrese un numero: ");
      scanf(" %i", &input);
      suma = suma + input;
      contador++;
      for(;maximo < input;)
          maximo = input;
      for(;minimo > input;)
          minimo = input;
      do {
          printf("Ingresar otro numero?(S/N) ");
          scanf(" %c", &continuar);
  	  while(continuar == 'N' || continuar == 'n')
	  {
		printf("Salgo. Suma: %i Cantidad: %i Promedio: %.2f Valor minimo: %i Valor maximo: %i\n",
				suma,
				contador,
				promedio(suma,contador),
				minimo,
				maximo);
		break;
	  } 
      } while(continuar != 'S' && continuar != 's');
    } while (continuar != 'N' && continuar != 'n'); 
    return 0;
}

double promedio(int suma_total,int cantidad){
        return (double) suma_total/cantidad;
}


