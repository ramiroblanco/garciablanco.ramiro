#include <stdio.h>

void swap(int*,int*);
int main(){
	int a = 10,b =15;
	printf("a: %i b: %i\n",a,b);
	swap (&a, &b);
	printf("a: %i b: %i\n",a,b);
}

void swap(int *a,int *b){
	int c = *b;	
	*b = *a;
	*a = c;
}

