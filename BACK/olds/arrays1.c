#include <stdio.h>

#define MAX 5

int valor;
int valores[MAX];
int suma;
int mayores = 0;
float promedio;
float get_promedio (int suma, int cantidad);

void main() {

	//for(int i = 0; i < sizeof(valores)/sizeof(int); i++) {
	for(int i = 0; i < MAX; i++) {
		printf("Ingrese valor %i:\n", (i+1));
	        scanf(" %i", &valores[i]);
		suma = suma + valores[i];

	}
	printf("suma: %i\n",suma);
	promedio = get_promedio(suma,MAX);
	printf("promedio: %.2f\n",promedio);
	for(int i = 0; i < MAX; i++) {
		if(valores[i] > promedio) {
			mayores++;
		}
	}
	printf("Valores superiores al promedio = %i\n",mayores);
	return 0;
}

float get_promedio (int suma,int cantidad) {
	return (float) suma/cantidad;
}
