#include <stdio.h>
#include <string.h>

#define MAX 2
#define MAX_STR 15

void print_array_string ( char str[][MAX_STR] , int size);

int main() {

	char str[][MAX_STR] = {"hello world","bye world"};
	char str2[MAX_STR];
	char* str3 = "pointer";

	strcpy(str2,"testing");

	print_array_string(str,MAX);

	int s;
	s = sizeof(str);
	printf("Tamanio str: %d\n",s);
	printf("str2: %s\n\n\n",str2);

	printf("str3: %s\n\n", str3);

	printf("str3: %s\n\n", str3);
	printf("____________________________________________\n");

	char* my_ptr_new[] = {"new","used"};
	printf("Tamanio punteros: %ld\n\n",sizeof(my_ptr_new));
	int total = 0;
	for(int i=0;i<2;i++){
		total += sizeof(my_ptr_new[i]); 
		printf("_%d\n",total);
	}
	printf("Tamanio strings: %d\n\n",total);


}


void print_array_string (char str[][MAX_STR], int size){
	for(int c=0;c < size; c++) {
		for(int i=0;i < MAX_STR; i++) {
			if(str[c][i]== '\0') break;

			printf("str %i %c \n",i,str[c][i]);
		}
		printf("________\n");
	}
}
