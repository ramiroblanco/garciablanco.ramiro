#include <stdio.h>

/* 
 *
 * escribir un programa que permita hacer conversiones entre bases numericas en funcion del argumento con el que se lo invoque.
 * -bin2dec
 * -dec2bin
 * -bin2oct
 * -bin2hex
 * etc
 * una vez invocado el programa se podran recibir hasta 10 numeros en en la base de origen ingresados manualmente por el usuario
 * el programa debera presentar los numeros ordenados de mayor a menor en la base de origen en una columna, y en la base d destino en la siguiente.
 *
 *Nota:considere numeros signados en CA2
 *
 *
 * */

void int_deci(double lista);

int main(int argc, char *argv[]) {

	double valor = 4534.234;
	int_deci(valor);

}


void int_deci(double lista) {

	printf("parte entera: %d \n", (int) lista);
	printf("parte decimal: %f \n", lista -  (int) lista) ;

}


