#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* 
 *
 * Escribir una función que permita validar fechas recibidas como parámetro desde
 * el programa invocante, mediante un puntero a caracter. Se consideran fechas
 * aquellas comprendidas entre 01/01/1900 y 31/12/2100.
 *
 * */

void check_date(char *date);

int main(int argc, char *argv[]) {

	char date[] = "04/08/2009";
	printf("date a validar: %s\n",date);
	check_date(date);

}


void check_date(char *date) {
   	
	char day[3];
	char month[3];
	char year[5];
	int i = 0;
	int numeric_day;
	int numeric_month;
	int numeric_year;

	for(; i < 2; i++)
		day[i] = date[i];
	printf("day: %s \n ", day);
	i++;
	int c = 0;
	for(; i < 5; i++)
	{
		month[c] = date[i];
		c++;
	}
	i++;
	c = 0;
	for(; i < 10; i++)
	{
		year[c] = date[i];
		c++;
	}

	numeric_day = atoi(day);
	numeric_month = atoi(month);
	numeric_year = atoi(year);

	printf("day: %d month: %d year: %d \n",numeric_day,numeric_month,numeric_year);
	// day
	if(numeric_day < 1 || numeric_day > 31) {
		printf("Day erronea.\n");
	}
	
	// month
	if(numeric_month < 1 || numeric_month > 12) {
		printf("Month erronea.\n");
	}
	
	// year
	if(numeric_year < 1900 || numeric_year > 2100) {
		printf("Year erronea.\n");
	}


}


