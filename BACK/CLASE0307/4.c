#include <stdio.h>

/* 
 *
 * Escribir una función que calcule la partes entera y decimal de cualquier número
 * real recibido como argumento, y las retorne al programa invocante. ¿Como se
 * resuelve devolver mas de un resultado?
 *
 * */

void int_deci(double lista);

int main(int argc, char *argv[]) {

	double valor = 4534.234;
	int_deci(valor);

}


void int_deci(double lista) {

	printf("parte entera: %d \n", (int) lista);
	printf("parte decimal: %f \n", lista -  (int) lista) ;

}


