#include <stdio.h>
#include <string.h>

/* 
 *
 Escribir una función que reciba como parámetro una cadena de caracteres que
comienza con espacios en blanco, y los elimine desplazando los caracteres útiles
hacia la izquierda. (operación "left-trim").
Prototipo: void left_trim (char *) ;
Escribir una función que reciba como parámetro una cadena de caracteres que
finaliza con espacios en blanco, y los elimine desplazando los caracteres útiles
hacia la izquierda. (operación "right-trim").
Prototipo: void right_trim (char *) ;
 *
 * */

void trim_left(char *cadena, char *nueva_cadena);
void trim_right(char *cadena, char *nueva_cadena);

int main(int argc, char *argv[]) {

	char cadena[] = "  holahola  ";
	char nueva_cadena[100];
	printf("Cadena: %sEND\n", cadena);
	trim_left(cadena, nueva_cadena);
	printf("Nueva cadena: %sEND\n", nueva_cadena);

	strcpy(cadena,"holahola  ");
	trim_right(cadena, nueva_cadena);
	printf("Nueva cadena: %sEND\n", nueva_cadena);


}

void trim_left(char *cadena, char *nueva_cadena) {

	int length = strlen(cadena);
	int c = 0;
	//printf("_ %d _ %d", c,length );
	while(c < length) {
		if(cadena[c] != 32) 
			break;
		else
			c++;
	}
	for(int i = 0; i <= length; i++ ){
		nueva_cadena[i] = cadena[c];
		c++;

	}
	//nueva_cadena[c] = '\0';
	
}
void trim_right(char *cadena, char *nueva_cadena) {

	int length = strlen(cadena);
	int c = 0;
	int i = 0;
	while(i < length) {
		if(cadena[i] != 32) {
			nueva_cadena[c] = cadena[i];
			c++;
		}
		i++;
	}
	nueva_cadena[c] = '\0';
}
