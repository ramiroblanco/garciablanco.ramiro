#include <stdio.h>
#include <stdlib.h>



int main() {


	int var,var2;
	int *p1;
	int **p2;

	var = 53;
	var2 = 67;

	printf("var %i\n",var);
	printf("&var %d\n",&var);

	p1 = &var;

	printf("p1 %d\n",p1);
	printf("*p1 %d\n",*p1);
	printf("&p1 %d\n",&p1);

	p2 = &p1;

	printf("p2 %d\n",p2);
	printf("*p2 %d\n",*p2);
	printf("**p2 %d\n",**p2);
	printf("&p2 %d\n",&p2);

	printf("Arrays__________________\n");
	printf("p1[0] %d\n",p1[0]);
	printf("*(p1+0) %d\n",*(p1+0));
	printf("&p1[0] %d\n",&p1[0]);

	printf("\n");

	printf("p2 %d\n",p2);
	printf("p2[0] %d\n",p2[0]);
	printf("*(p2+0) %d\n",*(p2+0));
	printf("&p2[0] %d\n",&p2[0]);

	return 0;
}
