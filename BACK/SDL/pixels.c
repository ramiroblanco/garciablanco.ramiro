/*
 *
 * gcc 1 1.c -lSDL2 -lSDL2_image -lGL
 * gcc -o gl gl.c  -lGLEW -lGL -lglfw
 *
 */


#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>


int main() {

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hola hola hola!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			1024,780,
			SDL_WINDOW_OPENGL);


	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);


	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;

	/*live*/
	int live = 1;

	/*event*/
	SDL_Event event;

	/*user pos / view */
	float user_x = 0;
	float user_y = 200;
	float user_z = 0;
	float user_angle_x = 0;
	float user_angle_y = 0;


	/*fps*/
	Uint32 then, now, frames;

	/*random*/
	float circ_y;
	float circ_x;
	float circ_angle = 0;
	int radius = 100, i,j;

	while(live) {

		frames = 0;
		then = SDL_GetTicks();

		while(SDL_PollEvent(&event))
			switch(event.type) {
				case SDL_QUIT:
					live = 0;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
					}
					break;
			}
		/*process keys*/
		if(k_up) user_x += 5;
		if(k_down) printf("");
		if(k_left) printf("left\n");
		if(k_right) printf("right\n");

		//Render clear
		//SDL_SetRenderDrawColor(renderer, 0, 5, 0, SDL_ALPHA_OPAQUE);
                //SDL_RenderClear(renderer);

		//circle
		circ_angle++;
		for(i = 1;i<=1024;i++) {
			for(j = 1;j<=780;j++) {
			//circ_y = ( ( radius + i ) * sin(circ_angle*M_PI/180) ) + 300 + i;
			//circ_x = ( ( radius + i ) * cos(circ_angle*M_PI/180) ) + 300 + i;
			//printf("_%f_%f_%i\n",circ_x, circ_y,circ_angle);
			SDL_SetRenderDrawColor(renderer, i+user_x, j+user_x, i-user_x, SDL_ALPHA_OPAQUE);

			SDL_RenderDrawPoint(renderer, i, j);
			}
		}

		
		//Render 
                SDL_RenderPresent(renderer);

	
		/*now = SDL_GetTicks();
		if (now > then) {
		printf("%2.2f frames per second\n",
		       ((double) frames * 1000) / (now - then));
		} */

		/*beats*/
		SDL_Delay(1000/60);
	}



	//SDL clean
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
