/*
 *
 *
 */

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#define MAP_MAX_X 67
#define MAP_MAX_Y 10
#define MAP_SCALE 100

int main() {

	int win_w = 640;
	int win_h = 480;

	/*MAP 1*/
	char map1[MAP_MAX_Y][MAP_MAX_X] = {
		{"###################################################################"},
		{"#.................................................................#"},
		{"#...........................##############........................#"},
		{"#...........................##############........................#"},
		{"#...........................##############........................#"},
		{"#...........................##############........................#"},
		{"#.................................................................#"},
		{"#.................................................................#"},
		{"#.................................................................#"},
		{"###################################################################"},
	};

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Test raycasting!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_w,win_h,
			SDL_WINDOW_OPENGL);


	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;
	int k_f = 0;

	/*live*/
	int live = 1;

	int r,w,w_start = win_w/2;

	/*user*/
	float user_x = 420;
	float user_y = 420;
	float user_dir_x = 0.7071f;
	float user_dir_y = 0.7071f;
	float ray_y,ray_x;

	/*event*/
	SDL_Event event;


	while(live) {

		while(SDL_PollEvent(&event))
			switch(event.type) {
				case SDL_QUIT:
					live = 0;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_F: k_f = 1; break;
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
					}
					break;
			}

		/*process keys*/
		if(k_up);

		if(k_down);

		if(k_left)  { 
			//if(user_dir_y == 0) user_dir_y = 1;
			user_dir_y = (user_dir_x + 0.01f) * user_dir_y / user_dir_x;
			user_dir_x += 0.01f;
			printf("udx %f udy %f\n",user_dir_x,user_dir_y);
		}

		if(k_right) user_dir_x += 0.01f;

		//Render clear
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);


		SDL_SetRenderDrawColor(renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);
		for(w=w_start*-1;w<=w_start;w+=5) {
		
			if( w == 0 || user_dir_x == 0 ) ray_y = 1; else ray_y = (float)  user_dir_x * w / w_start;
			if( w == 0 || user_dir_y == 0 ) ray_x = 1; else ray_x = (float)  user_dir_y * w_start / w * ray_y;
			//printf("ray x %f ray y %f\n",ray_x,ray_y);
		

			for(r=w_start;r<3000;r++) {
				SDL_RenderDrawPoint(renderer, (user_x + ray_x * r)/10, (user_y + ray_y * r)/10);
			}
		
		}



		SDL_SetRenderDrawColor(renderer, 250, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawPoint(renderer, user_x / 10, user_y / 10);

		//Render 
                SDL_RenderPresent(renderer);
	
		SDL_Delay(1000/29);
	}



	//SDL clean
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
