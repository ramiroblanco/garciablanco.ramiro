#include <stdio.h>
#include <math.h>

void loadSinValues(float sinVals[], float slice);

int main() {
	float sinVals[36000];
	float cosVals[36000];
	float tanVals[36000];
	loadSinValues(sinVals, 0.01f);


	for (int i = 0; i <= 360/0.01;i+=1) {
	
		printf("i: %i a: %f\n",i,sinVals[i]);
	
	}

}

void loadSinValues(float sinVals[], float slice) {
	for(int angle = 360/slice; angle >= 0; angle-=1) {
		sinVals[angle] = sinf((float) angle*slice*M_PI/180);
	}
}
void loadCosValues(float cosVals[], float slice) {
	for(int angle = 360/slice; angle >= 0; angle-=1) {
		cosVals[angle] = cosf((float) angle*slice*M_PI/180);
	}
}
void loadTanValues(float tanVals[], float slice) {
	for(int angle = 360/slice; angle >= 0; angle-=1) {
		tanVals[angle] = tanf((float) angle*slice*M_PI/180);
	}
}
