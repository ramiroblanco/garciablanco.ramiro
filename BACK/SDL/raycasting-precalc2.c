/*
 *
 * gcc -o raycasting-precalc raycasting-precalc2.c -lSDL2 -lSDL2_image -lm  -lSDL2_mixer
 *
 */

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_mixer.h>

#define MAP_MAX_X 170
#define MAP_MAX_Y 200
#define MAP_SCALE 100
#define ANG_ROT 0.1f
#define WALL_SCALE 10
#define MAX_FPS 20 
#define MAX_SHADES 30 
#define TEXTURES 7 

void loadCosValues(float *cosVals, float angles);
void loadSinValues(float *sinVals, float angles);
void loadTanValues(float *tanVals, float angles);

int main() {

	printf("STARTING...\n");

	int win_w = 640; int win_h = 360;
	//int win_w = 1280; int win_h = 720;

	/*MAP 1*/
	char map1[MAP_MAX_Y][MAP_MAX_X] = {
		{"##############################"},
		{"C................B...........C"},
		{"C............................C"},
		{"C......U.........B...........C"},
		{"C................BBB.........C"},
		{"C................BBBB........C"},
		{"C................BBBB........C"},
		{"C................BBBB........C"},
		{"WWWWWWWWWWWWWWWWWWWWW........C"},
		{"WWWWWWWWWWWWWWWWWWWWW........C"},
		{"WWWWWWWWWWWWWWWWWWWWW........C"},
		{"C......#.....................C"},
		{"C...#..#.....................C"},
		{"C...#.........#BBBBBBBBBBBBBBC"},
		{"C...###########BBBBBBBBBBBBBBC"},
		{"C.............#BBBBBBBBBBBBBBC"},
		{"C............................C"},
		{"C..B...B...B...B...BB.BBBBBBBC"},
		{"C..B...B...B...B...B.........C"},
		{"C..B...B...B...B...B........UC"},
		{"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"}
	};

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Test raycasting!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_w,win_h,
			SDL_WINDOW_OPENGL);
			//SDL_WINDOW_FULLSCREEN);
			//0);

	/*MOUSE relative*/
	SDL_SetRelativeMouseMode(SDL_TRUE);

	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

        SDL_Surface * all_walls_surface = SDL_CreateRGBSurface(0, win_w, win_h, 32, 0, 0, 0, 0);

	SDL_SetSurfaceBlendMode(all_walls_surface, SDL_BLENDMODE_NONE);
        SDL_Texture * all_walls_texture;

	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);


	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;
	int k_strafe_right = 0;
	int k_strafe_left= 0;
	float mouse_deltaX = 0;
	float mouse_deltaY = 0;

	/*live*/
	int live = 1;

	/*event*/
	SDL_Event event;

	/*user pos / view */
	float user_x_tmp, user_x_col, user_x = 400.0f;
	float user_y_tmp, user_y_col, user_y = 400.0f;
	int user_angle = 1900;
	int fov_angle = 900;

	/*iteration*/
	int i,j; //Usos multiples
	float it_angle;
	int it_width;
	int it_height;
	float it_ray_dis,it_ray_step;
	float it_x = 0;
	float it_y_tmp = 0;
	float it_y = 0;
	float it_cos;
	float it_sin;

	/*map*/
	int it_map_x;
	int it_map_y;
	int it_scale;

	const int f_near = win_w/2;

	int it_ray_max_sight = 300;

	int angles = 360/ANG_ROT*2;

	printf("Caching resources.\n");
	float sinVals[angles];
	loadSinValues(sinVals,angles);
	float cosVals[angles];
	loadCosValues(cosVals,angles);
	float tanVals[angles];
	loadTanValues(tanVals,angles);
	printf("Loading rays.\n");
        float atanValues[win_w];
	for(it_width = win_w; it_width>=0;it_width-=1) { 
		atanValues[it_width] = (float) atan((double) (((float)win_w/2 - it_width)/f_near)) * 180/M_PI * (360/ANG_ROT) / 360 * -1;
	}

        printf("Loading sprites.\n");
	SDL_Rect srcrect;
	SDL_Rect dstrect;

	struct st_cacodemon {
		float x;
		float y;
		int width;
		int half_width;
		int height;
		SDL_Surface * surface;
		//SDL_Texture * cacodemon = SDL_CreateTextureFromSurface(renderer, surface);
		//SDL_FreeSurface(surface);
	};
	struct st_cacodemon ST_CACODEMON = { 30.0f, 20.0f, 116, 58, 225, SDL_LoadBMP("sprites/zombie.bmp")};
	SDL_SetColorKey (ST_CACODEMON.surface, SDL_TRUE, SDL_MapRGB (ST_CACODEMON.surface->format, 0, 255, 255)); 

	/*hud*/
        SDL_Surface * hand[1]; 
	SDL_Rect handrect;
	handrect.x = win_w/2;
	handrect.y = win_h/2;
	handrect.w = win_w/2;
	handrect.h = win_h/2;
	hand[0] = SDL_LoadBMP("sprites/handgun.bmp");
	SDL_SetColorKey (hand[0], SDL_TRUE, SDL_MapRGB (hand[0]->format, 0, 255, 255)); 
	//fprintf (stderr, "LoadSprite: Unable to set color key (0x%X)\n", SDL_MapRGB (SDL_GetWindowPixelFormat(win), 255, 0, 255));



        SDL_Surface * image[TEXTURES][MAX_SHADES]; 
	for(i = 0; i < MAX_SHADES;i++) { 
		image[0][i] = SDL_LoadBMP("walls/wall5.bmp");
		image[1][i] = SDL_LoadBMP("walls/ulises.bmp");
		image[2][i] = SDL_LoadBMP("walls/horacio2.bmp");
		image[3][i] = SDL_LoadBMP("walls/wall2.bmp");
		image[4][i] = SDL_LoadBMP("walls/wall3.bmp");
		image[5][i] = SDL_LoadBMP("walls/wall4.bmp");
		image[6][i] = SDL_LoadBMP("walls/ulises.bmp");
	}

	for(j = 1; j< MAX_SHADES; j++)  
		for(i = 0;i<TEXTURES; i++) 
			SDL_SetSurfaceColorMod(image[i][j],
					(int)(255/MAX_SHADES*(MAX_SHADES - j)), 
					(int)(255/MAX_SHADES*(MAX_SHADES - j)), 
					(int)(255/MAX_SHADES*(MAX_SHADES - j)));

	printf("Loading walls.\n");
	float depth_buffer[win_w];
	char grid_code;

	struct wall_h {
		float wall_t[it_ray_max_sight];
		float wall_b[it_ray_max_sight];
		float ray_corrected_dist[angles];
		float wall_bright[it_ray_max_sight];
	} wall_h1;
	for(it_ray_dis = 0; it_ray_dis<=it_ray_max_sight;it_ray_dis++) { 
			wall_h1.wall_t[(int)it_ray_dis] =  win_h/2 - 5000 / it_ray_dis;
			wall_h1.wall_b[(int)it_ray_dis] =  5000 / it_ray_dis * 2;
	}
	int shades[it_ray_max_sight];
	for(it_ray_step = 0; it_ray_step<=it_ray_max_sight;it_ray_step++) { 
		shades[(int)it_ray_step] = (int)(it_ray_step*(MAX_SHADES-1)/it_ray_max_sight);
	}

	/* music sounds */
	Mix_OpenAudio(22050, AUDIO_S16SYS, 2, 640);
	Mix_Music *music = Mix_LoadMUS("songs/song1.mid"); //https://onlinesequencer.net/1597410
	Mix_PlayMusic(music, -1);

	/*frames*/
	Uint32 startTime = 0;
	Uint32 endTime = 0;
	Uint32 delta = 0;
	short fps = 10;
	short timePerFrame = 2; // miliseconds

	printf("Done loading.\n");

	while(live) {

		startTime = SDL_GetTicks();

		SDL_FreeSurface(all_walls_surface);
		all_walls_surface = SDL_CreateRGBSurface(0, win_w, win_h, 32, 0, 0, 0, 0);

		while(SDL_PollEvent(&event))
			switch(event.type) {
				/*MOUSE*/
				case SDL_MOUSEMOTION:
					mouse_deltaX = event.motion.xrel;
					//mouse_deltaY += event.motion.y; // no por ahora	
				break;
				/*teclado*/
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
						case SDL_SCANCODE_A: k_strafe_left = 1; break;
						case SDL_SCANCODE_D: k_strafe_right = 1; break;
					}
				break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
						case SDL_SCANCODE_A: k_strafe_left = 0; break;
						case SDL_SCANCODE_D: k_strafe_right = 0; break;
					}
				break;
				case SDL_QUIT:
					live = 0;
				break;
			}

		/*process keys*/
		if(k_up) {
			user_y_tmp = user_y;
			user_y_col = 2.2f*delta*sinVals[user_angle] + user_y;
			user_y = 0.6f*delta*sinVals[user_angle] + user_y;
			if(map1[(int)(user_x/32)][(int)(user_y_col/32)] != 46 ) user_y = user_y_tmp;
			user_x_tmp = user_x;
			user_x_col = 2.2f*delta*cosVals[user_angle] + user_x;
			user_x = 0.6f*delta*cosVals[user_angle] + user_x;
			if(map1[(int)(user_x_col/32)][(int)(user_y/32)] != 46 ) user_x = user_x_tmp;
		}

		if(k_down) {
			user_y = user_y - (float) 0.6f*delta*sinVals[user_angle];
			user_x = user_x - (float) 0.6f*delta*cosVals[user_angle];
		}

		if(mouse_deltaX != 0) { 
			user_angle += 10*mouse_deltaX;
			if(user_angle < 1800) user_angle += 360/ANG_ROT;
			else if(user_angle > 5400) user_angle -= 360/ANG_ROT;
			mouse_deltaX = 0;
		}

		if(k_left) { 
			user_angle -= 65;
			if(user_angle < 1800) user_angle += 360/ANG_ROT;
		}
		if(k_right) {
			user_angle += 65;
			if(user_angle > 5400) user_angle -= 360/ANG_ROT;
		}

		if(k_strafe_left) { 
			user_y = user_y - (float) 0.7f*delta*sinVals[user_angle+900];
			if(map1[(int)(user_x/32)][(int)(user_y/32)] != 46 ) user_y = user_y + (float) 0.7f*delta*sinVals[user_angle+900];
			user_x = user_x - (float) 0.7f*delta*cosVals[user_angle+900];
			if(map1[(int)(user_x/32)][(int)(user_y/32)] != 46 ) user_x = user_x + (float) 0.7f*delta*cosVals[user_angle+900];
		}
		if(k_strafe_right) {
			user_y = user_y + (float) 0.7f*delta*sinVals[user_angle+900];
			if(map1[(int)(user_x/32)][(int)(user_y/32)] != 46 ) user_y = user_y - (float) 0.7f*delta*sinVals[user_angle+900];
			user_x = user_x + (float) 0.7f*delta*cosVals[user_angle+900];
			if(map1[(int)(user_x/32)][(int)(user_y/32)] != 46 ) user_x = user_x - (float) 0.7f*delta*cosVals[user_angle+900];
		}

		//Render clear
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);

		SDL_SetRenderDrawColor(renderer, 0, 200, 0, 100);

		/*MAIN LOOP RAYCASTER*/
		for(it_width = 0; it_width<=win_w;it_width+=1) { 

			it_angle = user_angle + atanValues[it_width];
			//printf("%i\n",user_angle);

			for(it_ray_dis = 5; it_ray_dis <= it_ray_max_sight; it_ray_dis++) {
				depth_buffer[it_width] = it_ray_max_sight; 
				it_x = (float)(user_x + cosVals[(int)it_angle] * it_ray_dis)/32;
				if(it_x > MAP_MAX_X || it_x < 0) continue;
				it_y = (float)(user_y + sinVals[(int)it_angle] * it_ray_dis)/32;
				if(it_y > MAP_MAX_Y || it_y < 0) continue;

				grid_code = map1[(int)it_x][(int)it_y];
				if(grid_code != 46) {
					if((int)it_y_tmp != (int)it_y)
						if(user_angle > 1800 && user_angle < 3600)
							srcrect.x = (it_x-(int)it_x)*320;
						else
							srcrect.x = (1-(it_x-(int)it_x))*320;
					else
						if(user_angle > 2700 && user_angle < 4500)
							srcrect.x = (it_y-(int)it_y)*320;
						else
							srcrect.x = (1-(it_y-(int)it_y))*320;
					srcrect.y = 0;
					srcrect.w = 1;
					srcrect.h = 320;
					dstrect.x = it_width;
					dstrect.y = wall_h1.wall_t[abs((int)(it_ray_dis*cosVals[(int)((abs(atanValues[it_width])))]))];
					dstrect.w = 1;
					dstrect.h = wall_h1.wall_b[(int)(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]))])];
					switch(grid_code) {
						case 35:
							SDL_BlitScaled(image[0][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 77:
							SDL_BlitScaled(image[2][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 87:
							SDL_BlitScaled(image[3][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 67:
							SDL_BlitScaled(image[4][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 66:
							SDL_BlitScaled(image[5][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 85:
							SDL_BlitScaled(image[6][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
					}
					depth_buffer[it_width] = it_ray_dis; 
					break;
				}
					it_y_tmp = it_y;
			}
		} 
		/*sprites*/
			SDL_BlitScaled(hand[0], NULL, all_walls_surface, &handrect);
			
			it_ray_dis = 100;
			it_width = 200;
		for(i = 0; i <= ST_CACODEMON.width;i++) {
			srcrect.x = i;
			srcrect.y = 0;
			srcrect.w = 1;
			srcrect.h = ST_CACODEMON.height;
			dstrect.x = it_width-ST_CACODEMON.half_width + i;
			dstrect.y = wall_h1.wall_t[abs((int)(it_ray_dis*cosVals[(int)((abs(atanValues[it_width])) )]))];
			dstrect.w = 1;
			dstrect.h = wall_h1.wall_b[(int)(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]) )])];
			if(it_ray_dis > depth_buffer[it_width-ST_CACODEMON.half_width + i]) continue;
			if(SDL_BlitScaled(ST_CACODEMON.surface, &srcrect, all_walls_surface, &dstrect))
				printf("%s\n",SDL_GetError());
		}


		//RENDER COPY
		srcrect.x = 0;
		srcrect.y = 0;
		srcrect.w = win_w;
		srcrect.h = win_h;
		SDL_DestroyTexture(all_walls_texture);
		all_walls_texture = SDL_CreateTextureFromSurface(renderer, all_walls_surface);
		SDL_RenderCopy(renderer, all_walls_texture, &srcrect, &srcrect);

		//ceiling
		//for(it_ray_step = win_h/2;it_ray_step>=0;it_ray_step--) {
		//	SDL_SetRenderDrawColor(renderer, 255.0f/win_h/2*it_ray_step, 0, 255.0f/win_h/2*it_ray_step,SDL_ALPHA_OPAQUE);
		//	SDL_RenderDrawLine(renderer, 0, win_h/2 - it_ray_step, win_w, win_h/2 - it_ray_step);
		//}

		//Render 
                SDL_RenderPresent(renderer);
	
		// if less than 16ms, delay
		endTime = SDL_GetTicks();

		delta = endTime - startTime; // how many ms for a frame
		if (delta < 1000/MAX_FPS) {
			SDL_Delay(1000/MAX_FPS - delta);
			//printf("FPS is: %f \n", (float)delta*(float)MAX_FPS/1000);
			//printf("FPS is: %f \n", (float)delta);
		}

		// if delta is bigger than 16ms between frames, get the actual fps
		//if (delta > timePerFrame) {
		//	fps = 1000 / delta;
		//	printf("FPS is: %i \n", fps);
		//}


		//startTime = endTime;
		//endTime = SDL_GetTicks();
	}

	//SDL clean
	Mix_FreeMusic(music);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}

void loadCosValues(float *cosVals, float angles) {
        for(int angle = 0; angle < angles; angle++) {
                cosVals[angle] = cosf((float) angle*ANG_ROT*M_PI/180);
        }
}
void loadSinValues(float *sinVals, float angles) {
        for(int angle = 0; angle < angles; angle++) {
                sinVals[angle] = sinf((float) angle*ANG_ROT*M_PI/180);
        }
}
void loadTanValues(float *tanVals, float angles) {
        for(int angle = 0; angle < angles; angle++) {
                tanVals[angle] = tanf((float) angle*ANG_ROT*M_PI/180);
        }
}

