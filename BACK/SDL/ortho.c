/*
 *
 *
 */

#include <stdio.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#define FOV 100

void DrawLine(float, float, float, float, SDL_Renderer*);

/*structs*/

struct mesh {
	int vertex_count;
	float angle;
	float v[][3];
};

struct matrix {
	float m44[4][4];// = { 0 };
};

const int win_h = 480, win_w = 640;

/*projection Matrix*/
/*Orthographic*/
float orthoMatrix[][3] = {
	{ 1, 0, 0 },
	{ 0, 1, 0 },

};


void LoadMesh(struct mesh *m, float[], int);
void MatrixMultiplication (float *dest_v, float projection[][3], float v[], int, int );
void RotateAxis(char axis, struct mesh *mesh, struct mesh *dest);

int main() {


	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hola mundo 3D!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_w,win_h,
			//SDL_WINDOW_OPENGL);
			SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN);

	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	/*event*/
        SDL_Event event;


	/*live*/
	int i,live = 1;

	//Uint32 SDL_GetTicks(void)
	//uint32_t tick_time = SDL_GetTicks();

	/*cube*/
	struct mesh *cube = malloc( sizeof(int) + sizeof(float) + sizeof(float[3]) * 8);
	struct mesh *cube2 = malloc( sizeof(int) + sizeof(float) + sizeof(float[3]) * 8);
	cube->angle = 0.0f;
	float mesh_tmp[] = { 
			-50 , -50, -50, 
       		         50 , -50, -50, 
	                 50 ,  50, -50, 
		        -50 ,  50, -50, 
			-50 , -50, 50, 
       		         50 , -50, 50, 
	                 50 ,  50, 50, 
		        -50 ,  50, 50
	};
	LoadMesh(cube,mesh_tmp,8);



	while(live) {
                while(SDL_PollEvent(&event))
                        switch(event.type) {
                                case SDL_QUIT:
                                        live = 0;
                                        break;
			}


		//Render clear
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);

		SDL_SetRenderDrawColor(renderer, 0, 250, 0, SDL_ALPHA_OPAQUE);

		//Rotate (altero mesh orginal) 
		RotateAxis('X',cube,cube);
		RotateAxis('Y',cube,cube);
		RotateAxis('Z',cube,cube);

		//Project (mesh buffer)
		for(i = 0; i<cube->vertex_count;i++) MatrixMultiplication(cube2->v[i], orthoMatrix, cube->v[i], 2, 3);

		//Draw cube
		for(i=0;i<4;i++) {
				SDL_RenderDrawLine(renderer, cube2->v[i][0] + win_w/2, cube2->v[i][1] + win_h/2, cube2->v[(i+1)%4][0] + win_w/2, cube2->v[(i+1)%4][1] + win_h/2);
				SDL_RenderDrawLine(renderer, cube2->v[i+4][0] + win_w/2, cube2->v[i+4][1] + win_h/2, cube2->v[((i+1)%4) + 4][0] + win_w/2, cube2->v[((i+1)%4 ) + 4][1] + win_h/2);
				SDL_RenderDrawLine(renderer, cube2->v[i][0] + win_w/2, cube2->v[i][1] + win_h/2, cube2->v[i+4][0] + win_w/2, cube2->v[i+4][1] + win_h/2);
		}
		//Render 
                SDL_RenderPresent(renderer);
	
		/*beats*/
		SDL_Delay(1000/60);

		cube->angle = 0.02f;

	}

	//SDL clean
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}


void LoadMesh(struct mesh *m, float ver[], int count) {

	int i, c = 0;
	for(i=0;i<count;i++) {
		m->v[i][0] = ver[c];
		m->v[i][1] = ver[c+1];
		m->v[i][2] = ver[c+2];
		c+=3;
	}
	m->vertex_count = i;
}

void RotateAxis(char axis, struct mesh *mesh, struct mesh *dest) {

	float rotationMatrix[mesh->vertex_count][3];

	switch (axis) {
		case 'Y':
		case 'y': ;
			rotationMatrix[0][0] = cosf(mesh->angle);
			rotationMatrix[0][1] = 0;
			rotationMatrix[0][2] = -sinf(mesh->angle);
			rotationMatrix[1][0] = 0;
			rotationMatrix[1][1] = 1;
			rotationMatrix[1][2] = 0;
			rotationMatrix[2][0] = sinf(mesh->angle);
			rotationMatrix[2][1] = 0;
			rotationMatrix[2][2] = cosf(mesh->angle);
			for(int i = 0; i<mesh->vertex_count;i++) MatrixMultiplication(mesh->v[i], rotationMatrix, dest->v[i], 3, 3); 

		break;
		case 'Z':
		case 'z': ;
			rotationMatrix[0][0] = cosf(mesh->angle);
			rotationMatrix[0][1] = -sinf(mesh->angle);
			rotationMatrix[0][2] = 0;
			rotationMatrix[1][0] = sinf(mesh->angle);
			rotationMatrix[1][1] = cosf(mesh->angle);
			rotationMatrix[1][2] = 0;
			rotationMatrix[2][0] = 0;
			rotationMatrix[2][1] = 0;
			rotationMatrix[2][2] = 1;
			for(int i = 0; i<mesh->vertex_count;i++) MatrixMultiplication(mesh->v[i], rotationMatrix, dest->v[i], 3, 3); 

		break;
		case 'X':
		case 'x': ;
			rotationMatrix[0][0] = 1;
			rotationMatrix[0][1] = 0;
			rotationMatrix[0][2] = 0;
			rotationMatrix[1][0] = 0;
			rotationMatrix[1][1] = cosf(mesh->angle);
			rotationMatrix[1][2] = -sinf(mesh->angle);
			rotationMatrix[2][0] = 0;
			rotationMatrix[2][1] = sinf(mesh->angle);
			rotationMatrix[2][2] = cosf(mesh->angle); 
			for(int i = 0; i<mesh->vertex_count;i++) MatrixMultiplication(mesh->v[i], rotationMatrix, dest->v[i], 3, 3); 

		break;
		default:
			return;
	}
}

void MatrixMultiplication (float *dest_v, float projection[][3], float v[], int r, int c) {

	int j;
	float row_add[r];
	for(int i = 0; i<r;i++) {
		row_add[i] = 0;
		for(j = 0; j<c;j++) {
			row_add[i] += projection[i][j] * v[j];
	
		}
	}
	for(j = 0; j<r;j++) dest_v[j] = row_add[j];
}
