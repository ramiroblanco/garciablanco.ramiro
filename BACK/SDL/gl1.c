#include<stdio.h>
#include<GL/glew.h>
#include<GLFW/glfw3.h>


int main() {

	GLFWwindow *window;

	int win_w = 640;
	int win_h = 480;


	if(!glfwInit()){
		printf("Error creando window.\n");
		return 1;	
	}

	window = glfwCreateWindow(win_w,win_h,"test1",NULL,NULL);

	if(!window) {
		glfwTerminate();
		printf("Error creando window.\n");
		return 1;	
	}

	glfwMakeContextCurrent(window);

	glViewport(0.0f,0.0f,win_w,win_h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,win_w,0,win_h,0,1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat vertices[] = 
	{
		10,40,0,
		40,40,0,
		40,10,0,
		10,10,0
	};

	GLfloat pointVertex[] = { win_w / 2, win_h / 2 };
	float i = 0;
	while(!glfwWindowShouldClose(window)) {
	
		//glClearColor(0.0,0.0,0.0,0.0);
		glClear(GL_COLOR_BUFFER_BIT);

		//draw
		glEnableClientState(GL_VERTEX_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, vertices);
		glDrawArrays(GL_QUADS, 0, 11);

		pointVertex[0] = (win_w / 2) + i;
		pointVertex[1] = (win_h / 2) + i;
		i+=0.5;
		glPointSize(50);
		glVertexPointer(2, GL_FLOAT, 0 ,pointVertex);
		glDrawArrays(GL_POINTS, 0, 2);

		glDisableClientState(GL_VERTEX_ARRAY);
		
		//swap front and back buffers
		glfwSwapBuffers(window);

		//user input
		glfwPollEvents();

	}

	glfwTerminate();
	return 0;
}
