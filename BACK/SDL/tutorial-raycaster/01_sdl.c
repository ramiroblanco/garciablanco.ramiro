#include <stdio.h>
#include <SDL2/SDL.h>

int main() {

	int live = 1;
	int win_w = 640, win_h = 480;
	int k_up = 0, k_down = 0, k_left = 0, k_right = 0;

	if(SDL_Init(SDL_INIT_VIDEO) != 0) {
		printf("Error: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Raycaster Tutorial!", 
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_w, win_h,
			0);

	if(!win) {
		printf("Error: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(win, -1, 0);
	SDL_Event event;

	while(live) {

		while(SDL_PollEvent(&event)) {
			switch(event.type) {
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W: k_up = 1;break;
						case SDL_SCANCODE_S: k_down = 1;break;
						case SDL_SCANCODE_A: k_left = 1;break;
						case SDL_SCANCODE_D: k_right = 1;break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W: k_up = 0;break;
						case SDL_SCANCODE_S: k_down = 0;break;
						case SDL_SCANCODE_A: k_left = 0;break;
						case SDL_SCANCODE_D: k_right = 0;break;
					}
					break;
				case SDL_QUIT:
					live = 0;
					break;
			}

		}

		//handle keys
		if(k_up) printf("Up pressed\n");
		if(k_down) printf("Down pressed\n");
		if(k_left) printf("Left pressed\n");
		if(k_right) printf("Right pressed\n");

		SDL_SetRenderDrawColor(renderer,0,0,0,SDL_ALPHA_OPAQUE);
		SDL_RenderClear(renderer);
		SDL_SetRenderDrawColor(renderer,0,255,0,SDL_ALPHA_OPAQUE);
		SDL_RenderDrawLine(renderer, 20,40,600,400);
		SDL_RenderPresent(renderer);
		SDL_Delay(50);
	}

	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
