/*
	idemscripten_set_main_loop_arg(frame, NULL, -1, 1);
 *
 * export SAFE_HEAP=1; emcc raycasting-precalc.c -O2 -s USE_SDL=2 -s USE_SDL_IMAGE=2 -s SDL2_IMAGE_FORMATS='["bmp"]' -s USE_SDL_MIXER=2 -s USE_SDL_TTF=2 --preload-file assets  -o raycaster.html
 *
 */
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <emscripten.h>

#define WIN_W 425
#define WIN_H 240


/*main loop*/
int live = 1;

/* input tracking */
int k_up = 0;
int k_down = 0;
int k_left = 0;
int k_right = 0;
int k_strafe_right = 0;
int k_strafe_left= 0;
float mouse_deltaX = 0;
float mouse_deltaY = 0;

SDL_Window *win;
SDL_Renderer *renderer;
SDL_Event event;
SDL_Surface *all_walls_surface;
SDL_Texture *all_walls_texture;

void frame();

int main() {

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {

	printf("Error iniciando: %s\n", SDL_GetError());
	return 1;
        }

        win = SDL_CreateWindow("Test raycasting!",
                        SDL_WINDOWPOS_CENTERED,
                        SDL_WINDOWPOS_CENTERED,
                        WIN_W,WIN_H,
                        SDL_WINDOW_OPENGL);
                        //SDL_WINDOW_FULLSCREEN);
                        //0);

        if(!win) {
                printf("Error creando ventana: %s\n", SDL_GetError());
                return 1;
        }

        renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        //renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_PRESENTVSYNC);
        //SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED);

        //all_walls_surface = SDL_CreateRGBSurface(0, WIN_W, WIN_H, 32, 0, 0, 0, 0);
	all_walls_surface = SDL_LoadBMP("assets/handgun.bmp");
	SDL_SetColorKey (all_walls_surface, SDL_TRUE, SDL_MapRGB (all_walls_surface->format, 0, 255, 255));

	//while(live)frame();
	emscripten_set_main_loop(frame, 5, 1);

        //SDL clean
        //Mix_FreeMusic(music);
        SDL_DestroyWindow(win);
        SDL_Quit();
        return 0;

}

void frame() {

                while(SDL_PollEvent(&event))
                        switch(event.type) {
                                //MOUSE
                                case SDL_MOUSEMOTION:
                                        mouse_deltaX = event.motion.xrel;
                                        //mouse_deltaY += event.motion.y; // no por ahora       
                                break;
                                //teclado
                                case SDL_KEYDOWN:
                                        switch(event.key.keysym.scancode) {
                                                case SDL_SCANCODE_W:
                                                case SDL_SCANCODE_UP: k_up = 1; break;
                                                case SDL_SCANCODE_LEFT: k_left = 1; break;
                                                case SDL_SCANCODE_S:
                                                case SDL_SCANCODE_DOWN: k_down = 1; break;
                                                case SDL_SCANCODE_RIGHT: k_right = 1; break;
                                                case SDL_SCANCODE_A: k_strafe_left = 1; break;
                                                case SDL_SCANCODE_D: k_strafe_right = 1; break;
                                        }
                                break;
                                case SDL_KEYUP:
                                        switch(event.key.keysym.scancode) {
                                                case SDL_SCANCODE_W:
                                                case SDL_SCANCODE_UP: k_up = 0; break;
                                                case SDL_SCANCODE_LEFT: k_left = 0; break;
                                                case SDL_SCANCODE_S:
                                                case SDL_SCANCODE_DOWN: k_down = 0; break;
                                                case SDL_SCANCODE_RIGHT: k_right = 0; break;
                                                case SDL_SCANCODE_A: k_strafe_left = 0; break;
                                                case SDL_SCANCODE_D: k_strafe_right = 0; break;
                                        }
                                break;
                                case SDL_QUIT:
                                        live = 0;
                                break;
                        }
                /*process keys*/
                if(k_up) {
                }

                if(k_down) {
                }

                if(mouse_deltaX != 0) {
                }

                if(k_left) {
                }
                if(k_right) {
                }

                if(k_strafe_left) {
                }
                if(k_strafe_right) {
                }
                //Render clear
                //SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                //SDL_RenderClear(renderer);

                //SDL_SetRenderDrawColor(renderer, 0, 200, 0, 100);

                //SDL_BlitScaled(hand[0], NULL, all_walls_surface, &handrect);

                //RENDER COPY
                //SDL_DestroyTexture(all_walls_texture);
                all_walls_texture = SDL_CreateTextureFromSurface(renderer, all_walls_surface);
                //SDL_FillRect(all_walls_surface, NULL, 0x000000); //QUITAR!!!!
                SDL_RenderCopy(renderer, all_walls_texture, NULL, NULL);

                SDL_SetRenderDrawColor(renderer, 255,0,0,SDL_ALPHA_OPAQUE);
                SDL_RenderDrawLine(renderer, 10,10,200,200);
                //Render 
		SDL_RenderPresent(renderer);

		//SDL_Delay(50);

}

