/*
 *
 * gcc -o raycasting-precalc raycasting-precalc2.c -lSDL2 -lSDL2_image -lm -lSDL2_mixer -lSDL2_ttf
 *
 */

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#define MAP_MAX_X 170
#define MAP_MAX_Y 200
#define MAP_SCALE 100
#define ANG_ROT 0.1f
#define MAX_FPS 20
#define MAX_FPS_AVERAGE 5
#define MAX_SHADES 30 
#define TEXTURES 8

void loadCosValues(float *cosVals, float angles);
void loadSinValues(float *sinVals, float angles);
void loadTanValues(float *tanVals, float angles);

//Pixel functions
Uint32 get_pixel32( SDL_Surface *surface, int x, int y);
void put_pixel32( SDL_Surface *surface, int x, int y, Uint32 pixel);

int main() {

	printf("STARTING...\n");

	int win_w = 640; int win_h = 360;
	//int win_w = 1280; int win_h = 720;
	//int win_w = 960; int win_h = 540;

	/*MAP 1*/
	char map1[MAP_MAX_Y][MAP_MAX_X] = {
		{"##############################"},
		{"C..............B.............C"},
		{"C..............B.B.B.B.B.B.B.C"},
		{"C................B...........C"},
		{"C................B.B.B.B.B.B.C"},
		{"C................B...........C"},
		{"C................B.B.B.B.B.B.C"},
		{"C................B...........C"},
		{"WW.WWWWWWWWWWWWWWB.B.B.B.B.B.C"},
		{"W............................C"},
		{"WWWWWWW.WWWWWWWWWB.B.B.B.B.B.C"},
		{"C......#.....................C"},
		{"C...#..#...#.................C"},
		{"C...#......#..#BBBBBBBBBBBBBBC"},
		{"C...###########.............BC"},
		{"C.............#BBBBBBBBBBBB.BC"},
		{"C............................C"},
		{"C..B...B...B...B...BB.BBBBBBBC"},
		{"C..B...B...B...B...B.........C"},
		{"C..B...B...B...B...B........UC"},
		{"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"}
	};

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Test raycasting!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_w,win_h,
			SDL_WINDOW_OPENGL);
			//SDL_WINDOW_FULLSCREEN);
			//0);

	/*MOUSE relative*/
	SDL_SetRelativeMouseMode(SDL_TRUE);

	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	//SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_PRESENTVSYNC);
	//SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED);

        SDL_Surface * all_walls_surface = SDL_CreateRGBSurface(0, win_w, win_h, 32, 0, 0, 0, 0);

	SDL_SetSurfaceBlendMode(all_walls_surface, SDL_BLENDMODE_NONE);
        SDL_Texture * all_walls_texture;

	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);


	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;
	int k_strafe_right = 0;
	int k_strafe_left= 0;
	float mouse_deltaX = 0;
	float mouse_deltaY = 0;

	/*live*/
	int live = 1;

	/*event*/
	SDL_Event event;

	/*user pos / view */
	float user_x_tmp, user_x_col, user_x = 400.0f;
	float user_y_tmp, user_y_col, user_y = 400.0f;
	int user_angle = 1900;
	int fov_angle = 900;

	/*iteration*/
	int i,j; //Usos multiples
	float it_angle;
	int it_width;
	int it_height;
	float it_ray_dis,it_ray_step;
	float it_x = 0;
	float it_y_tmp = 0;
	float it_y = 0;
	float it_cos;
	float it_sin;

	/*map*/
	int it_map_x;
	int it_map_y;
	int it_scale;

	const int f_near = win_w/2;

	int it_ray_max_sight = 300;

	int angles = 360/ANG_ROT*2;

	printf("Caching resources.\n");
	float sinVals[angles];
	loadSinValues(sinVals,angles);
	float cosVals[angles];
	loadCosValues(cosVals,angles);
	float tanVals[angles];
	loadTanValues(tanVals,angles);
	printf("Loading rays.\n");
        float atanValues[win_w];
	for(it_width = win_w; it_width>=0;it_width-=1) { 
		atanValues[it_width] = (float) atan((double) (((float)win_w/2 - it_width)/f_near)) * 180/M_PI * (360/ANG_ROT) / 360 * -1;
	}

        printf("Loading sprites.\n");
	SDL_Rect srcrect;
	SDL_Rect dstrect;

	float it_x_sprite,it_y_sprite;
	struct st_cacodemon {
		float x;
		float y;
		float xyratio;
		int width;
		int half_width;
		int height;
		SDL_Surface * surface;
	};
	struct st_cacodemon ST_CACODEMON = { 100.0f, 100.0f, 5.0f, 116, 58, 225, SDL_LoadBMP("sprites/uli.bmp")};
	ST_CACODEMON.xyratio = (float) ST_CACODEMON.width / ST_CACODEMON.height;
	SDL_SetColorKey (ST_CACODEMON.surface, SDL_TRUE, SDL_MapRGB (ST_CACODEMON.surface->format, 0, 255, 255)); 

	/*hud*/
        SDL_Surface * hand[1]; 
	SDL_Rect handrect;
	handrect.x = win_w/2;
	handrect.y = win_h/2;
	handrect.w = win_w/2;
	handrect.h = win_h/2;
	hand[0] = SDL_LoadBMP("sprites/handgun.bmp");
	SDL_SetColorKey (hand[0], SDL_TRUE, SDL_MapRGB (hand[0]->format, 0, 255, 255)); 
	//fprintf (stderr, "LoadSprite: Unable to set color key (0x%X)\n", SDL_MapRGB (SDL_GetWindowPixelFormat(win), 255, 0, 255));



        SDL_Surface * image[TEXTURES][MAX_SHADES]; 
	for(i = 0; i < MAX_SHADES;i++) { 
		image[0][i] = SDL_LoadBMP("walls/wall5.bmp");
		image[1][i] = SDL_LoadBMP("walls/wall1.bmp");
		image[2][i] = SDL_LoadBMP("walls/horacio2.bmp");
		image[3][i] = SDL_LoadBMP("walls/wall2.bmp");
		image[4][i] = SDL_LoadBMP("walls/wall3.bmp");
		image[5][i] = SDL_LoadBMP("walls/wall4.bmp");
		image[6][i] = SDL_LoadBMP("walls/ulises.bmp");
		image[7][i] = SDL_LoadBMP("walls/wall4.bmp");
	}

	for(j = 1; j< MAX_SHADES; j++)  
		for(i = 0;i<TEXTURES; i++) 
			SDL_SetSurfaceColorMod(image[i][j],
					(int)(255/MAX_SHADES*(MAX_SHADES - j)), 
					(int)(255/MAX_SHADES*(MAX_SHADES - j)), 
					(int)(255/MAX_SHADES*(MAX_SHADES - j)));

	printf("Loading walls.\n");
	float depth_buffer[win_w];
	char grid_code;

	struct wall_h {
		float wall_t[it_ray_max_sight];
		float wall_b[it_ray_max_sight];
		float ray_corrected_dist[angles];
		float wall_bright[it_ray_max_sight];
	} wall_h1;
	for(it_ray_dis = 0; it_ray_dis<=it_ray_max_sight;it_ray_dis++) { 
			wall_h1.wall_t[(int)it_ray_dis] =  win_h/2 - 5000 / it_ray_dis;
			wall_h1.wall_b[(int)it_ray_dis] =  5000 / it_ray_dis * 2;
	}
	int shades[it_ray_max_sight];
	for(it_ray_step = 0; it_ray_step<=it_ray_max_sight;it_ray_step++) { 
		shades[(int)it_ray_step] = (int)(it_ray_step*(MAX_SHADES-1)/it_ray_max_sight);
	}

	/*floor*/
	int it_floor;
	float it_ray_dis_floor;
        SDL_Surface * floor_surface = SDL_CreateRGBSurface(0, win_w, win_h/2, 32, 0, 0, 0, 0);
	SDL_Rect srcrect_floor;
	SDL_Rect dstrect_floor;
	srcrect_floor.w = 1;
	srcrect_floor.h= 1;
	dstrect_floor.w = 1;
	dstrect_floor.h = win_h/2;

	/* music sounds */
	Mix_OpenAudio(22050, AUDIO_S16SYS, 2, 640);
	Mix_Music *music = Mix_LoadMUS("songs/song1.mid"); //https://onlinesequencer.net/1597410
	Mix_PlayMusic(music, -1);

	/*frames*/
        Uint32 time_now,time_last,time_sum_avg,time_delta;
        Uint32 fps_arr[MAX_FPS_AVERAGE];
        int fps_count=0;

        /*text message*/
        TTF_Init();
        TTF_Font* font = TTF_OpenFont("tutorial-raycaster/font/DS-DIGII.TTF", 12); //font and size
        SDL_Color White = {255, 255, 255};
        char text[100];
        SDL_Surface* surfaceMessage = TTF_RenderText_Solid(font, text, White);
        SDL_Rect Message_rect; //alternative way to declare: SDL_Rect textLocation = { 0, 0, 300, 100 };
        Message_rect.x = 5;
        Message_rect.y = 5;
        Message_rect.w = 5*strlen(text);
        Message_rect.h = 5;

	printf("Done loading.\n");

	while(live) {

                time_last = time_now;
                time_now = SDL_GetTicks();
                time_delta = time_now-time_last;
                fps_arr[fps_count] = time_delta;
                if(fps_count < MAX_FPS_AVERAGE) {
			//printf("%i %u\n",fps_count,time_now-time_last);
                        fps_count++;
		} else {
                        time_sum_avg = 0;
                        for(;fps_count>=0;fps_count--) time_sum_avg += fps_arr[fps_count];

                        snprintf(text, 12, "FPS: %f", (float) 1000/(time_sum_avg/ MAX_FPS_AVERAGE));
                        surfaceMessage = TTF_RenderText_Solid(font, text, White);
                        fps_count = 0;
                }

		while(SDL_PollEvent(&event))
			switch(event.type) {
				/*MOUSE*/
				case SDL_MOUSEMOTION:
					mouse_deltaX = event.motion.xrel;
					//mouse_deltaY += event.motion.y; // no por ahora	
				break;
				/*teclado*/
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
						case SDL_SCANCODE_A: k_strafe_left = 1; break;
						case SDL_SCANCODE_D: k_strafe_right = 1; break;
					}
				break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
						case SDL_SCANCODE_A: k_strafe_left = 0; break;
						case SDL_SCANCODE_D: k_strafe_right = 0; break;
					}
				break;
				case SDL_QUIT:
					live = 0;
				break;
			}

		/*process keys*/
		if(k_up) {
			user_y_tmp = user_y;
			user_y_col = 2.2f*time_delta*sinVals[user_angle] + user_y;
			user_y = 0.6f*time_delta*sinVals[user_angle] + user_y;
			if(map1[(int)(user_x/32)][(int)(user_y_col/32)] != 46 ) user_y = user_y_tmp;
			user_x_tmp = user_x;
			user_x_col = 2.2f*time_delta*cosVals[user_angle] + user_x;
			user_x = 0.6f*time_delta*cosVals[user_angle] + user_x;
			if(map1[(int)(user_x_col/32)][(int)(user_y/32)] != 46 ) user_x = user_x_tmp;
		}

		if(k_down) {
			user_y = user_y - (float) 0.6f*time_delta*sinVals[user_angle];
			user_x = user_x - (float) 0.6f*time_delta*cosVals[user_angle];
		}

		if(mouse_deltaX != 0) { 
			user_angle += 10*mouse_deltaX;
			if(user_angle < 1800) user_angle += 360/ANG_ROT;
			else if(user_angle > 5400) user_angle -= 360/ANG_ROT;
			mouse_deltaX = 0;
		}

		if(k_left) { 
			user_angle -= 65;
			if(user_angle < 1800) user_angle += 360/ANG_ROT;
		}
		if(k_right) {
			user_angle += 65;
			if(user_angle > 5400) user_angle -= 360/ANG_ROT;
		}

		if(k_strafe_left) { 
			user_y = user_y - (float) 0.7f*time_delta*sinVals[user_angle+900];
			if(map1[(int)(user_x/32)][(int)(user_y/32)] != 46 ) user_y = user_y + (float) 0.7f*time_delta*sinVals[user_angle+900];
			user_x = user_x - (float) 0.7f*time_delta*cosVals[user_angle+900];
			if(map1[(int)(user_x/32)][(int)(user_y/32)] != 46 ) user_x = user_x + (float) 0.7f*time_delta*cosVals[user_angle+900];
		}
		if(k_strafe_right) {
			user_y = user_y + (float) 0.7f*time_delta*sinVals[user_angle+900];
			if(map1[(int)(user_x/32)][(int)(user_y/32)] != 46 ) user_y = user_y - (float) 0.7f*time_delta*sinVals[user_angle+900];
			user_x = user_x + (float) 0.7f*time_delta*cosVals[user_angle+900];
			if(map1[(int)(user_x/32)][(int)(user_y/32)] != 46 ) user_x = user_x - (float) 0.7f*time_delta*cosVals[user_angle+900];
		}

		//Render clear
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);

		SDL_SetRenderDrawColor(renderer, 0, 200, 0, 100);

		/*MAIN LOOP RAYCASTER*/
		for(it_width = 0; it_width<=win_w;it_width+=1) { 

			it_angle = user_angle + atanValues[it_width];
			dstrect.y = 180; dstrect.h = 1;

			for(it_ray_dis = 10; it_ray_dis <= it_ray_max_sight; it_ray_dis+=0.8f) {
				depth_buffer[it_width] = it_ray_max_sight; 
				it_x = (float)(user_x + cosVals[(int)it_angle] * it_ray_dis)/32;
				if(it_x > MAP_MAX_X || it_x < 0) continue;
				it_y = (float)(user_y + sinVals[(int)it_angle] * it_ray_dis)/32;
				if(it_y > MAP_MAX_Y || it_y < 0) continue;

				grid_code = map1[(int)it_x][(int)it_y];
				if(grid_code != 46) {
					if((int)it_y_tmp != (int)it_y)
						if(user_angle > 1800 && user_angle < 3600)
							srcrect.x = (it_x-(int)it_x)*320;
						else
							srcrect.x = (1-(it_x-(int)it_x))*320;
					else
						if(user_angle > 2700 && user_angle < 4500)
							srcrect.x = (it_y-(int)it_y)*320;
						else
							srcrect.x = (1-(it_y-(int)it_y))*320;
					srcrect.y = 0;
					srcrect.w = 1;
					srcrect.h = 320;
					dstrect.x = it_width - 1; //FIX
					dstrect.y = wall_h1.wall_t[abs((int)(it_ray_dis*cosVals[(int)((abs(atanValues[it_width])))]))];
					dstrect.w = 1;
					dstrect.h = wall_h1.wall_b[(int)(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]))])];
					
					switch(grid_code) {
						case 35:
							SDL_BlitScaled(image[0][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 77:
							SDL_BlitScaled(image[2][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 87:
							SDL_BlitScaled(image[3][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 67:
							SDL_BlitScaled(image[4][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 66:
							SDL_BlitScaled(image[5][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
						case 85:
							SDL_BlitScaled(image[6][shades[(int)it_ray_dis]], &srcrect, all_walls_surface, &dstrect);
						break;
					}

					//TO DO
					//SDL_SetRenderDrawColor(renderer, 0,0,255,SDL_ALPHA_OPAQUE);
					//SDL_RenderDrawPoint(renderer, it_x*32/5, it_y*32/5);

					depth_buffer[it_width] = it_ray_dis; 

					break;

				} 
					it_y_tmp = it_y;
			}
			//floor 
			for((it_floor=dstrect.h+dstrect.y-win_h/2);it_floor < win_h/2; it_floor++) {
					it_ray_dis_floor = fabsf((win_h/2*f_near/it_floor)/cosVals[(int)((abs(atanValues[it_width])))]);
					//it_ray_dis_floor = (win_h/2*(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]))])/it_floor);
					//it_ray_dis = 300/it_ray_max * it_ray_dis;

					it_x = (float)(user_x + cosVals[(int)it_angle] * it_ray_dis_floor)/32;
					//if(it_x > MAP_MAX_X || it_x < 0) continue;
					it_y = (float)(user_y + sinVals[(int)it_angle] * it_ray_dis_floor)/32;
					//printf("%f %f \n", it_x, it_y); 
					//if(it_y > MAP_MAX_Y || it_y < 0) continue;
					put_pixel32(all_walls_surface, it_width, it_floor+win_h/2,
					//get_pixel32(image[6][shades[(int)it_ray_dis]], (int)((it_x-(int)it_x)*320), (int)((it_y-(int)it_y)*320))
					get_pixel32(image[6][0], (int)((it_x-(int)it_x)*320), (int)((it_y-(int)it_y)*320))
					//get_pixel32(image[7][15], 150, 150)
					);
					//printf("%i %i \n",  (int)((it_x-(int)it_x)*320), (int)((it_y-(int)it_y)*320));
					//it_ray_dis-=1.0f;
				//}
			}
			//ceiling
			/*
			for(it_floor=dstrect.y;it_floor > 0; it_floor--) {
				//for(;it_ray_dis >= f_near/32;it_ray_dis--) {
					it_x = (float)(user_x + cosVals[(int)it_angle] * it_ray_dis)/32;
					if(it_x > MAP_MAX_X || it_x < 0) continue;
					it_y = (float)(user_y + sinVals[(int)it_angle] * it_ray_dis)/32;
					if(it_y > MAP_MAX_Y || it_y < 0) continue;
					//printf("%i %i %i %i\n",it_width, it_floor, (int)((it_y-(int)it_y)*320), (int)((it_x-(int)it_x)*320));
					//1 360 248 303
					put_pixel32(all_walls_surface, it_width, it_floor,
					get_pixel32(image[5][0], (int)((it_x-(int)it_x)*320), (int)((it_y-(int)it_y)*320))
					//get_pixel32(image[7][shades[(int)it_ray_dis]], 150, 150)
					);
					it_ray_dis--;
				//}
			}
			*/
				//dstrect.y = win_h/2;
				//dstrect.x = 0;
				//dstrect.h = win_h/2;
				//SDL_BlitScaled(floor_surface, NULL, all_walls_surface, &dstrect);
				//SDL_FreeSurface(floor_surface);
		} 
		/*sprites*/

			
			//it_ray_dis = (abs(ST_CACODEMON.x - user_x))/cosVals[(int)(tanf((abs(ST_CACODEMON.x - user_x))/(abs(ST_CACODEMON.x - user_y))*ANG_ROT))];
			it_x_sprite = user_x - ST_CACODEMON.x; 
			it_y_sprite = user_y - ST_CACODEMON.y; 
			it_ray_dis = sqrtf(it_x_sprite*it_x_sprite + it_y_sprite*it_y_sprite); 
			if(it_ray_dis < it_ray_max_sight) {
				it_angle = (atan2f(it_y_sprite, it_x_sprite)*180/M_PI)/ANG_ROT;
				//if(it_angle > (float)user_angle+fov_angle/2 && it_angle < (float)user_angle-fov_angle/2) { //it_angle -= 360/ANG_ROT; //inside FOV
				printf("%i \n", user_angle);
				printf("%f \n",it_angle);
				//if(fabsf(it_angle - user_angle - 270/ANG_ROT) < 180/ANG_ROT) { //it_angle -= 360/ANG_ROT; //inside FOV
					if(it_angle < 1800) it_angle = 360/ANG_ROT + it_angle;
					it_angle += 180/ANG_ROT;
						it_width = (int)((tanf((it_angle-user_angle)/10*M_PI/180))*f_near)+win_w/2;
						for(i = 0; i <= ST_CACODEMON.width;i++) {
							srcrect.x = i;
							srcrect.y = 0;
							srcrect.w = 1;
							srcrect.h = ST_CACODEMON.height;
							//dstrect.x = it_width-(ST_CACODEMON.half_width + i*((wall_h1.wall_b[(int)it_ray_dis]-wall_h1.wall_t[(int)it_ray_dis])*ST_CACODEMON.xyratio));
							dstrect.x = it_width - ST_CACODEMON.half_width + i;
								//it_width-(fabs(wall_h1.wall_t[(int)it_ray_dis]-wall_h1.wall_b[(int)it_ray_dis])*ST_CACODEMON.xyratio)/2 
								//+ i*(ST_CACODEMON.width/(fabs(wall_h1.wall_t[(int)it_ray_dis]-wall_h1.wall_b[(int)it_ray_dis])*ST_CACODEMON.xyratio));

							//dstrect.y = wall_h1.wall_t[abs((int)(it_ray_dis*cosVals[(int)((abs(atanValues[it_width])) )]))];
							dstrect.y = wall_h1.wall_t[(int)it_ray_dis];
							dstrect.w = 1;
							//dstrect.h = wall_h1.wall_b[(int)(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]) )])];
							dstrect.h = wall_h1.wall_b[(int)it_ray_dis];
							if(it_ray_dis > depth_buffer[it_width-ST_CACODEMON.half_width + i]) continue;
							if(SDL_BlitScaled(ST_CACODEMON.surface, &srcrect, all_walls_surface, &dstrect))
								printf("%s\n",SDL_GetError());
						}
				//}
			}
			
			//HUD HAND
			SDL_BlitScaled(hand[0], NULL, all_walls_surface, &handrect);


		//FPS
                //if(
		SDL_BlitSurface(surfaceMessage, NULL, all_walls_surface, &Message_rect);
		//) {
                //          printf("%s\n",SDL_GetError());
                //}



		//RENDER COPY
		srcrect.x = 0;
		srcrect.y = 0;
		srcrect.w = win_w;
		srcrect.h = win_h;
		SDL_DestroyTexture(all_walls_texture);
		all_walls_texture = SDL_CreateTextureFromSurface(renderer, all_walls_surface);
		SDL_RenderCopy(renderer, all_walls_texture, &srcrect, &srcrect);

		/*SDL_SetRenderDrawColor(renderer, 255,255,255,SDL_ALPHA_OPAQUE);
		SDL_RenderDrawPoint(renderer, user_x/5, user_y/5);
		SDL_SetRenderDrawColor(renderer, 0,255,0,SDL_ALPHA_OPAQUE);
		SDL_RenderDrawPoint(renderer, ST_CACODEMON.x/5, ST_CACODEMON.y/5);
		SDL_SetRenderDrawColor(renderer, 255,0,0,SDL_ALPHA_OPAQUE);
		SDL_RenderDrawLine(renderer, user_x/5, user_y/5,ST_CACODEMON.x/5, ST_CACODEMON.y/5);
*/
		//Render 
                SDL_RenderPresent(renderer);
	
	}

	//SDL clean
	Mix_FreeMusic(music);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}

void loadCosValues(float *cosVals, float angles) {
        for(int angle = 0; angle < angles; angle++) {
                cosVals[angle] = cosf((float) angle*ANG_ROT*M_PI/180);
        }
}
void loadSinValues(float *sinVals, float angles) {
        for(int angle = 0; angle < angles; angle++) {
                sinVals[angle] = sinf((float) angle*ANG_ROT*M_PI/180);
        }
}
void loadTanValues(float *tanVals, float angles) {
        for(int angle = 0; angle < angles; angle++) {
                tanVals[angle] = tanf((float) angle*ANG_ROT*M_PI/180);
        }
}

Uint32 get_pixel32( SDL_Surface *surface, int x, int y )
{
	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;

	//Get the requested pixel
	return pixels[ ( y * surface->pitch/4 ) + x ];

}

void put_pixel32( SDL_Surface *surface, int x, int y, Uint32 pixel )
{
	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;

	//Set the pixel
	//pixels[ ( y * surface->w ) + x ] = pixel;
	pixels[ ( y * surface->pitch/4 ) + x ] = pixel;

}
