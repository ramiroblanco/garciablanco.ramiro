/*
 *
 * gcc 1 1.c -lSDL2 -lSDL2_image -lGL
 * gcc -o gl gl.c  -lGLEW -lGL -lglfw
 *
 */


#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <GL/gl.h>


int main() {

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hola hola hola!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			864,486,
			SDL_WINDOW_OPENGL);


        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

        SDL_GLContext Context = SDL_GL_CreateContext(win);


	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

        printf("glString: %s\n",glGetString(GL_VERSION));


	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;

	/*live*/
	int live = 1;

	/*event*/
	SDL_Event event;

	/*user pos / view */
	float user_x = 0;
	float user_y = 200;
	float user_z = 0;
	float user_angle_x = 0;
	float user_angle_y = 0;


	/*fps*/
	Uint32 then, now, frames;

	/*random*/
	float circ_y;
	float circ_x;
	float circ_angle = 0;
	int radius = 100, i,j;

	glViewport(0, 0, 864, 486);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();


	while(live) {

		frames = 0;
		then = SDL_GetTicks();

		while(SDL_PollEvent(&event))
			switch(event.type) {
				case SDL_QUIT:
					live = 0;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
					}
					break;
			}
		/*process keys*/
		if(k_up) user_x += 50;
		if(k_down) user_x-= 50;
		if(k_left) user_y += 100;
		if(k_right) user_y -= 100;

		//glClearColor(1.f, 1.f, 1.f, 1.f);
		glClear(GL_COLOR_BUFFER_BIT);		
		    glBegin(GL_LINES);
    //All subsequent vertices will be red.
    glColor3f(1.0f,0.0f,0.0f);
    glVertex3f(0.0f,0.0f,0.0f); glVertex3f(1.0f,0.0f,0.0f);
    //All subsequent vertices will be green.
    glColor3f(0.0f,1.0f,0.0f);
    glVertex3f(0.0f,0.0f,0.0f); glVertex3f(0.0f,1.0f,0.0f);
    //All subsequent vertices will be blue.
    glColor3f(0.0f,0.0f,1.0f);
    glVertex3f(0.0f,0.0f,0.0f); glVertex3f(0.0f,0.0f,-11.0f);
    //Since OpenGL thinks the color is blue now, all subsequent vertices will be blue.  But, we want the
    //  triangle above to be white the *next* time we call this function!  So, reset the color to white.
    glColor3f(1.0f,1.0f,1.0f);
    glEnd();

		SDL_GL_SwapWindow(win);
		//SDL_Delay(1000/60);
		//printf("_%zu\n",(then-now)/1000);

	}



	//SDL clean
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
