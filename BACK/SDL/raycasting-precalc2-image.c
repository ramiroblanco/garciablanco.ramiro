/*
 *
 *
 */

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#define MAP_MAX_X 170
#define MAP_MAX_Y 200
#define MAP_SCALE 100
#define ANG_ROT 0.1f
#define MAX_FPS 30

void loadCosValues(float *cosVals, float angles);
void loadSinValues(float *sinVals, float angles);
void loadTanValues(float *tanVals, float angles);


int main() {

	printf("STARTING...\n");

	int win_w = 640; int win_h = 360;
	//int win_w = 1280; int win_h = 720;

	/*MAP 1*/
	char map1[MAP_MAX_Y][MAP_MAX_X] = {
		{"##################################################################################################################################################################"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................H...............................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M..........................................................................................................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"MWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW...............................................................................................C"},
		{"MWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW...............................................................................................C"},
		{"MWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW...............................................................................................C"},
		{"MWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW...............................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M..........................BBBBBBBBB.............................................................................................................................C"},
		{"M..........................BBBBBBBBB.............................................................................................................................C"},
		{"M..........................BBBBBBBBB.............................................................................................................................C"},
		{"M..........................BBBBBBBBB.............................................................................................................................C"},
		{"M..........................BBBBBBBBB.............................................................................................................................C"},
		{"M..........................BBBBBBBBBB............................................................................................................................C"},
		{"M..........................BBBBBBBBBBB...........................................................................................................................C"},
		{"M...........................BBBBBBBBBBB..........................................................................................................................C"},
		{"M............................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC"},
		{"M.............................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC"},
		{"M..............................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC"},
		{"M...............................BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M..........................................................................................................................................................M.....C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"M................................................................................................................................................................C"},
		{"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"}
	};

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Test raycasting!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_w,win_h,
			//SDL_WINDOW_OPENGL);
			SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN);


	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);


	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;

	/*live*/
	int live = 1;

	/*event*/
	SDL_Event event;

	/*user pos / view */
	float user_x = 10.0f;
	float user_y = 20.0f;
	int user_angle = 0;
	int fov_angle = 900;

	/*iteration*/
	float it_angle;
	int it_width;
	int it_height;
	float it_ray_dis,it_ray_step;
	float it_x = 0;
	float it_y = 0;
	float it_cos;
	float it_sin;

	/*map*/
	int it_map_x;
	int it_map_y;
	int it_scale;

	const int f_near = win_w/2;

	int it_ray_max_sight = 5000;

	int angles = 360/ANG_ROT*2;

	printf("Loading sin.\n");
	float sinVals[angles];
	loadSinValues(sinVals,angles);
	printf("Loading cos.\n");
	float cosVals[angles];
	loadCosValues(cosVals,angles);
	printf("Loading tan.\n");
	float tanVals[angles];
	loadTanValues(tanVals,angles);
	printf("Loading rays.\n");
	//float ray_angle = (float) fov_angle / win_w;// * ((360/ANG_ROT)/360);
	//printf("ray angle: %f (%f)\n",ray_angle,ray_angle*win_w);
        float atanValues[win_w+win_w/2];
	
	for(it_width = win_w; it_width>=0;it_width-=1) { 
		atanValues[it_width] = (float) atan((double) (((float)win_w/2 - it_width)/f_near)) * 180/M_PI * (360/ANG_ROT) / 360 * -1;
	}



	printf("Loading walls.\n");
	struct wall_h {
		float wall_t[it_ray_max_sight];
		float wall_b[it_ray_max_sight];
		float ray_corrected_dist[angles];
		float wall_bright[it_ray_max_sight];
	} wall_h1;
	for(it_ray_dis = 0; it_ray_dis<=it_ray_max_sight;it_ray_dis+=1) { 
			wall_h1.wall_t[(int)it_ray_dis] =  win_h/2 + (15000 / it_ray_dis);
			wall_h1.wall_b[(int)it_ray_dis] =  win_h/2 - (15000 / it_ray_dis);
	}

	printf("Loading sprites.\n");
	SDL_Surface * image = SDL_LoadBMP("sprites/horacio.bmp");
	SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, image);


	/*frames*/
	Uint32 startTime = 0;
	Uint32 endTime = 0;
	Uint32 delta = 0;
	short fps = 10;
	short timePerFrame = 2; // miliseconds


	while(live) {

		startTime = SDL_GetTicks();

		while(SDL_PollEvent(&event))
			switch(event.type) {
				case SDL_QUIT:
					live = 0;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
					}
					break;
			}

		/*process keys*/
		if(k_up) {
			user_y = (float) 5*sinVals[user_angle] + user_y;
			//user_y = (float) 0.05*delta*sinVals[user_angle] + user_y;
			user_x = (float) 5*cosVals[user_angle] + user_x;
			//user_x = (float) 0.05*delta*cosVals[user_angle] + user_x;
			//printf("%f\n", delta);
		}

		if(k_down) {
			user_y = user_y - (float) 0.05*delta*sinVals[user_angle];
			user_x = user_x - (float) 0.05*delta*cosVals[user_angle];
		}

		if(k_left) { 
			user_angle -= 45;
			if(user_angle < 450) user_angle += 360/ANG_ROT;
		}
		if(k_right) {
			user_angle += 45;
			if(user_angle > 4050) user_angle -= 360/ANG_ROT;
		}

		//Render clear
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);

		SDL_RenderCopy(renderer, texture, NULL, NULL);

		/*floor
		for(it_height = win_h/2;it_height<=win_h;it_height++) {
			SDL_SetRenderDrawColor(renderer, it_height, it_height, it_height, SDL_ALPHA_OPAQUE);
			SDL_RenderDrawLine(renderer, 0, it_height, win_w, it_height);
		}
		*/

		/*ceiling
		for(it_height = win_h/2;it_height>=0;it_height--) {
			SDL_SetRenderDrawColor(renderer, it_height, it_height, it_height, SDL_ALPHA_OPAQUE);
			SDL_RenderDrawLine(renderer, 0, it_height, win_w, it_height);
		}
		*/
		

		/*MAIN LOOP*/
		for(it_width = 0; it_width<=win_w;it_width+=1) { 

		SDL_SetRenderDrawColor(renderer, it_width, 0, 0, SDL_ALPHA_OPAQUE);

			it_angle = user_angle + atanValues[it_width];
			//it_angle = user_angle + ray_angle*it_width + 3150;
			//printf("%i %i %f %f\n",it_width,user_angle,it_angle,cosVals[(int)(ray_angle*it_width)]);

			for(it_ray_dis = 5; it_ray_dis <= it_ray_max_sight; it_ray_dis++) {
				it_x = (float)(user_x + cosVals[(int)it_angle] * it_ray_dis)/5;
				if(it_x > MAP_MAX_X || it_x < 0) continue;
				//it_y = user_y + sinVals[(int)it_angle] * it_ray_dis;
				it_y = (float)(user_y + sinVals[(int)it_angle] * it_ray_dis)/5;
				if(it_y > MAP_MAX_Y || it_y < 0) continue;
				//printf("itrd %f iy %f ix %f t%f a%f\n",it_ray_dis,it_y,it_x,tanVals[(int)it_angle],it_angle);
					//default:
					//SDL_SetRenderDrawColor(renderer, 0, 255, 0, 1);
					//SDL_RenderDrawPoint(renderer, it_x, it_y);

				switch(map1[(int)it_x][(int)it_y]) {
					case 35:
					SDL_SetRenderDrawColor(renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawLine(renderer, it_width, wall_h1.wall_t[abs((int)(it_ray_dis*cosVals[(int)((abs(atanValues[it_width])) )]))], it_width, wall_h1.wall_b[(int)(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]) )])]);
					SDL_SetRenderDrawColor(renderer, 0, 250, 0, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawPoint(renderer, it_x, it_y);
					it_ray_dis = 9999;
					break;
					case 77:
					SDL_SetRenderDrawColor(renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawLine(renderer, it_width, wall_h1.wall_t[abs((int)(it_ray_dis*cosVals[(int)((abs(atanValues[it_width])) )]))], it_width, wall_h1.wall_b[(int)(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]) )])]);
					SDL_SetRenderDrawColor(renderer, 0, 250, 0, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawPoint(renderer, it_x, it_y);
					it_ray_dis = 9999;
					break;
					case 87:
					SDL_SetRenderDrawColor(renderer, 200, 50, 200, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawLine(renderer, it_width, wall_h1.wall_t[abs((int)(it_ray_dis*cosVals[(int)((abs(atanValues[it_width])) )]))], it_width, wall_h1.wall_b[(int)(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]) )])]);
					SDL_SetRenderDrawColor(renderer, 0, 250, 0, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawPoint(renderer, it_x, it_y);
					it_ray_dis = 9999;
					break;
					case 67:
					SDL_SetRenderDrawColor(renderer, 180, 180, 180, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawLine(renderer, it_width, wall_h1.wall_t[abs((int)(it_ray_dis*cosVals[(int)((abs(atanValues[it_width])) )]))], it_width, wall_h1.wall_b[(int)(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]) )])]);
					SDL_SetRenderDrawColor(renderer, 0, 250, 0, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawPoint(renderer, it_x, it_y);
					it_ray_dis = 9999;
					break;
					case 66:
					SDL_SetRenderDrawColor(renderer, 165, 42, 42, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawLine(renderer, it_width, wall_h1.wall_t[abs((int)(it_ray_dis*cosVals[(int)((abs(atanValues[it_width])) )]))], it_width, wall_h1.wall_b[(int)(it_ray_dis*cosVals[(int)(abs(atanValues[it_width]) )])]);
					SDL_SetRenderDrawColor(renderer, 0, 250, 0, SDL_ALPHA_OPAQUE);
					SDL_RenderDrawPoint(renderer, it_x, it_y);
					it_ray_dis = 9999;
					break;
				}
			//break;
			}
		
		} 

		//MAP

		//SDL_SetRenderDrawColor(renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
		//SDL_RenderDrawPoint(renderer, user_x , user_y );

		//Render 
                SDL_RenderPresent(renderer);
	
		// if less than 16ms, delay
		endTime = SDL_GetTicks();

		delta = endTime - startTime; // how many ms for a frame
		if (delta < 1000/MAX_FPS) {
			SDL_Delay(1000/MAX_FPS - delta);
			//printf("FPS is: %f \n", (float)delta*(float)MAX_FPS/1000);
			//printf("FPS is: %f \n", (float)delta);
		}

		// if delta is bigger than 16ms between frames, get the actual fps
		//if (delta > timePerFrame) {
		//	fps = 1000 / delta;
		//	printf("FPS is: %i \n", fps);
		//}


		//startTime = endTime;
		//endTime = SDL_GetTicks();
	}



	//SDL clean
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}

void loadCosValues(float *cosVals, float angles) {
        for(int angle = 0; angle < angles; angle++) {
                cosVals[angle] = cosf((float) angle*ANG_ROT*M_PI/180);
        }
}
void loadSinValues(float *sinVals, float angles) {
        for(int angle = 0; angle < angles; angle++) {
                sinVals[angle] = sinf((float) angle*ANG_ROT*M_PI/180);
        }
}
void loadTanValues(float *tanVals, float angles) {
        for(int angle = 0; angle < angles; angle++) {
                tanVals[angle] = tanf((float) angle*ANG_ROT*M_PI/180);
        }
}
