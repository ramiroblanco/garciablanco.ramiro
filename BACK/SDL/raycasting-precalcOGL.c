#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <GL/gl.h>
#include <assert.h>

#define MAX_FPS 50
#define MAX_FPS_AVERAGE 5


int main() {

	int live = 1;
	int win_w = 640, win_h = 480;
	int k_up = 0, k_down = 0, k_left = 0, k_right = 0;

	SDL_Init( SDL_INIT_VIDEO );

	SDL_Window *win = SDL_CreateWindow("Raycaster Tutorial!", 
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_w, win_h,
			SDL_WINDOW_OPENGL);

	assert(win);

	if(!win) {
		printf("Error: %s\n", SDL_GetError());
		return 1;
	}

	SDL_GLContext Context = SDL_GL_CreateContext(win);

	SDL_Event event;

	while(live) {

		while(SDL_PollEvent(&event)) {
			switch(event.type) {
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W: k_up = 1;break;
						case SDL_SCANCODE_S: k_down = 1;break;
						case SDL_SCANCODE_A: k_left = 1;break;
						case SDL_SCANCODE_D: k_right = 1;break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W: k_up = 0;break;
						case SDL_SCANCODE_S: k_down = 0;break;
						case SDL_SCANCODE_A: k_left = 0;break;
						case SDL_SCANCODE_D: k_right = 0;break;
					}
					break;
				case SDL_QUIT:
					live = 0;
					break;
			}

		}

		//handle keys
		if(k_up) printf("Up pressed\n");
		if(k_down) printf("Down pressed\n");
		if(k_left) printf("Left pressed\n");
		if(k_right) printf("Right pressed\n");

		glViewport(0, 0, win_w, win_h);
		glClearColor(0.f, 0.f, 0.f, 0.f);
		glClear(GL_COLOR_BUFFER_BIT);

		glBegin();
		glVertex2i(100,100);
		glEnd();

		SDL_GL_SwapWindow(win);
		SDL_Delay(1);

	}
	SDL_GL_DeleteContext(Context);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
