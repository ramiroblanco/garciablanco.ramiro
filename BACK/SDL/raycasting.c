/*
 *
 *
 */

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#define MAP_MAX_X 67
#define MAP_MAX_Y 10
#define MAP_SCALE 100

int main() {

	int win_w = 640;
	int win_h = 480;

	/*MAP 1*/
	char map1[MAP_MAX_Y][MAP_MAX_X] = {
		{"###################################################################"},
		{"#.................................................................#"},
		{"#...........................##############........................#"},
		{"#...........................##############........................#"},
		{"#...........................##############........................#"},
		{"#...........................##############........................#"},
		{"#.................................................................#"},
		{"#.................................................................#"},
		{"#.................................................................#"},
		{"###################################################################"},
	};

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Test raycasting!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_w,win_h,
			SDL_WINDOW_OPENGL);


	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;

	/*live*/
	int live = 1;

	/*event*/
	SDL_Event event;

	/*user pos / view */
	float user_x = 100.0f;
	float user_y = 100.0f;
	float user_angle = 0.0f;

	/*iteration*/
	float it_angle;
	int it_width;
	const int it_start = -1 * win_w/2;
	int it_ray_dis;
	float it_x;
	float it_y;

	/*map*/
	int it_map_x;
	int it_map_y;
	int it_scale;

	const int f_near = win_w/2;

	int it_ray_max_sight = 2000;

	while(live) {

		while(SDL_PollEvent(&event))
			switch(event.type) {
				case SDL_QUIT:
					live = 0;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
					}
					break;
			}

		/*process keys*/
		if(k_up) {
			user_y = (float) 5*sinf(user_angle) + user_y;
			user_x = (float) 5*cosf(user_angle) + user_x;
		}

		if(k_down) {
			user_y = user_y - (float) sinf(user_angle);
			user_x = user_x - (float) cosf(user_angle);
		}

		if(k_left) user_angle -= 0.1f;
		if(k_right) user_angle += 0.1f;

		//Render clear
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);

		SDL_SetRenderDrawColor(renderer, 0, 200, 0, 100);

		for(it_width = it_start; it_width<=win_w/2;it_width+=10) {

			it_angle = user_angle + atan((double) it_width/f_near);

				for(it_ray_dis = f_near; it_ray_dis <= it_ray_max_sight; it_ray_dis++) {
				
					it_y = sinf(it_angle) * it_ray_dis/100;
					it_x = cosf(it_angle) * it_ray_dis/100;
					//SDL_RenderDrawPoint(renderer, user_x + it_x, user_y + it_y);
				
				}
		
		} 

		//MAP
		SDL_SetRenderDrawColor(renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
		for(it_map_y = 0; it_map_y<MAP_MAX_Y;it_map_y++) 
			for(it_map_x = 0; it_map_x<MAP_MAX_X;it_map_x++) {
				if(map1[it_map_y][it_map_x] == 35)
					SDL_RenderDrawPoint(renderer, it_map_x*MAP_SCALE , it_map_y*MAP_SCALE );
			}

		SDL_SetRenderDrawColor(renderer, 250, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawPoint(renderer, user_x , user_y );

		//Render 
                SDL_RenderPresent(renderer);
	
		SDL_Delay(1000/30);
	}



	//SDL clean
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
