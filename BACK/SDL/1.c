/*
 *
 * gcc 1 1.c -lSDL2 -lSDL2_image -lGL
 * gcc -o gl gl.c  -lGLEW -lGL -lglfw
 *
 */

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

int main() {

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hola hola hola!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			648,480,
			SDL_WINDOW_OPENGL);


	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;

	/*live*/
	int live = 1;

	/*event*/
	SDL_Event event;

	/*user pos / view */
	float user_x = 0;
	float user_y = 200;
	float user_z = 0;
	float user_angle_x = 0;
	float user_angle_y = 0;

	/*cube*/
	static float cube[8][3] = {
		{320, 240, 300},
		{320, 400, 300},
		{420, 240, 300},
		{420, 400, 300},
		{320, 240, 500},
		{320, 400, 500},
		{420, 240, 500},
		{420, 400, 500},
	}; 

	/*fps*/
	Uint32 then, now, frames;

	while(live) {

		frames = 0;
		then = SDL_GetTicks();

		while(SDL_PollEvent(&event))
			switch(event.type) {
				case SDL_QUIT:
					live = 0;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
					}
					break;
			}

		/*process keys*/
		if(k_up) cube[0][0] += 10;
		if(k_down) cube[0][0] -= 10;
		if(k_left) printf("left\n");
		if(k_right) printf("right\n");

		//Render clear
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);


		SDL_SetRenderDrawColor(renderer, 200, 200, 200, SDL_ALPHA_OPAQUE);

		/* cube[8][3] */
		for(int i=0; i<8; i++)
			for(int e=0;e<3;e++) {
				if(e==i) continue;
				SDL_RenderDrawLine(renderer, cube[i][0], cube[i][1], cube[e][0], cube[e][1]);
			}

		
		
		//Render 
                SDL_RenderPresent(renderer);

	
		/*now = SDL_GetTicks();
		if (now > then) {
		printf("%2.2f frames per second\n",
		       ((double) frames * 1000) / (now - then));
		} */

		/*beats*/
		//SDL_Delay(1000/60);
	}



	//SDL clean
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
