/*
 *
 * gcc 1 1.c -lSDL2 -lSDL2_image -lGL
 * gcc -o gl gl.c  -lGLEW -lGL -lglfw
 *
 */

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#define FOV 100

void DrawLine(float, float, float, float, float, float, SDL_Renderer*);
float getDistance(float, float, float, float, float, float);

/*structs*/
struct vertex {
	float x,y,z;
};
struct triangle {
	struct vertex p[3];
};

struct mesh {
	int count;
	float angle;
	struct triangle triangles[]; 
};

struct matrix {
	float m44[4][4];// = { 0 };
};

void DrawTriangle(struct triangle tri, SDL_Renderer*);
void ProjectVertex(struct triangle* vResult, struct triangle* vOrig);
void RotateMesh(struct mesh *m, float);

const int win_w = 640;
const int win_h = 480;
const float fov = 1/tan((FOV/2)*M_PI/180);
const float win_ratio = (float) win_w/win_h;
const float znear = 2.1f;
const float zfar = 10000000.0;

int main() {


	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hola mundo 3D!",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_w,win_h,
			SDL_WINDOW_OPENGL);

	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;

	/*live*/
	int live = 1;

	/*event*/
	SDL_Event event;

	/*user pos / view */
	float user_x = 0;
	float user_y = 0;
	float user_z = 0;
	float user_angle_x = 0;
	float user_angle_y = 0;
	float ftheta = 0;

	//Uint32 SDL_GetTicks(void)
	uint32_t tick_time = SDL_GetTicks();

	/*cube*/
	struct mesh *cube = malloc( sizeof(int) + sizeof(float) + sizeof(struct triangle) * 4);
	cube->count = 4;
	cube->angle = 0.0f;
	cube->triangles[0].p[0].x = 20; cube->triangles[1].p[0].x = 20; 
	cube->triangles[0].p[0].y = 20; cube->triangles[1].p[0].y = 20; 
	cube->triangles[0].p[0].z = 0 ; cube->triangles[1].p[0].z = 1; 
	
	cube->triangles[0].p[1].x = 20; cube->triangles[1].p[1].x = 20; 
	cube->triangles[0].p[1].y = 40; cube->triangles[1].p[1].y = 40; 
	cube->triangles[0].p[1].z = 0; cube->triangles[1].p[1].z = 1; 

	cube->triangles[0].p[2].x = 20; cube->triangles[1].p[2].x = 20; 
	cube->triangles[0].p[2].y = 20; cube->triangles[1].p[2].y = 40; 
	cube->triangles[0].p[2].z = 1; cube->triangles[1].p[2].z = 0; 

	cube->triangles[2].p[0].x = 20; cube->triangles[3].p[0].x = 20; 
	cube->triangles[2].p[0].y = 20; cube->triangles[3].p[0].y = 20; 
	cube->triangles[2].p[0].z = 0; cube->triangles[3].p[0].z = 1; 
	
	cube->triangles[2].p[1].x = 40; cube->triangles[3].p[1].x = 40; 
	cube->triangles[2].p[1].y = 20; cube->triangles[3].p[1].y = 20; 
	cube->triangles[2].p[1].z = 0; cube->triangles[3].p[1].z = 0; 

	cube->triangles[2].p[2].x = 20; cube->triangles[3].p[2].x = 40; 
	cube->triangles[2].p[2].y = 20; cube->triangles[3].p[2].y = 20; 
	cube->triangles[2].p[2].z = 1; cube->triangles[3].p[2].z = 1; 

/*	cube->triangles[4].p[0].x = 200; cube->triangles[5].p[0].x = 200; 
	cube->triangles[4].p[0].y = 200; cube->triangles[5].p[0].y = 200; 
	cube->triangles[4].p[0].z = 2; cube->triangles[5].p[0].z = 202; 
	
	cube->triangles[4].p[1].x = 200; cube->triangles[5].p[1].x = 200; 
	cube->triangles[4].p[1].y = 200; cube->triangles[5].p[1].y = 400; 
	cube->triangles[4].p[1].z = 202; cube->triangles[5].p[1].z = 202; 

	cube->triangles[4].p[2].x = 200; cube->triangles[5].p[2].x = 200; 
	cube->triangles[4].p[2].y = 400; cube->triangles[5].p[2].y = 400; 
	cube->triangles[4].p[2].z = 2; cube->triangles[5].p[2].z = 2; 
	*/
	while(live) {
		while(SDL_PollEvent(&event))
			switch(event.type) {
				case SDL_QUIT:
					live = 0;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
					}
					break;
			}

		/*process keys*/
		//if(k_up) cube->triangles[0].p[0].x += 10; //cube->triangles[2].p[0].x = 200; 
		//if(k_down) cube->triangles[0].p[1].x += 10; //cube->triangles[2].p[1].x = 200; 
		//if(k_left) { cube->triangles[0].p[2].x -= 100; cube->triangles[0].p[2].y -= 100; cube->triangles[0].p[2].z -= 100;}  //cube->triangles[2].p[2].x = 400; 
		//if(k_right) user_angle_x -=10;
		ftheta += 1.0f * tick_time;
		printf("t:%f\n",ftheta);

		//Render clear
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);

		SDL_SetRenderDrawColor(renderer, 200, 200, 200, SDL_ALPHA_OPAQUE);

		//RotateMesh(cube, 0.05f);
		
		//for(int i = 0; i < cube->count;i++)
		for(int i = 0; i < 4;i++) {
			DrawTriangle(cube->triangles[i], renderer);
		}
		
		//Render 
                SDL_RenderPresent(renderer);
	
		/*beats*/
		SDL_Delay(1000/60);
	}



	//SDL clean
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}

float getDistance(float x1, float y1, float z1, float x2, float y2, float z2) {
	
	float dist, size_x, size_y, size_z;
	size_x = x2 - x1;
	size_y = y2 - y1;
	size_z = z2 - z1;

	return sqrtf(powf(size_x, 2.0f) + powf(size_y, 2.0f));
	//printf("\n dist: %f\n",dist);
}

void DrawLine(float x1, float y1, float z1, float x2, float y2, float z2, SDL_Renderer *renderer) {

	SDL_RenderDrawLine(renderer, x1, y1, x2, y2);

	return; //FIX
	
	float dist, angle, size_x, size_y, size_z;
	size_x = x2 - x1;
	size_y = y2 - y1;
	size_z = z2 - z1;

	angle = atanf(( fabsf( size_y / size_x ) ));
	dist = getDistance(x1,y1,0,x2,y2,0);

	for(int i = 0; i < dist; i++) {
		// DRAW PIXEL
		SDL_RenderDrawPoint(renderer, x2 + i, y2 + i/cosf(angle));
		printf("\n x %f y %f dist %f\n",x2 + i, y2 + i/cosf(angle), dist);
	}
}

void DrawTriangle(struct triangle tri, SDL_Renderer *renderer) {

	struct triangle triPro;
	ProjectVertex(&triPro, &tri);
	/*printf("1_%f %f %f\n", tri.p[0].x,tri.p[0].y,tri.p[0].z);
	printf("1_%f %f %f\n", triPro.p[0].x,triPro.p[0].y,triPro.p[0].z);
	printf("2_%f %f\n", tri.p[1].x,tri.p[1].y);
	printf("2_%f %f\n", tri.p[1].x,tri.p[1].y);
	printf("3_%f %f\n", tri.p[2].x,tri.p[2].y);
	printf("3_%f %f\n", tri.p[2].x,tri.p[2].y); */
	DrawLine(triPro.p[0].x,triPro.p[0].y,triPro.p[0].z,triPro.p[1].x,triPro.p[1].y,triPro.p[1].z,renderer);

	DrawLine(triPro.p[1].x,triPro.p[1].y,triPro.p[1].z,triPro.p[2].x,triPro.p[2].y,triPro.p[2].z,renderer);

	DrawLine(triPro.p[2].x,triPro.p[2].y,triPro.p[2].z,triPro.p[0].x,triPro.p[0].y,triPro.p[0].z,renderer);

}

void ProjectVertex(struct triangle* vResult, struct triangle* vOrig){

	//printf("FOV: %f\n",fov);
	//printf("win_ratio: %f\n",win_ratio);
	for(int i = 0; i < 3; i++) {
		vResult->p[i].x = ( (win_ratio*fov * vOrig->p[i].x) / vOrig->p[i].z ) + win_w / 2;
		vResult->p[i].y = ( (fov * vOrig->p[i].y) / vOrig->p[i].z ) + win_h / 2;
		vResult->p[i].z = vOrig->p[i].z*(zfar/(zfar-znear))-znear; 
	}
	return;
};


void RotateMesh(struct mesh *m, float n_angle) {

	for(int i = 0; i < m->count; i++) {
		printf("i: %i x: %f count: %i \n", i, m->triangles[i].p[0].x , m->count);
		m->triangles[i].p[0].x = m->triangles[i].p[0].x * cosf(m->angle+n_angle) - m->triangles[i].p[0].y * sinf(m->angle+n_angle);
		m->triangles[i].p[0].y = m->triangles[i].p[0].x * sinf(m->angle+n_angle) + m->triangles[i].p[0].y * cosf(m->angle+n_angle);

		m->triangles[i].p[1].x = m->triangles[i].p[1].x * cosf(m->angle+n_angle) - m->triangles[i].p[1].y * sinf(m->angle+n_angle);
		m->triangles[i].p[1].y = m->triangles[i].p[1].x * sinf(m->angle+n_angle) + m->triangles[i].p[1].y * cosf(m->angle+n_angle);

		m->triangles[i].p[2].x = m->triangles[i].p[2].x * cosf(m->angle+n_angle) - m->triangles[i].p[2].y * sinf(m->angle+n_angle);
		m->triangles[i].p[2].y = m->triangles[i].p[2].x * sinf(m->angle+n_angle) + m->triangles[i].p[2].y * cosf(m->angle+n_angle);
	}

}
