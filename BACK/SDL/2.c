/*
 *
 * gcc 2 2.c -lSDL2 -lSDL2_image -lGL
 * gcc -o gl gl.c  -lGLEW -lGL -lglfw
 *
 */


#include <stdio.h>
#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_opengl.h>

#define SHOW_STATS 0
#define MAX_FPS 60


int main() {

	int win_width = 640;
	int win_height = 480;

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0) {
	
		printf("Error iniciando: %s\n", SDL_GetError());
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Pijas de goma! v1.0",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			win_width,win_height,
			SDL_WINDOW_OPENGL);
	/*render*/
	SDL_Renderer *renderer;
	renderer = SDL_CreateRenderer(win,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderDrawColor(renderer,0,0,0,0);


	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
			SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GLContext context = SDL_GL_CreateContext(win);

	if(!win) {
		printf("Error creando ventana: %s\n", SDL_GetError());
		return 1;
	}

	printf("glString: %s\n",glGetString(GL_VERSION));

	/* input tracking */
	int k_up = 0;
	int k_down = 0;
	int k_left = 0;
	int k_right = 0;
	float user_x = 0;
	float user_y = 0;
	float user_z = 0;

	/*live*/
	int live = 1;

	/*event*/
	SDL_Event event;

#if SHOW_STATS > 0
	Uint32 fps = 0.0f;
	Uint32 start_time = 0.0f;
#endif 


		glViewport(0, 0, win_width, win_height);
glMatrixMode(GL_PROJECTION);
glLoadIdentity();
glOrtho(-50, 50, -50, 50, -50, 50);
glMatrixMode(GL_MODELVIEW);

	while(live) {

#if SHOW_STATS > 0
	start_time = SDL_GetTicks();
#endif 

		while(SDL_PollEvent(&event))
			switch(event.type) {
				case SDL_QUIT:
					live = 0;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 1; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 1; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 1; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 1; break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.scancode) {
						case SDL_SCANCODE_W:
						case SDL_SCANCODE_UP: k_up = 0; break;
						case SDL_SCANCODE_A:
						case SDL_SCANCODE_LEFT: k_left = 0; break;
						case SDL_SCANCODE_S:
						case SDL_SCANCODE_DOWN: k_down = 0; break;
						case SDL_SCANCODE_D:
						case SDL_SCANCODE_RIGHT: k_right = 0; break;
					}
					break;
			}

		/*process keys*/
		if(k_up) user_x+=100;
		if(k_down) user_x+=100;
		if(k_left) user_y+=100;
		if(k_right) user_y-=100;
	
		//Rendering
		SDL_RenderClear(renderer);
		//SDL_RenderPresent(renderer);

		//glClearColor(0.f, 0.f, 1.f, 0.f);
		//glClear(GL_COLOR_BUFFER_BIT);
		//glLineWidth(5);
		glColor3f(1, .5, 1.0);
		glLineWidth(0);
		glBegin(GL_LINES);
		glVertex3f(100+user_x, 100+user_y,-300+user_z);
		glVertex3f(100+user_x, 200+user_y,-400+user_z);
		glEnd();


		SDL_GL_SwapWindow(win);

#if SHOW_STATS > 0
		fps = (Uint32) ((start_time - SDL_GetTicks()) / 1000);
		printf("\nCurrent Frames Per Second: %zu\n\n", fps);
#endif 
		/*beats*/
		//SDL_Delay(1000/60);

	}



	//SDL clean
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
