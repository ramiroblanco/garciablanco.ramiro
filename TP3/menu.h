#include <stdio.h>

const static char *XMN_OPTIONS[] = { "1) agregar registro",
	"2) buscar registro por IMEI",
	"3) eliminar registro",
	"4) seleccionar subconjunto por marca o modelo",
	"5) imprimir subconjunto seleccionado",
	"6) exportar subconjunto seleccionado en CSV",
	"7) eliminar subconjunto seleccionado",
	"8) imprimir toda la DB",
	"9) exportar toda la DB",
	"10) guardar la DB a archivo (encriptada)",
	"99) salir",
	NULL};

const static char *XMN_SUB[] = { "1) Seleccionar subconjunto por marca.",
	"2) Seleccionar subconjunto por modelo.",
	"99) volver",
	NULL};
