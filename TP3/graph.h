#ifndef GRAPH_H
#define GRAPH_H

#include <stdio.h>

void draw_box(char *msg);

void draw_multi_box(const char **msgs, int subconjunto);

void print_table(ST_CELLPHONE **p);

void print_table_subconjunto(ST_CELLPHONE **p);

void cls();

void print_record(ST_CELLPHONE*, int record_id);

#endif
