#ifndef DATABASE_H
#define DATABASE_H

#include <stdio.h>

#define DATABASE_NAME "db.bin"

#define DATABASE_WRITE_FAIL 0
#define DATABASE_WRITE_OK 1
#define DATABASE_READ_FAIL 0
#define DATABASE_READ_FOUND 1
#define DATABASE_READ_NOTFOUND 0

#define MAX_RECORDS 2000
#define RECORDS_COLS 8 
#define MAX_BUFF_NAME 300
#define MAX_BUFF_RECORD 1500
#define MAX_BUFF_MARCA 300
#define MAX_BUFF_MODELO 300

typedef struct {

	int mem_int;
	int mem_ram;
	float cpu_clock;
	int cam_mpx;

} ST_HARDW;

typedef struct {

        int id; 
        char marca[MAX_BUFF_MARCA];
        char modelo[MAX_BUFF_MODELO];
        long int imei;
        float peso;
	ST_HARDW *hardware;

} ST_CELLPHONE;

#endif
