#include <stdio.h>

int agregar_registro();

ST_CELLPHONE* buscar_registro_IMEI(char[]);

int eliminar_registro(ST_CELLPHONE*);

//seleccionar subconjunto por marca o modelo

//imprimir subconjunto seleccionado

//exportar subconjunto seleccionado en CSV

//eliminar subconjunto seleccionado

int imprimir_DB();

int exportar_DB();

int guardar_DB_crypt();

FILE *db_open(char *file_name);

int db_close(FILE *fp);

int write_record(FILE *fp, ST_CELLPHONE *p);

int read_record(FILE *fp, int id, ST_CELLPHONE *p);

int input_record(ST_CELLPHONE**);

int load_records(FILE *fp, ST_CELLPHONE ***p, int salto);

void read_csv(char *file_name, ST_CELLPHONE ***cellphones);

void menu_interactivo(int *live, int *option, ST_CELLPHONE ***p, ST_CELLPHONE ***s,FILE *database, int salto);
