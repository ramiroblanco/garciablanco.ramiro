#include <stdio.h>
#include <stdlib.h>
#include "database.h"
#include "dblib.h"
#include "graph.h"


//
//
//
// http://wiki.c2.com/?ThreeStarProgrammer
//
//

int main(int argc, char **argv) {

	int c;// counter
	int live = 1, option = 0;

	//Main array
	ST_CELLPHONE **CELLPHONES_DB = malloc(sizeof(ST_CELLPHONE*)); *CELLPHONES_DB = NULL;

	ST_CELLPHONE **SUBCONJUNTO_DB = malloc(sizeof(ST_CELLPHONE*)); *SUBCONJUNTO_DB = NULL;

	cls(); //CLS

	if(argc < 2) {
		draw_box("No hay numero para desencriptar la base de datos.");
		return 1;
	}

	//Open DB
	FILE *database = NULL;
        database = db_open(DATABASE_NAME);
	
	printf("\n ] Salto/Key: %s\n",argv[1]);
	
	//Importo a memoria
	printf(" ] Importo registros a memoria.\n");
	load_records(database, &CELLPHONES_DB, atoi(argv[1]));

	//Files CSV
	printf(" ] Importo archivos csv si hay.\n");
	for(c=2;argv[c] != NULL ;c++) {
	
		printf("\n ] CSV: %s ", argv[c]);
		read_csv(argv[c], &CELLPHONES_DB);
	}

	//MENU
        while(live) menu_interactivo(&live,&option, &CELLPHONES_DB, &SUBCONJUNTO_DB, database, atoi(argv[1]));
	//Close DB
        if(db_close(database)) printf("Error %s from %s, line %d\n", __FUNCTION__, __FILE__, __LINE__);

	return 0;
}

