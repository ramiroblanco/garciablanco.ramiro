#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../database.h"
#include "../menu.h"
#include "../graph.h"
#include "../utils.h"
#include "../crypt.h"


FILE *db_open(char *file_name) {

	FILE *fp;
	fp = fopen(file_name, "a+");
	return fp;

}

int db_close(FILE *fp) {

	return fclose(fp);

}

int write_record(FILE *fp, ST_CELLPHONE *p) {

	if(fseek(fp, 0, SEEK_END) != 0) {
		return DATABASE_WRITE_FAIL;
	}
	if(fwrite(p, sizeof(ST_CELLPHONE), 1, fp) == 0) {
		return DATABASE_WRITE_FAIL;
	}
	if(fwrite(p->hardware, sizeof(ST_HARDW), 1, fp) == 0) {
		return DATABASE_WRITE_FAIL;
	}

	return DATABASE_WRITE_OK;
}

int input_record(ST_CELLPHONE ***p){

	int r;
	ST_CELLPHONE *cellphone_tmp = malloc(sizeof(ST_CELLPHONE));
        char str_tmp[MAX_BUFF_NAME] = {};

	//MARCA
        printf(" Ingrese Marca: ");
        scanf(" %s", str_tmp);
        strcpy(cellphone_tmp->marca,str_tmp);

	//MODELO
        printf(" Ingrese Modelo: ");
        scanf(" %s", str_tmp);
        strcpy(cellphone_tmp->modelo,str_tmp);

	//IMEI
        printf(" Ingrese IMEI: ");
        scanf(" %li", &cellphone_tmp->imei);

	//PESO
        printf(" Ingrese peso: ");
        scanf(" %f", &cellphone_tmp->peso);

	//HARDW
        cellphone_tmp->hardware = malloc(sizeof(ST_HARDW));
        if(cellphone_tmp->hardware == NULL) {
                printf("No ha sido posible alocar memoria para el registro.");
                return -1;
        }
        printf(" Ingrese memoria interna: ");
	scanf(" %i", &cellphone_tmp->hardware->mem_int);
        printf(" Ingrese memoria raw: ");
	scanf(" %i", &cellphone_tmp->hardware->mem_ram);
        printf(" Ingrese cpu clock: ");
	scanf(" %f", &cellphone_tmp->hardware->cpu_clock);
        printf(" Ingrese cam mpx: ");
	scanf(" %i", &cellphone_tmp->hardware->cam_mpx);

	r = arr_push(p);
	**(*p+r) = *cellphone_tmp;
	return r;

}

int load_records(FILE *fp, ST_CELLPHONE ***p, int salto) { 

	int c=0;
	int len;

	ST_CELLPHONE *cellphone_tmp = malloc(sizeof(ST_CELLPHONE));
	ST_HARDW *hardw_tmp;
       
	char char_buffer[MAX_BUFF_NAME];
       
	if(fseek(fp, 0, SEEK_END) != 0) {
		return DATABASE_READ_FAIL;
        }

	len = ftell(fp);

	if(fseek(fp, 0, SEEK_SET) != 0) {
		return DATABASE_READ_FAIL;
	}

	while(ftell(fp) <  len) {
		c = arr_push(p);
		fread(cellphone_tmp, sizeof(ST_CELLPHONE), 1, fp);

		des_desplazar_letras(cellphone_tmp->marca,char_buffer,salto);
		strcpy(cellphone_tmp->marca,char_buffer);
		des_desplazar_letras(cellphone_tmp->modelo,char_buffer,salto);
		strcpy(cellphone_tmp->modelo,char_buffer);

		hardw_tmp = malloc(sizeof(ST_HARDW));
		fread(hardw_tmp, sizeof(ST_HARDW), 1, fp);
		cellphone_tmp->hardware = hardw_tmp;

		**(*p+c)=*cellphone_tmp;
	}
	return DATABASE_READ_NOTFOUND;
}


void read_csv(char *file_name, ST_CELLPHONE ***cellphones) {


	int c; // getc/fscanf
	int cantidad_registros_exito = 0;
	int cant_cols = 0;
        FILE *fp;
	char *char_buffer = malloc(sizeof(char)*MAX_BUFF_RECORD);
	char delim[] = ",";
	char *line_buffer[MAX_RECORDS] = {NULL};
	int lines = 0;
	ST_CELLPHONE cellphone_tmp;

        if ((fp = fopen(file_name, "r")) == NULL) {
		draw_box("No se pudo abrir el archivo.");
		return;
	}

	if(fseek(fp, 0, SEEK_SET) != 0) {
		draw_box("No se pudo leer el archivo.");
	}

	line_buffer[0] = calloc(MAX_BUFF_RECORD, sizeof(char));
	while((c = getc(fp)) != EOF) {

		if(c == 44) cant_cols++;
		if(c == '\n') {
			if(cant_cols != RECORDS_COLS-1 ) {
				free(line_buffer[lines]);
				line_buffer[lines] = calloc(MAX_BUFF_RECORD,sizeof(char));
			} else {
				line_buffer[lines][strlen(line_buffer[lines])] = '\0';
				line_buffer[++lines] = calloc(MAX_BUFF_RECORD,sizeof(char));
			}
			cant_cols = 0;
			continue;
		}
	       	line_buffer[lines][strlen(line_buffer[lines])] = c; 
	
	}
	while(lines > 0) {
		lines--;
		//marca
		char_buffer = strtok(line_buffer[lines], delim);
		strcpy(cellphone_tmp.marca,char_buffer);
		//modelo
		char_buffer = strtok(NULL, delim);
		strcpy(cellphone_tmp.modelo,char_buffer);
		
		//imei
		char_buffer = strtok(NULL, delim);
		cellphone_tmp.imei = atoi(char_buffer);
		//peso
		char_buffer = strtok(NULL, delim);
		cellphone_tmp.peso = strtof(char_buffer,NULL); 
		//HARDW
		cellphone_tmp.hardware = malloc(sizeof(ST_HARDW));
		if(cellphone_tmp.hardware == NULL) {
			printf("No ha sido posible alocar memoria para el registro.");
			return;
		}
		//mem_int
		char_buffer = strtok(NULL, delim);
		cellphone_tmp.hardware->mem_int = atoi(char_buffer);
		//mem_ram
		char_buffer = strtok(NULL, delim);
		cellphone_tmp.hardware->mem_ram = atoi(char_buffer);
		//cp
		char_buffer = strtok(NULL, delim);
		cellphone_tmp.hardware->cpu_clock = strtof(char_buffer,NULL);
		//cam
		char_buffer = strtok(NULL, delim);
		cellphone_tmp.hardware->cam_mpx = atoi(char_buffer);

		c = arr_push(cellphones);
		**(*cellphones+c) = cellphone_tmp;
		cantidad_registros_exito++;
		free(line_buffer[lines]);
	}
	draw_box("Se importaron los registros.");
	fclose(fp);
	return; 
}



void menu_interactivo(int *live, int *option, ST_CELLPHONE ***p, ST_CELLPHONE ***s, FILE *database, int salto) {

	int i=0;
	int c;
	int imei_nro;
	int registro_nro;
	int registro_cant;
	char char_buffer[MAX_BUFF_MARCA];
	FILE * fpdump;

	draw_multi_box(XMN_OPTIONS, arr_length(*s));

	printf(" ] Opcion: ");
	scanf(" %i", option);
	switch(*option) {
                                case 1: //ingresar registro manualmente
					cls();
					registro_nro = input_record(p);
					print_record(*(*p+registro_nro), registro_nro);

                                break;
                                case 2: //seleccionar por imei
					cls();
                                        printf("\n ] Ingrese IMEI: ");
                                        scanf(" %i", &imei_nro);
					while(*(*p+i) != NULL) {
						if((*(*p+i))->imei == imei_nro) {
							print_record(*(*p+i),i);
							break;
						}
						i++;	
					}
                                break;
                                case 3: //Eliminar registro
					cls();
					print_table(*p);
					printf("\n ] Ingrese ID de registro a borrar: ");
                                        scanf(" %i", &i);
					registro_cant = arr_length(*p) - 1;
					if(i > registro_cant) {
						draw_box("El indice es mayor que la cantidad de elementos en la base!");
						break;
					}


					//libero hardware
					free((*(*p+i))->hardware);
					//libero puntero
					free(*(*p+i)); 
					while( i < registro_cant) {
						*(*p+i) = *(*p+i+1);
						i++;
					}
					*(*p+registro_cant) = NULL;

					if((*p = (ST_CELLPHONE**)realloc(*p, sizeof(ST_CELLPHONE*)*registro_cant+1))  == NULL) {draw_box("no se pudo alocar! Detono todo...\n");break;}

                                break;
                                case 4:
                                        //seleccionar subconjunto (marca o meodelo)
					cls();
					draw_multi_box(XMN_SUB, 0);
					printf(" ] Opcion: ");
					scanf(" %i", option);
					switch(*option) {
						case 1: //MARCA
							printf(" ] Marca: ");
							scanf(" %s", char_buffer);
							while(*(*p+i) != NULL) {
								if(!strcmp((*(*p+i))->marca,char_buffer )) {
									c = arr_push(s);
									**(*s+c) = **(*p+i);
								}
								i++;
							}
						break;
						case 2: //MODELO
							printf(" ] Modelo: ");
							scanf(" %s", char_buffer);
							while(*(*p+i) != NULL) {
								if(!strcmp((*(*p+i))->modelo,char_buffer )) {
									c = arr_push(s);
									**(*s+c) = **(*p+i);
								}
								i++;
							}
						break;
						case 99:
							cls();
						break;
					}
                                break;
				case 5: //imprimir subconjunto seleccionado 
					cls();
					printf("\n ] Imprimir subconjunto.\n");
					print_table_subconjunto(*s);
                                break;
				case 6: //exportar subconjunto seleccionado en CSV 
					cls();
					fpdump = fopen("subconjuntodump.csv","w");
					while(*(*s+i) != NULL) {
						fprintf (fpdump, "%s,%s,%li,%f,%i,%i,%f,%i\n",(*(*s+i))->marca,(*(*s+i))->modelo,(*(*s+i))->imei,(*(*s+i))->peso,(*(*s+i))->hardware->mem_int,(*(*s+i))->hardware->mem_ram,(*(*s+i))->hardware->cpu_clock,(*(*s+i))->hardware->cam_mpx);
						i++;
					}
					draw_box("El subconjunto se exporto a subconjuntodump.csv");
					fclose (fpdump);
                                break;
				case 7: //eliminar subconjunto seleccionado
					cls();
					c = arr_length(*s);
					while(c>0) {
						free(*(*s+c));
						c--;
					}
					*(*s+c) = NULL;
					draw_box("El subconjunto ha sido eliminado");
                                break;
                                case 8: //Imprimir todos los registros
					cls();
					printf("\n ] Imprimir registros.\n");
					print_table(*p);

                                break;
                                case 9: // Exportar base a csv
					cls();
					fpdump = fopen("dbdump.csv","w");
					while(*(*p+i) != NULL) {
						fprintf (fpdump, "%s,%s,%li,%f,%i,%i,%f,%i\n",(*(*p+i))->marca,(*(*p+i))->modelo,(*(*p+i))->imei,(*(*p+i))->peso,(*(*p+i))->hardware->mem_int,(*(*p+i))->hardware->mem_ram,(*(*p+i))->hardware->cpu_clock,(*(*p+i))->hardware->cam_mpx);
						i++;
					}
					draw_box("La base se exporto a dbdump.csv");
					fclose (fpdump);
                                break;
                                case 10:  // guardar base encriptada
					cls();
					fclose(fopen(DATABASE_NAME, "w"));//empty file fix
					ST_CELLPHONE CELLPHONE_CRYPT; 
					while(*(*p+i) != NULL) {
						desplazar_letras((*(*p+i))->marca,CELLPHONE_CRYPT.marca,salto);
						desplazar_letras((*(*p+i))->modelo,CELLPHONE_CRYPT.modelo,salto);
						//strcpy(CELLPHONE_CRYPT.marca,(*(*p+i))->marca);
						//strcpy(CELLPHONE_CRYPT.modelo,(*(*p+i))->modelo);
						CELLPHONE_CRYPT.peso = (*(*p+i))->peso;
						CELLPHONE_CRYPT.imei = (*(*p+i))->imei;
						CELLPHONE_CRYPT.hardware = (*(*p+i))->hardware;
						//print_record(&CELLPHONE_CRYPT);
						write_record(database, &CELLPHONE_CRYPT);
						i++;
					}
					draw_box("La base se ha guardado!");
                                break;
                                case 99:
                                        *live=0;
                                break;
				default:
					cls();
				break;

	}

}


