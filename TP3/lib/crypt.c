#include <stdio.h>
#include <string.h>

void desplazar_letras (char* s,char *e, int salto) {

        int i;
        int new_char;
        int s_length =  strlen(s);

        for(i = 0; i < s_length; i++) {
                if(s[i] == '\0') break;
                new_char = s[i] + salto;
                if(s[i] > 64 && s[i] < 91) {
                                while(new_char > 90 ) {
                                        new_char += 64 - 90;
                                }
                                e[i] = new_char;
                }else if(s[i] > 96 && s[i] < 123) {
                                while(new_char > 122) {
                                        new_char += 96 - 122;
                                }
                                e[i] = new_char;
                }else e[i] = s[i];
        }
        e[i] = '\0';
}
void des_desplazar_letras (char* s,char *e, int salto) {

        int i;
        int new_char;
        int s_length =  strlen(s);

        for(i = 0; i < s_length; i++) {
                if(s[i] == '\0') break;
                new_char = s[i] - salto;
                if(s[i] > 64 && s[i] < 91) {
                                while(new_char < 65 ) {
                                        new_char = 90 - (64 - new_char); //+ 90 - 65;
                                }
                                e[i] = new_char;
                }else if(s[i] > 96 && s[i] < 123) {
                                while(new_char < 97) {
                                        new_char = 122 - (96 - new_char);// + 122 - 95;
                                }
                                e[i] = new_char;
                }else e[i] = s[i];
        }
        e[i] = '\0';
}

