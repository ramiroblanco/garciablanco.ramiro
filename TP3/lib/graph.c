#include <stdio.h>
#include <string.h>
#include <string.h>
#include "../database.h"
#include "../utils.h"

void draw_box(char *msg) {

	int msg_len = strlen(msg);
	printf("\n\n ╔");
	for(int i=0; i<msg_len+2;i++) {
		printf("═");
	}
	printf("╗\n ");

	printf("║ %s ║\n",msg);

	printf(" ╚");
	for(int i=0; i<msg_len+2;i++) {
		printf("═");
	}

	printf("╝\n\n");

}

void draw_multi_box(const char **msgs, int subconjunto) {

	int i=0,j,tmp=0,mlen=0;

	for(;msgs[i]!=NULL;i++) {
		tmp=strlen(msgs[i]);
		if(tmp>mlen) mlen=tmp;
	}

	printf("\n\n ╔");
	for(i=0; i<mlen+2;i++) {
		printf("═");
	}
	printf("╗\n");

	for(i=0;msgs[i]!=NULL;i++) {
		printf(" ║ %s",msgs[i]);
		for(j=strlen(msgs[i]);j<=mlen;j++) printf(" ");
		printf("║\n");
	}
	if(subconjunto > 0) {
		printf(" ╠");
		for(int i=0; i<mlen+2;i++) {
			printf("═");
		}
		printf("╣\n");

		//for(j=0;j<=mlen;j++) printf("   "); 

		printf(" ║ Hay subconjunto! Registros: %-18i",subconjunto);
		printf("║\n");

		printf(" ╚");
		for(int i=0; i<mlen+2;i++) {
			printf("═");
		}
		printf("╝\n\n");
	}else{
		printf(" ╚");
		for(int i=0; i<mlen+2;i++) {
			printf("═");
		}
		printf("╝\n\n");

	}

}

void print_table(ST_CELLPHONE **p) {

	int length = arr_length(p);
	if(length == 0) {
		draw_box("No hay registros para imprimir!");
		return;
	}

	printf("\n ╔══════╦══════════════════╦══════════════════╦══════════════════╦═════════╦════════════╦══════════╦══════════╦═════╗\n");
	printf("%-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s\n", " ║ ID  ", "║ Marca            ", "║ Modelo           ", "║ IMEI             ", "║ Peso    ", "║ CPU CLOCK  ", "║ MEM RAM  ", "║ MEM INT  ","║ MPX ║");	
	printf(" ╠══════╬══════════════════╬══════════════════╬══════════════════╬═════════╬════════════╬══════════╬══════════╬═════╣\n");
	for(int i=0;i<length;i++) {
		printf(" ║ %-4i ║ %-16s ║ %-16s ║ %-16li ║ %-7.2f ║ %-10.2f ║ %-8i ║ %-8i ║ %-3i ║\n", i, p[i]->marca, p[i]->modelo, p[i]->imei, p[i]->peso, p[i]->hardware->cpu_clock, p[i]->hardware->mem_ram, p[i]->hardware->mem_int, p[i]->hardware->cam_mpx);	
	}
	printf(" ╚══════╩══════════════════╩══════════════════╩══════════════════╩═════════╩════════════╩══════════╩══════════╩═════╝\n");

}

void print_table_subconjunto(ST_CELLPHONE **p) {

	int length = arr_length(p);
	if(length == 0) {
		draw_box("No hay registros para imprimir!");
		return;
	}

	printf("\n ╔══════════════════╦══════════════════╦══════════════════╦═════════╦════════════╦══════════╦══════════╦═════╗\n");
	printf("%-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s\n", " ║ Marca            ", "║ Modelo           ", "║ IMEI             ", "║ Peso    ", "║ CPU CLOCK  ", "║ MEM RAM  ", "║ MEM INT  ","║ MPX ║");	
	printf(" ╠══════════════════╬══════════════════╬══════════════════╬═════════╬════════════╬══════════╬══════════╬═════╣\n");
	for(int i=0;i<length;i++) {
		printf(" ║ %-16s ║ %-16s ║ %-16li ║ %-7.2f ║ %-10.2f ║ %-8i ║ %-8i ║ %-3i ║\n", p[i]->marca, p[i]->modelo, p[i]->imei, p[i]->peso, p[i]->hardware->cpu_clock, p[i]->hardware->mem_ram, p[i]->hardware->mem_int, p[i]->hardware->cam_mpx);	
	}
	printf(" ╚══════════════════╩══════════════════╩══════════════════╩═════════╩════════════╩══════════╩══════════╩═════╝\n");

}

void cls() { printf("\e[1;1H\e[2J"); }

void print_record(ST_CELLPHONE* record, int record_id) {

	printf("\n ╔═══════════════════════╗");
	printf("\n ║ Registro: %-12i║", record_id);
	printf("\n ╠═══════════════════════╩═══╦═════════════════════╗");
        printf("\n ║ Marca: %-19s║ Peso: %-14.2f║",record->marca,record->peso);
        printf("\n ║ Modelo: %-18s║ IMEI: %-14li║",record->modelo,record->imei);
	printf("\n ╠═══════════╦═══════════════╩═════════╦═══════════╩═════════════╗");
        printf("\n ║ Hardware: ║ Memoria interna: %-7i║ Memoria RAM: %-11i║",record->hardware->mem_int,record->hardware->mem_ram);
        printf("\n ╚═══════════╣ CPU Clock: %-13.2f║ Camara MPX: %-12i║",record->hardware->cpu_clock,record->hardware->cam_mpx);
        printf("\n             ╚═════════════════════════╩═════════════════════════╝");

}

