/* 
 * Realizar un programa para la gestión de estudiantes, en donde se debe
 * ingresar el nombre, el apellido,  el legajo del estudiante y las notas de cantidad_de_notas.
 * Un estudiante puede tener varias notas (no todos los estudiante tiene la misma
 * cantidad de notas). Se desconoce la cantidad de estudiantes, pero se sabe que no
 * superará los 100 estudiantes.
 * El ingreso de datos finaliza con legajo cero (0). Se debe verificar que los valores
 * ingresados sean mayores a cero (0).
 * Una vez finalizado el ingreso, se debe informar:
 *  - Listar mejores promedio ‑ (estudiantes y nota) considerando estudiantes que hayan dado
 *    más cantidad_de_notas de dos (2) cantidad_de_notas.
 *  - Listar estudiantes que hayan dado dos (2) o menos cantidad_de_notas.
 *  - Listar todos los estudiantes con sus promedios.
 * 
 * Aclaracion: En ningun momento se solicita el detalle de cada nota individual
 *
 * version 0: solo legajo, cantidad de notas y promedio. Nota entre 0 y 10.
 * version 1: agregar nombre y apellido
 * version 2: comprobar legajo duplicado. Se pueden agregar notas en varias entradas diferentes.
 * version 3: comprobar buller lleno
 * version 4: mensaje si es necesario que no existe "mejor promedio"  
 */

#include <stdio.h>
#include <string.h>

/* solo para exit() */
#include <stdlib.h>

#define MAX_ALUMNOS 5 /* 100 */
#define MAX_NOTA 10
#define MIN_NOTA 0
#define MAX_APELLIDO 150
#define MAX_NOMBRE 100

void salir();

void draw_menu(int o);
void draw_notas(int cantidad_de_notas);
void draw_ingreso(int legajo,char nombre[], char apellido[]);

//int ingreso(int *legajos,int *cantidad_de_notas,char *nombres,char *apellidos,float *promedios);
int ingreso(int *legajos,int *cantidad_de_notas,char nombres[][MAX_NOMBRE],char apellidos[][MAX_APELLIDO],float *promedios, int c);

int check_legajo_existe(int leg,int *legajos, int c);

float ingresar_notas(int cantidad_de_notas);

void listar_todos_promedio(int *legajos,char nombres[][MAX_NOMBRE], char apellidos[][MAX_APELLIDO], int *cantidad_de_notas, float *promedios, int c);
void listar_mejores_promedios(int *legajos,char nombres[][MAX_NOMBRE], char apellidos[][MAX_APELLIDO], int *cantidad_de_notas, float *promedios, int c);
void listar_dos_o_menos_cantidad_de_notas(int *legajos,char nombres[][MAX_NOMBRE], char apellidos[][MAX_APELLIDO], int *cantidad_de_notas, int c);

int main() {
	int c=0;	
	int array_lleno=0;
	int legajos[MAX_ALUMNOS];
	int cantidad_de_notas[MAX_ALUMNOS];
	char nombres[MAX_ALUMNOS][MAX_NOMBRE];
	char apellidos[MAX_ALUMNOS][MAX_APELLIDO];
	float promedios[MAX_ALUMNOS];
	int option;
	char salida;

	while(1) {
		draw_menu(array_lleno);
		array_lleno=0;
		scanf(" %i", &option);

		switch (option) {
		case 1:
			if(c < MAX_ALUMNOS) {
				//ingreso(&legajos[c],&cantidad_de_notas[c],nombres[c],apellidos[c],&promedios[c],legajos,c);
				ingreso(legajos,cantidad_de_notas,nombres,apellidos,promedios,c);
				c++;
			} else {
				array_lleno=1;
			}
			break;
		case 2:
			listar_mejores_promedios(legajos,nombres,apellidos,cantidad_de_notas,promedios,c);
			listar_dos_o_menos_cantidad_de_notas(legajos,nombres,apellidos,cantidad_de_notas,c);
			listar_todos_promedio(legajos,nombres,apellidos,cantidad_de_notas,promedios,c);

			printf("Salir? s/n: ");
			scanf(" %c", &salida);
			if(salida == 83 || salida == 115)
				salir();
			break;
		case 5:
			salir();
			break;
		}
	}

	return 0;
}

int ingreso(int *legajos,int *cantidad_de_notas,char nombres[][MAX_NOMBRE],char apellidos[][MAX_APELLIDO],float *promedios, int c) {
	int leg = 0;
	int o = 0;
	while(1) {
		draw_ingreso(leg,"","");
		if(o!=0){
                        printf("\033[12;1f");
                        printf(" ╔═════════════════════════════════════════╗\n");
                        printf(" ║  El legajo                              ║\n");
                        printf(" ╚═════════════════════════════════════════╝\n");
                        printf("\033[13;15f");
                        printf("%i ya existe!",o);
                        o=0;		
		}
		printf("\033[6;22f");
		printf(" ");
		printf("\033[6;22f");
		scanf(" %i", &leg);
		if(leg == 0)
			salir();
		if(check_legajo_existe(leg, legajos, c) == 0) {
			legajos[c] = leg;
			printf("\033[7;14f");
			scanf(" %s", nombres[c]);
			printf("\033[8;16f");
			scanf(" %s", apellidos[c]);
			printf("\033[9;25f");
			scanf(" %i", &cantidad_de_notas[c]);
			promedios[c] = ingresar_notas(cantidad_de_notas[c]);
			break;

		} else o=leg;
		
	}
	return 0;
}

float ingresar_notas(int cantidad_de_notas) {
	float nota;
	float suma;
	int i = 0;
	int o = 0;

	/* ARREGLAR */
	while(i<cantidad_de_notas) {
		draw_notas(i);
		if(o != 0) {
	        	printf("\033[9;1f");
	        	printf(" ╔═════════════════════════════════════════╗\n");
	        	printf(" ║  La nota debe ser menor/igual a         ║\n");
		        printf(" ║  y mayor/igual a                        ║\n");
		        printf(" ╚═════════════════════════════════════════╝\n");
		        printf("\033[10;36f");
		        printf("%i", MAX_NOTA);
		        printf("\033[11;21f");
		        printf("%i",MIN_NOTA);
			o=0;
		}

		printf("\033[6;9f");
		scanf(" %f",&nota );
		if(nota > MAX_NOTA || nota < MIN_NOTA)
			o=1;
		else {
			suma+=nota;
			i++;
		}
	}
	return suma/cantidad_de_notas;

}

void listar_todos_promedio(int *legajos,char nombres[][MAX_NOMBRE], char apellidos[][MAX_APELLIDO], int *cantidad_de_notas, float *promedios, int c) {
	        printf("\n");
        printf("  Listado de todos los alumnos con promedio.\n");
        printf(" ╔═════════════╦═══════════════════════════════════╦════════╦════════╗\n");
        printf(" ║  Legajo     ║ Apellido, nombre                  ║examenes║promedio║\n");
        printf(" ╠═════════════╬═══════════════════════════════════╬════════╬════════╣\n");
        for(int i=0;i<c;i++) {
                printf(" ║             ║                                   ║        ║        ║");
                printf("\033[100D");
                printf("\033[5C");
                printf("%i",legajos[i]);
                printf("\033[100D");
                printf("\033[18C");
                printf("%s, %s",apellidos[i],nombres[i]);
                printf("\033[100D");
                printf("\033[54C");
                printf("%i",cantidad_de_notas[i]);
                printf("\033[100D");
                printf("\033[63C");
                printf("%.2f",promedios[i]);
                printf("\033[100D");
                printf("\033[100C");
                printf("\n");
        }
        printf(" ╚═════════════╩═══════════════════════════════════╩════════╩════════╝\n");
}

void listar_dos_o_menos_cantidad_de_notas(int *legajos,char nombres[][MAX_NOMBRE], char apellidos[][MAX_APELLIDO], int *cantidad_de_notas, int c) {
        printf("\n");
        printf("  Listado alumnos con dos o menos notas.\n");
        printf(" ╔═════════════╦═══════════════════════════════════╦════════╗\n");
        printf(" ║  Legajo     ║ Apellido, nombre                  ║examenes║\n");
        printf(" ╠═════════════╬═══════════════════════════════════╬════════╣\n");
        for(int i=0;i<c;i++) {
		if(cantidad_de_notas[i] > 2)
			continue;
        	printf(" ║             ║                                   ║        ║");
                printf("\033[100D");
                printf("\033[5C");
                printf("%i",legajos[i]);
                printf("\033[100D");
                printf("\033[18C");
                printf("%s, %s",apellidos[i],nombres[i]);
                printf("\033[100D");
                printf("\033[54C");
                printf("%i",cantidad_de_notas[i]);
                printf("\033[100D");
                printf("\033[100C");
                printf("\n");
        }
        printf(" ╚═════════════╩═══════════════════════════════════╩════════╝\n");

}

void listar_mejores_promedios(int *legajos,char nombres[][MAX_NOMBRE], char apellidos[][MAX_APELLIDO], int *cantidad_de_notas, float *promedios, int c) {
	int i=0, promedio_mas_alto=0, promedio_mas_alto_indice=-1,promedio_mas_alto_unico=0;
        for(i=0;i<c;i++) {
                if(cantidad_de_notas[i] <= 2)
                        continue;
		if(promedios[i] > promedio_mas_alto) {
			promedio_mas_alto = promedios[i];
			promedio_mas_alto_indice=i;
			promedio_mas_alto_unico=0;
		} else if(promedios[i] == promedio_mas_alto) {
			promedio_mas_alto_unico=1;
		}
	}
        printf("  Listado de mejores promedios.\n");
        printf(" ╔═════════════╦═══════════════════════════════════╦════════╦════════╗\n");
        printf(" ║  Legajo     ║ Apellido, nombre                  ║examenes║promedio║\n");
        printf(" ╠═════════════╬═══════════════════════════════════╬════════╬════════╣\n");
        for(i=0;i<c;i++) {
                if(cantidad_de_notas[i] <= 2)
                        continue;
                printf(" ║             ║                                   ║        ║        ║");
                printf("\033[100D");
                printf("\033[5C");
                printf("%i",legajos[i]);
                printf("\033[100D");
                printf("\033[18C");
                printf("%s, %s",apellidos[i],nombres[i]);
                printf("\033[100D");
                printf("\033[54C");
                printf("%i",cantidad_de_notas[i]);
                printf("\033[100D");
                printf("\033[63C");
                printf("%.2f",promedios[i]);
		if(promedio_mas_alto_indice == i && promedio_mas_alto_unico == 0) {
                	printf("\033[100D");
                	printf("\033[72C");
			printf("<<< MEJOR PROMEDIO");
		}
                printf("\033[100D");
                printf("\033[130C");
                printf("\n");
        }
        printf(" ╚═════════════╩═══════════════════════════════════╩════════╩════════╝\n");
	if(promedio_mas_alto_unico != 0 || promedio_mas_alto_indice == -1) 
		printf(" [!No hay mejor promedio.]\n");
}

int check_legajo_existe(int leg,int *legajos, int c) {
	int o=0;
	for(int i=0;i<c;i++) {
		if(legajos[i] == leg)
			o=1;
	}
	return o;
}

void draw_menu(int o) {
	printf ("\033[2J");
	printf("\033[2;5f");
	printf("\n");
	printf(" ╔═════════════════════════════╗\n");
	printf(" ║  MENU                       ║\n");
	printf(" ╟─────────────────────────────╢\n");
	printf(" ║  1) ingresar alumno         ║\n");
	printf(" ║  2) Listar                  ║\n");
	printf(" ║                             ║\n");
	printf(" ║  5) Salir                   ║\n");
	printf(" ╚═════════════════════════════╝\n");
	if( o == 1 ) {
		printf(" ╔═════════════════════════════════════════╗\n");
		printf(" ║  El array esta lleno                    ║\n");
		printf(" ╚═════════════════════════════════════════╝\n");
	}
	printf("  ==> ");
}

void draw_ingreso(int legajo,char nombre[], char apellido[]) {
        printf ( "\033[2J");
        printf("\033[2;5f");
	printf("\n");
        printf(" ╔═══════════════════════════════════════════════════════╗\n");
        printf(" ║  INGRESO                                              ║\n");
        printf(" ╟───────────────────────────────────────────────────────╢\n");
        printf(" ║  -LEGAJO(0 sale):                                     ║\n");
        printf(" ║  -NOMBRE:                                             ║\n");
        printf(" ║  -APELLIDO:                                           ║\n");
        printf(" ║  -CANTIDAD DE NOTAS:                                  ║\n");
        printf(" ╚═══════════════════════════════════════════════════════╝\n");

	if(legajo != 0)
        	printf("\033[6;22f%i",legajo);
        printf("\033[7;14f%s",nombre);
        printf("\033[8;16f%s",apellido);
}

void draw_notas(int nota_numero) {
        printf ( "\033[2J");
        printf("\033[2;5f");
        printf("\n");
        printf(" ╔══════════════════════════════════════╗\n");
        printf(" ║  INGRESE NOTA #0                     ║\n");
        printf(" ╟──────────────────────────────────────╢\n");
        printf(" ║  ==>                                 ║\n");
        printf(" ╚══════════════════════════════════════╝\n");
        printf("\033[4;19f%i",nota_numero+1);
}

void salir() {
        printf ( "\033[2J");
        printf("\033[2;5f");
	printf("\n");
        printf(" ╔════════════════════╗\n");
        printf(" ║    ┌─┐┬ ┬┌─┐┬ ┬    ║\n");
        printf(" ║    │  ├─┤├─┤│ │    ║\n");
        printf(" ║    └─┘┴ ┴┴ ┴└─┘    ║\n");
        printf(" ╚════════════════════╝\n");
	printf("\n");
	printf("\n");
	exit(0);
}
