/*
 *
 * gcc -o ej -lm
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define MAX_CRYPT_LENGTH 300
#define MAX_STRING_LENGTH 300

/* Función que debe generar un desplazamiento alfabético de cada una de las letras del string recibido como argumento, una cantidad aleatoria de posiciones (entre 1 y 25). En caso de sobrepasar la ‘Z’, se deberá continuar con la ‘A’. Los números y espacios deben permanecer invariables. La función debe retornar la cantidad de posiciones que se han desplazado los caracteres.  Ej: HOLA 54. Si el */
int desplazar_letras (char*,char *);

/* Función que realiza el proceso inverso de la función anterior. Recibe un string y una cantidad y debe desplazar en sentido inverso las letras para recuperar el texto original */
void desencriptar_texto (char *,char*,int );

/*Para encriptar los números del string el procedimiento será el siguiente: se debe
convertir el número en octal, para luego reemplazar cada uno de sus dígitos por los
siguientes caracteres:
0 $
1 %
2 &amp;
3 *
4 @
5 !
6 +
7 =
Adicionalmente, se debe agregar el carácter # tanto al comienzo como al final del numero
encriptado.  EJ: “305” (461 en octal)*/
void encriptar_numeros (char*,char*);


void desencriptar_numero (char*, char*);


int es_encriptable (char *);

/*auxs*/
//void aoctal(char *c);
void char_replace(char *c);





// MAIN	
int main(int argc, char **argv) {


	//char *s = "hola4hola_ HOLA_1234_abc_ABC_z_Z"; //para test
	char e[MAX_CRYPT_LENGTH];
	char u[MAX_STRING_LENGTH];
	char e_n[MAX_CRYPT_LENGTH];

	if(argc < 2) {
		printf("No hay argumentos\n");
		return 0;
	}


	if(es_encriptable(argv[1])) printf("Es encriptable\n");
	else {
		printf("No es encriptable\n"); 
		return 0;
	}

	int salto = desplazar_letras(argv[1],e);

	printf("String: %s\n",argv[1]);
	printf("Salto: %i\n", salto);

	printf("Hash: %s\n",e);
	desencriptar_texto (u,e,salto);
	encriptar_numeros (e,e_n);

	printf("Hash numbers: %s\n",e_n);

	return 0;
}



int desplazar_letras (char* s,char *e) {

	int i;
	int new_char; 

	srand((unsigned) time(NULL));
	int salto = rand() % 9;

	int s_length =  strlen(s);

	for(i = 0; i < s_length; i++) {
		if(s[i] == '\0') continue;
		if( (s[i] > 64 && s[i] < 91 ) || (s[i] > 96 && s[i] < 123) ) {
			new_char = s[i] + salto;
			if(s[i] < 96 ) {// minusc
				if(new_char > 90) new_char = new_char - 90 + 64;
			} else  {
				if(new_char > 122) new_char = new_char - 122 + 96;
			}
			e[i] = new_char;
		}else e[i] = s[i];
		
	} 
	e[i] = '\0';
	return salto;
}


void desencriptar_texto (char* u,char* e,int salto) {

	int i;
	int new_char;
	int e_length =  strlen(e);

	for(i = 0; i < e_length; i++) {

		if(e[i] > 47 && e[i] < 58) {
			u[i] = e[i];
			continue;
		}
			new_char = e[i] - salto;

			if(new_char < 96 ) {// minusc
				if(new_char < 65) new_char = new_char + 90 - 64;
			} else  {
				if(new_char < 97) new_char = new_char + 122 - 96;
			}

			if( (new_char > 64 && new_char < 91 ) || (new_char > 96 && new_char < 123) )  {
				u[i] = new_char;
			} else {
			
				u[i] = e[i];
			}

	} 
	u[i] = '\0';
	printf("Decrypted: %s\n", u);
}



void encriptar_numeros (char *e, char *e_n) {

	int e_length = strlen(e);
	int i_i = 0; 
	int d;
	char number[MAX_STRING_LENGTH];
	int number_c = 0;
	//int tmp_int;


	for(int i = 0; i < e_length; i++) {

		if(e[i] > 47 && e[i] < 58) {
			number[number_c] = e[i];
			number_c++;
		} else {

			if(number_c > 0) {
				//calculo
				number[number_c] = '\0';
				//aoctal(number);
				char_replace(number);
				e_n[i_i] = 35;
				i_i++;
				for(d=0;d<number_c;d++){
					e_n[i_i] = number[d];
					i_i++;
				}
				e_n[i_i] = 35;
				i_i++;
				number_c = 0;
				i--;

			}else{
				e_n[i_i] = e[i];
				i_i++;
			}
		}

	}
			if(number_c > 0) {
				//calculo
				number[number_c] = '\0';
				//aoctal(atoi(number));
				char_replace(number);
				e_n[i_i] = 35;
				i_i++;
				for(d=0;d<number_c;d++){
					e_n[i_i] = number[d];
					i_i++;
				}
				e_n[i_i] = 35;
				i_i++;
				number_c = 0;

			}
		e_n[i_i] = '\0';

}

/*
void desencriptar_numero (char *e_n, char *e) {

	int e_n_length = strlen(e_n);
	int number_c = 0;
	char number[MAX_STRING_LENGTH];
	for(int i = 0; i < e_length; i++) {

		if(e_n[i] == 35)
			number[number_c] = e[i];
			number_c++;
		} else {

			if(number_c > 0) {
				//calculo
				number[number_c] = '\0';
				//aoctal();
				e_n[i_i] = 35;
				i_i++;
				for(d=0;d<number_c;d++){
					e_n[i_i] = number[d];
					i_i++;
				}
				e_n[i_i] = 35;
				i_i++;
				number_c = 0;
				i--;

			}else{
				e_n[i_i] = e[i];
*/
int es_encriptable (char *s) {

	int s_length = strlen(s);
	int c=1;
	for(int i = 0; i < s_length;i++) 
	if( ( (s[i] < 65 || s[i] > 90 ) && (s[i] < 97 || s[i] > 122) && ( s[i] < 48 || s[i] > 57) ) &&  s[i] != ' ') c=0;

	return c;

}


/* void aoctal(char *c)
{
	int c_length = strlen(c);
	char tmp[c_length];

	int i = 1;

    while ((s[i] - 48) > 0)
    { oc_n += (n % 8) * i;
        n /= 8;
        //n  8;
        i *= 10;
    }

    return oc_n;
} */

void char_replace(char *c) {

	int c_length = strlen(c);
	char tmp[c_length];
	int characters[] = {'$','%','&','*','@','!','+','='};

	for(int i = 0; i < c_length; i++){
		tmp[i] = characters[c[i] - 48];
	}
	strcpy(c,tmp);
}
