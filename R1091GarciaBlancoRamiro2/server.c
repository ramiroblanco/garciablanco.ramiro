#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>

#define MAX_ALUMNOS 100
#define BUFFER 100

#define SLEEP_PROFESOR 3
#define FILE_DB "db.bin"
#define FILE_DB_LIVE "db_live.bin"
#define FILE_MAX_SIZE 1024

typedef struct alumnos
{
	char nombre[20];
	char apellido[20];
	int legajo;
}ALUMNOS;

ALUMNOS st_alumnos[MAX_ALUMNOS];

int live=1;

void handler_sigint(int signal_number) {
        printf(" Shuting down (%i)\n",signal_number);
	//live=0;
}
void handler_sigchld(int signal_number) {
        printf(" Child exits. (%i)\n",signal_number);
	//wait(NULL);
}

int main() {

        struct sigaction sa_int;
        struct sigaction sa_chld;
        memset(&sa_int, 0, sizeof(struct sigaction));
        memset(&sa_chld, 0, sizeof(struct sigaction));
        sa_int.sa_handler = handler_sigint;
        sa_chld.sa_handler = handler_sigchld;
        //sigaction(SIGINT, &sa_int, NULL);
        sigaction(SIGCHLD, &sa_chld, NULL);


	// char local_ip_address[] = "localhost";
	int local_port = 8010;
	int max_connections = 10;
	char *buff;
	int aux; 
	int id;
	int alumnos_conectados=0;
	int resp_code=0;
	FILE *fp_append = fopen(FILE_DB, "a");
	FILE *fp_read = fopen(FILE_DB, "r");
	FILE *fp_append_live = fopen(FILE_DB_LIVE, "a");
	FILE *fp_read_live = fopen(FILE_DB_LIVE, "r");
	FILE *fp_alumno_file;
	int len,file_size;
	time_t tstamp;

	ALUMNOS *st_alumno;

	// variables auxiliares para el manejo del system call de socket --
	int sockaux;
	struct sockaddr_in my_addr;
	int newfd; 	/* Por este socket duplicado del inicial se transaccionará*/
	struct sockaddr_in their_addr;  /* Contendra la direccion IP y número de puerto del cliente */
	unsigned int sin_size = sizeof(struct sockaddr_in);

	// -- 1) crear un socket --
	if ((sockaux = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		fprintf(stderr, "Error en función socket. Código de error %s\n", strerror(sockaux));
		return -1;
	}

	// eliminar timeout
	int reuse  = 1;
	setsockopt(sockaux, SOL_SOCKET, SO_REUSEADDR, &reuse , sizeof(reuse)); 

	// -- familia de sockets INET para UNIX  --
	my_addr.sin_family = AF_INET;		

	// -- convierte el entero formato PC a entero formato network --
	my_addr.sin_port = htons(local_port);	

	// -- automaticamente usa la IP local (todas las que tenga) --
	my_addr.sin_addr.s_addr = INADDR_ANY;	

	// -- rellena con ceros el resto de la estructura  --
	bzero(&(my_addr.sin_zero), 8);



	// -- 3) con la estructura sockaddr_in completa, se declara en el Sistema que este 
	//       proceso escuchará pedidos por la IP y el port definidos --
	if ( (aux = bind (sockaux, (struct sockaddr *) &my_addr, sizeof(struct sockaddr))) == -1) {
		fprintf(stderr, "Error en función bind. Código de error %s\n", strerror(aux));
		return -1;
	}

	// -- 4) Habilitamos el socket para recibir conexiones, con una cola de conexiones 
	//       en espera que tendrá como máximo el tamaño especificado en max_connections --
	//       listen es 'non bloking'
	if ((aux = listen (sockaux, max_connections)) == -1) {
		fprintf(stderr, "Error en función listen. Código de error %s\n", strerror(aux));
		return -1;
	}
	while(1) {
		printf("Escucho...\n");
		id=fork();
		if(id==0) {
			if ((newfd = accept(sockaux, (struct sockaddr *)&their_addr, &sin_size)) == -1) {
				fprintf(stderr, "Error en función accept. Código de error %s\n", strerror(newfd)); return -1;
			} else { 
				printf  ("Conexión desde:  %s\n", inet_ntoa(their_addr.sin_addr));
				if (alumnos_conectados< MAX_ALUMNOS) {
					id=fork();
					if(id==0) {
					while (live) {
						buff=malloc(sizeof(char)*BUFFER);
						if(read(newfd, buff, sizeof(char)*BUFFER)) {

							if(!strcmp(buff,"LOGIN_ALUMNO")){      //ALUMNO
								printf("Login alumno...\n");
								resp_code=200;
								if (write (newfd, &resp_code, sizeof(int)) != -1) {
									st_alumno = malloc(sizeof(ALUMNOS));
									if(read(newfd, st_alumno, sizeof(ALUMNOS))!= -1) {
											printf("Alumno: \"%s,%s\" legajo: %i\n",st_alumno->apellido,st_alumno->nombre,st_alumno->legajo);
											if(fseek(fp_append_live, 0, SEEK_END) != 0) {
												printf("seek failed\n");
											}
											if(fwrite(st_alumno, sizeof(ALUMNOS), 1, fp_append_live) == 0) {
												printf("write failed\n");
											}
									}
									fclose(fp_append_live);
									free(st_alumno);
								}
							} else if(!strcmp(buff,"LOGIN_PROFESOR")) {    //PROFESOR
								printf("Login profesor\n");
								if(fseek(fp_read_live, 0, SEEK_END) != 0) {
									printf("seek file!\n");
								}

								len = ftell(fp_read_live);
								//len = len > 0 ? len/sizeof(ALUMNOS) : 0;
								if(len > 0){
									printf("LEN: %i\n",len);
									buff=malloc(sizeof(char)*BUFFER);
									strcpy(buff,"FINISH");
									if (write (newfd, buff, sizeof(char)*BUFFER) != -1) {
										//free(buff);
										if(read(newfd, &resp_code, sizeof(ALUMNOS))!= -1) {
											printf("Req code: %i\n",resp_code);
											if(resp_code == 101) {
												if (write (newfd, &len, sizeof(int)) != -1) {
													printf("Envio file size: %i\n",len);
													char *file = malloc(len);
													if(fseek(fp_read_live, 0, SEEK_SET) != 0) {
														printf("seek file!\n");
													}
													fread(file, len, 1, fp_read_live);
													if (write(newfd, file, len) != -1) {
														printf("sent file! \n");
													}
													//free(file);
													//envio archivos de alumnos
													//char *path;
													//buff=malloc(sizeof(char)*BUFFER);
													//while(1) {
														//path = malloc(sizeof(char)*BUFFER);
														//path[0] = '\0';
														//strcat(path,"alumnos_files/");
													//if(read(newfd, &buff, sizeof(char)*BUFFER)!= -1) {
													//		printf("__%s\n",buff);
															//strcat(path,buff);
															//if(!strcmp(buff,"XXSALIRXX")) break;
															//if(!strcmp(buff,"")) break;
															//fp_alumno_file = fopen(FILE_DB_LIVE, "r");


													//	}
														//free(buff);
														//free(path);
													//}
												}
											}
										}
										//while(fread(, sizeof(char), 1, fp);
										//	write(newfd, &c, sizeof(int)); 
										//if (write (newfd,&len, sizeof(int)) != -1) {
										//	printf("Send profesor cant alumnos %i\n", len);
										//}
									}
								} else {
									printf("LEN: %i\n",len);

									buff=malloc(sizeof(char)*12);
									if(len > 0) {
										strcpy(buff,"FINISH");
										if (write (newfd, buff, sizeof(char)*BUFFER) != -1) {
										}
									
									
									} else {
										strcpy(buff,"WAITING");
										if (write (newfd, buff, sizeof(char)*BUFFER) != -1) {
										}
									
									}

								}
								//sleep(SLEEP_PROFESOR);
								printf("end profesor\n");

							} else if(!strcmp(buff,"ENVIO_ARCHIVO")) {
								resp_code=200;
								if (write (newfd, &resp_code, sizeof(int)) != -1) {
									if(read(newfd, &file_size, sizeof(ALUMNOS))!= -1) {
											printf("recibo archivo size: \"%i\"\n",file_size);
											char *file_name = malloc(sizeof(char)*BUFFER);
											char *file = malloc(sizeof(char)*file_size);
											strcpy(file_name, "alumnos_files/");
											strcat(file_name, st_alumno->apellido); //MAL
											FILE *fp_alumno = fopen(file_name, "w+");
											if(read(newfd, file, file_size)== -1) {
												printf("unable to read file alumno\n");
											}
											if(fwrite(file, file_size, 1, fp_alumno) == 0) {
												printf("unable to write file alumno\n");
											}

											fclose(fp_alumno);
											free(file_name);
											free(file);
											/*if(fseek(fp_append, 0, SEEK_END) != 0) {
												printf("seek failed\n",buff);
											}
											if(fwrite(st_alumno, sizeof(ALUMNOS), 1, fp_append) == 0) {
												printf("write failed\n",buff);
											}*/
											resp_code=200;
											if (write (newfd, &resp_code, sizeof(int)) != -1) {
												printf("Adios alumno\n");
												exit(0);
											}
									}
								}
							} else  if(!strcmp(buff,"KEEP_ALIVE")) {
								tstamp=time(NULL);
								printf("recibo keep alive %li\n", tstamp);

								

							} else  if(!strcmp(buff,"GAME_OVER")) {
								printf("recibo game over\n");
							}
						} else {
							printf("SALGO CHILD\n\n");
							live=0;
							//return 0;
						}
						free(buff);
					}
					}//child child
					exit(0);
				} 
		}
			} else { 
				wait(NULL); //Childs exit
			}
	}  

	return 0;
}
