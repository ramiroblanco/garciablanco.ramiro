#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>

// -- El puerto donde se conectará, lado servidor --
#define PORT 8010	
#define HOST "localhost"
#define PARCIAL_DOC "parcial.doc"

// -- máxima cantidad de bytes que puede recibir en una transacción --
#define MAXDATASIZE 4096 
#define MAX_TIME_MINS 1
#define SLEEP_SECONDS 6
#define BUFFER 100

int live=1;

typedef struct alumnos {
	char nombre[20];
	char apellido[20];
	int legajo;
}ALUMNO;

int conectar (char* hostname, int port);
int send_login(int sockfd, ALUMNO *st_alumno);
int send_file_server(int sockfd, FILE *fp);
int send_keep_alive(int sockfd);


void handler_sigint(int signal_number) {
        printf(" Finalice el examen! (%i)\n",signal_number);
}
void handler_sigalrm(int signal_number) {
        printf("El tiempo ha finalizado! (%i)\n",signal_number);
	live=0;
}

int main(int argc, char * argv[]) {

	struct sigaction sa_int;
	struct sigaction sa_alrm;
	memset(&sa_int, 0, sizeof(struct sigaction));
	memset(&sa_alrm, 0, sizeof(struct sigaction));
	sa_int.sa_handler = handler_sigint;
	sa_alrm.sa_handler = handler_sigalrm;
	sigaction(SIGINT, &sa_int, NULL);
	sigaction(SIGALRM, &sa_alrm, NULL);

	//alarm(MAX_TIME_MINS*60);
	alarm(15);

	ALUMNO st_alumno;

	int sockfd;  /*File Descriptor para sockets*/
	FILE *fp;
	int req_code,resp_code,file_size;
	// -- Tratamiento de la línea de comandos --
	if (argc < 3) {
		fprintf(stderr,"uso: %s nombre apellido legajo\n",argv[0]);
		exit(1);
	}

	strcpy(st_alumno.nombre,argv[1]);
	strcpy(st_alumno.apellido,argv[2]);
	st_alumno.legajo = atoi(argv[3]);

	// -- conectarse al servidor --
	sockfd = conectar (HOST, PORT);

	send_login(sockfd,&st_alumno);

	while(live) {
	
		sleep(SLEEP_SECONDS);
		if ((fp = fopen(PARCIAL_DOC, "r")) == NULL) {
			send_keep_alive(sockfd);
		} else send_file_server(sockfd,fp);
	}


	// -- vevolvemos recursos al sistema --
	close(sockfd);
	return 0;
}


int conectar (char* hostname, int port) {

	int sockfd;
	struct hostent *he;	            /* Se utiliza para convertir el nombre del host a su dirección IP */
	struct sockaddr_in their_addr;  /* dirección del server donde se conectará */

	// -- convertimos el nombre del host a su dirección IP  --
	if ((he = gethostbyname ((const char *) hostname)) == NULL) {
		fprintf(stderr, "Error en Nombre de Host");
		exit(1);
	}

	// -- creamos el socket  --
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Error en creación de socket");
		exit(1);
	}

	// -- establecemos their_addr con la direccion del server --
	their_addr.sin_family = AF_INET;
	their_addr.sin_port = htons(port);
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);

	// -- intentamos conectarnos con el servidor  --
	if (connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1) {
		fprintf(stderr, "Error tratando de conectar al server");
		exit(1);
	}
	return sockfd;
}

int send_file_server(int sockfd, FILE *fp) {

	char *msg = "ENVIO_ARCHIVO";
	int resp_code,file_size;
	char *file;

	if (write (sockfd, msg, sizeof(char)*BUFFER) != -1) {
                if(read(sockfd, &resp_code, sizeof(int))!= -1) {
                        if(resp_code == 200) {
				if(fseek(fp, 0, SEEK_END) != 0) {
					printf("seek file!\n");
				}
				file_size = ftell(fp);
				if(fseek(fp, 0, SEEK_SET) != 0) {
					printf("set file!\n");
				}
				
				printf("Envio archivo size: \"%i\"\n",file_size);
                                if (write (sockfd, &file_size, sizeof(int)) == -1) {
                                        perror("Error sending user data");
                                }
				//while(ftell(fp) <  file_size) {
				file = malloc(file_size);
					fread(file, file_size, 1, fp);
					if (write (sockfd, file, file_size) == -1) {
						perror("Error sending user data");
					}
					free(file);
					if(read(sockfd, &resp_code, sizeof(int))!= -1) {
						if(resp_code == 200) {
							printf("Archivo enviado. Gracias.\n");
							exit(0);
						}
					}
				//}
                        }
                }

		//exit (1);
	}
}

int send_keep_alive(int sockfd) {
	char *msg = "KEEP_ALIVE";
	if (write (sockfd, msg, sizeof(char)*BUFFER) == -1)
	{
		perror("Error KEEP ALIVE");
		//exit (1);
	}
}

int send_game_over(int sockfd) {
	char *msg = "GAME_OVER";
	if (write (sockfd, msg, sizeof(char)*BUFFER) == -1)
	{
		perror("Error sending GAME OVER");
		//exit (1);
	}
}

int send_login(int sockfd, ALUMNO *st_alumno) {
	char *msg = "LOGIN_ALUMNO";
	char buff[BUFFER];
	int resp_code=0;
	if (write(sockfd, msg, sizeof(char)*BUFFER))
	{
		if(read(sockfd, &resp_code, sizeof(int))!= -1) {
			if(resp_code == 200) {
				if (write (sockfd, st_alumno, sizeof(char)*BUFFER) == -1) {
					perror("Error sending user data");
				}
			}
		}
	}
}
