#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>

// -- El puerto donde se conectará, lado servidor --
#define PORT 8010	
#define HOST "localhost"
#define PARCIAL_DOC "parcial.doc"

// -- máxima cantidad de bytes que puede recibir en una transacción --
#define MAXDATASIZE 4096 
#define MAX_TIME_MINS 100
#define SLEEP_SECONDS 6
#define BUFFER 100

#define CREDS "docente"

typedef struct alumnos {
	char nombre[20];
	char apellido[20];
	int legajo;
}ALUMNO;
typedef struct profesor {
	char nombre[20];
	char apellido[20];
	int legajo;
} PROFESOR;

int conectar (char* hostname, int port);
int send_login(int sockfd, PROFESOR *st_profesor);

int main(int argc, char * argv[])
{

	PROFESOR st_profesor = {"docente","docente",0};

	ALUMNO *st_alumno;

	int sockfd;  /*File Descriptor para sockets*/
	int numbytes;/*Contendrá el número de bytes recibidos por read () */
	char buf[MAXDATASIZE];  /* Buffer donde se reciben los datos de read ()*/
	FILE *fp;
	int resp_code;
	int leg;
	// -- Tratamiento de la línea de comandos --
	if (argc < 3) {
		fprintf(stderr,"uso: %s nombre apellido legajo\n",argv[0]);
		exit(1);
	}
	if(strcmp(argv[1],CREDS) || strcmp(argv[2],CREDS) || atoi(argv[3]) != 0){
		printf("No es docente\n");
		exit(1);
	}


	// -- conectarse al servidor --
	sockfd = conectar (HOST, PORT);

	send_login(sockfd,&st_profesor);

	// -- recibimos los datos del servidor --
	//while( read (sockfd, &resp_code, sizeof(resp_code))!= -1) {
	//	printf("recibido: %d\n",resp_code);
	//}

	
		/*if ((fp = fopen(PARCIAL_DOC, "r")) == NULL) {
			printf("No se pudo abrir parcial.doc.\n");
			send_keep_alive(sockfd);
		} else send_file_server(sockfd);*/

	 if(read(sockfd, &resp_code, sizeof(int))!= -1) {
		 printf("Server code: %i\n",resp_code);
	 }


	// -- vevolvemos recursos al sistema --
	close(sockfd);
	return 0;
}


int conectar (char* hostname, int port) {

	int	sockfd;
	struct hostent *he;	            /* Se utiliza para convertir el nombre del host a su dirección IP */
	struct sockaddr_in their_addr;  /* dirección del server donde se conectará */

	// -- convertimos el nombre del host a su dirección IP  --
	if ((he = gethostbyname ((const char *) hostname)) == NULL) {
		fprintf(stderr, "Error en Nombre de Host");
		exit(1);
	}

	// -- creamos el socket  --
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Error en creación de socket");
		exit(1);
	}

	// -- establecemos their_addr con la direccion del server --
	their_addr.sin_family = AF_INET;
	their_addr.sin_port = htons(port);
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);

	// -- intentamos conectarnos con el servidor  --
	if (connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1) {
		fprintf(stderr, "Error tratando de conectar al server\n");
		exit(1);
	}
	return sockfd;
}

int send_file_server(int sockfd) {

	char *msg = "ENVIO_ARCHIVO";

	if (write (sockfd, msg, sizeof(char)*BUFFER) == -1)
	{
		perror("Error escribiendo mensaje en socket");
		//exit (1);
	}
}

int send_login(int sockfd, PROFESOR *st_profesor) {
	char *msg = "LOGIN_PROFESOR";
	char buff[BUFFER];
	int resp_code=0,req_code=0;
	int len;
	char *file;
	ALUMNO *st_alumno = malloc(sizeof(ALUMNO));
	if (write(sockfd, msg, sizeof(char)*BUFFER))
	{
		printf("espero resp\n");
		if(read(sockfd, &buff, sizeof(char)*BUFFER)!= -1) {
			printf("Logueado como profesor!\n");
			if(!strcmp(buff, "FINISH")){
				printf("FINISH!\n");

				req_code=101;
				if (write(sockfd, &req_code, sizeof(int))) {
					printf("send req code\n");
					//get file size
					if(read(sockfd, &len, sizeof(int))!= -1) {
						//get file 
						printf("file size: %i\n",len);
						file = malloc(len);
						if(read(sockfd, file, len)!= -1) {
							FILE *fp_alumnos = fopen("alumnos.bin", "w+");
							if(fwrite(file, len, 1, fp_alumnos) == 0) {
								printf("unable to write file alumno\n");
							}
							fclose(fp_alumnos);
							free(file);
							fp_alumnos = fopen("alumnos.bin", "r");
							fseek(fp_alumnos, 0, SEEK_SET);
							char test[] = "test";
							while(fread(st_alumno, sizeof(ALUMNO), 1, fp_alumnos)) {
								printf("Alumno: \"%s,%s\" legajo: %i\n",st_alumno->apellido,st_alumno->nombre,st_alumno->legajo);
								//if (write(sockfd, st_alumno->apellido, sizeof(char)*BUFFER)) {
							//	if (write(sockfd, &test, sizeof(char)*BUFFER)) {

							//		sleep(2);
							//	}

							}


						}

					}
				}

				exit(0);

			} else if(!strcmp(buff, "TESTING")){
				printf("TESTING!\n");

			} else if(!strcmp(buff, "WAITING")){
				printf("WAITING\n");

			}
		}
	}
}
