#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_NAME_LEN 255
#define MAX_RECORDS 100


/*
 * a) leer y escribir una estructura PERSONA en un archivo
 * b) leer y escribir un array de estructuras PERSONA en un archivo
 * c) leer y escribir un array de punteros a estructuras PERSONA. El array con memoria dinamica
 * d) hacer un menu con las siguientes opciones: 1) cargar, 2) buscar por nombre, 3) buscar por id. cuando el programa inicia carga la informacion desde el archivo
 * e) modificar el programa anterior para que la base de datos este ordena siempre por id. cuando se carque un nuevo elemento se inserte ordenado 
 * f) modificar la funciona de buscar por id para que sea eficiente con la premisa que la base de datos esta ordenada ( fseek )
 * g) implementar funcionar que modifica un registro por id
 */

// la estructura no tiene campos dinamicos
struct persona {
	int id; // unico y consecutivo!
	char nombre[STR_NAME_LEN];
	int edad;
	float altura;
	float peso;
};

typedef struct persona PERSONA;

void escribir(PERSONA *p, FILE *fp, int id);
int cargar_todo( FILE *fp, PERSONA **records);
void leer( FILE *fp, int id);
void buscar_nombre( PERSONA **records, char *nombre, int records_count);
void imprimir(PERSONA *q );

PERSONA* cargar_persona();
void modificar_registro(PERSONA **records, int id, FILE *fp);

int main(){
	
	int option = 0, live = 1, records_count = 0;

	FILE *fp = fopen("data", "r+");

	if(fp == NULL) {
		printf("No se pudo abrir el archivo.\n");
		exit(1);
	}

	PERSONA *p = NULL;
	PERSONA *records[MAX_RECORDS] = {NULL};

	char nombre_buscar[STR_NAME_LEN];

	records_count = cargar_todo(fp, records);

        while(live) {
		printf ("\033[2J");
		printf("\033[2;5f\n");
                if(option > 0) {
                        switch(option) {
                                case 1:
					p = cargar_persona();
					escribir( p, fp, records_count);
					records_count++;
					break;
                                case 2:
					printf("Buscar por nombre: ");
					scanf(" %s", nombre_buscar);
					buscar_nombre(records, nombre_buscar, records_count);
					break;
                                case 3:
					printf("Buscar ID: ");
					scanf(" %i", &option);
					leer(fp, option);
					break;
                                case 4:
					printf("ID a modificar: ");
					scanf(" %i", &option);
					modificar_registro(records, option, fp);
					break;
                                case 5:
					records_count = cargar_todo(fp, records);
					printf("Registros en archivo: %i \n", records_count);
					break;
                                case 6: 
					fclose(fp);
					exit(0);
					break;
			}
		}

		printf("\n");
		printf(" ╔═════════════════════════════╗\n");
		printf(" ║  MENU                       ║\n");
		printf(" ╟─────────────────────────────╢\n");
		printf(" ║  1) INGRESAR PERSONA        ║\n");
		printf(" ║  2) BUSCAR POR NOMBRE       ║\n");
		printf(" ║  3) BUSCAR POR ID           ║\n");
		printf(" ║  4) MODIFICAR REGISTRO      ║\n");
		printf(" ║  5) IMPRIMIR TODO           ║\n");
		printf(" ║                             ║\n");
		printf(" ║  6) Salir                   ║\n");
		printf(" ╚═════════════════════════════╝\n");
		printf("  ==> ");
		scanf(" %i", &option);


		//PERSONA p = { 9, "Juan", 12, 1.40, 40};
		//PERSONA q;

		//escribir( p, fp);
		//leer(fp, 1);
		//imprimir( q );
		
		}

	return 0;

}


void escribir(PERSONA *p, FILE *fp, int id) {

	p->id = id;
	fseek(fp, p->id*sizeof(PERSONA), SEEK_SET);
	printf("Id: %i Nombre: %s Edad: %i Altura: %f Peso: %f\n", id, p->nombre, p->edad, p->altura, p->peso);
	fwrite(p, sizeof(PERSONA), 1, fp);

}
int cargar_todo( FILE *fp, PERSONA **records) {
	fseek(fp, 0, SEEK_SET);
	int records_count = 0;
	while (!feof(fp)) {
		records[records_count] = malloc(sizeof(PERSONA));
		if(fread(records[records_count], sizeof(PERSONA), 1, fp) > 0) {
			printf("Id: %i Nombre: %s Edad: %i Altura: %f Peso: %f\n", records[records_count]->id,  records[records_count]->nombre, records[records_count]->edad, records[records_count]->altura, records[records_count]->peso);
			records_count++;
		}
	}
	return records_count;
}
void imprimir(PERSONA *q ) {
}

void leer( FILE *fp, int id) {
	PERSONA p;
	fseek(fp, id*sizeof(PERSONA), SEEK_SET);
	fread(&p, sizeof(PERSONA), 1, fp);
	printf("Id: %i Nombre: %s Edad: %i Altura: %f Peso: %f\n", p.id, p.nombre, p.edad, p.altura, p.peso);
}


PERSONA* cargar_persona() {
        printf ("\033[2J");
        printf("\033[2;1f");
        printf(" ╔═════════════════════════════╗\n");
        printf(" ║  Ingresar Persona           ║\n");
        printf(" ╚═════════════════════════════╝\n");

        PERSONA *persona = malloc(sizeof(PERSONA));
	if(persona == NULL) {
		printf("Error alocando memoria!\n");
		exit(1);
	}

        printf(" Ingrese Nombre: ");
        scanf(" %s", persona->nombre);
        //fgets(str_tmp, MAX_BUFF_NAME, stdin);

        printf(" Ingrese edad: ");
        scanf(" %i", &persona->edad);

        printf(" Ingrese altura: ");
        scanf(" %f", &persona->altura);

        printf(" Ingrese peso: ");
        scanf(" %f", &persona->peso);

        return persona;

}

void buscar_nombre( PERSONA **records, char *nombre, int records_count) {

	for(int i=0;i<=records_count;i++)  {
		if(strstr(records[i]->nombre, nombre) != NULL)
		printf("Id: %i Nombre: %s Edad: %i Altura: %f Peso: %f\n", records[i]->id, records[i]->nombre, records[i]->edad, records[i]->altura, records[i]->peso);
	}
}

void modificar_registro(PERSONA **records, int id, FILE *fp) {

	PERSONA p = {};
	
        printf ("\033[2J");
        printf("\033[2;1f");
        printf(" ╔═════════════════════════════╗\n");
        printf(" ║  Modificar registro         ║\n");
        printf(" ╚═════════════════════════════╝\n");

        printf(" Nombre: %s\n", records[id]->nombre);
        printf(" Ingrese nombre nuevo (blanco para no cambiar): ");
        scanf(" %s", p.nombre);
	if(strlen(p.nombre) == 0)
		strcpy(p.nombre, records[id]->nombre);


        printf(" Edad: %i\n", records[id]->edad);
        printf(" Ingrese nueva edad: ");
        scanf(" %i", &p.edad);

        printf(" Altura: %f\n", records[id]->altura);
        printf(" Ingrese nueva altura: ");
        scanf(" %f", &p.altura);

        printf(" Peso: %f\n", records[id]->peso);
        printf(" Ingrese nuevo peso: ");
        scanf(" %f", &p.peso);

	escribir(&p, fp, id);
}
