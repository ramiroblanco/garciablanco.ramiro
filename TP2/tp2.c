/* 
 
  Escriba un programa capaz de almacenar hasta 100 registros de personas. cada registro debera estar compuesto por el nombre (sin limite de tamaño), su edad e informacion de su salud. Dicha informacion, la cual debe estar agrupada, consta de altura y peso.
para realizarlo, se propone trabajar haciendo un uso eficiente de la memoria; por lo que los registros de personas deberan estar inicializados a NULL. Al momento de cargar un registro, se deberan realizar las reservas de memoria necesarias, crear las estructuras que correspondan y enlazar los punteros.

se pide realizar las siguientes funciones:

void imprimir_persona (struct people*)
//imprime toda la informacion del registro recibido como parametro

struct people* cargar_persona(void)
//solicita informacion al usuario y devuelve un puntero a una estructura con toda la informacion

void buscar_nombre (struct people**)
//busca a un usuario por nombre e imprime toda su informacion, o "usuario no encontrado" si corresponde

void buscar_edad (struct people**)
//busca e imprime a todos los usuarios con esa edad

void liberar (struct people **)
//funcion que libera TODA la memoria pedida


el programa debe presentarle un menu interactivo al usuario, permitiendole escoger entre las distintas funcionalidades o finalizar el mismo
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RECORDS_MAX 1
#define MAX_BUFF_NAME 200

struct health {
	float height;
	float weight;
};
typedef struct health ST_HEALTH;

struct people {
	char *name;
	int age;
	ST_HEALTH *health;
};

typedef struct people ST_PEOPLE;

void imprimir_persona (ST_PEOPLE*);
struct people* cargar_persona(void);
void buscar_nombre (ST_PEOPLE**);
void buscar_edad (ST_PEOPLE **, int);
void liberar (ST_PEOPLE**);
void salir();

//Auxs
int get_arr_next_free(ST_PEOPLE **records);

int main() {

	int live = 1, option, i;
	char **search_name;

	ST_PEOPLE *records[RECORDS_MAX] = {NULL};

	while(live) {

		if(option > 0) {
			switch(option) {
				case 1:
					i = get_arr_next_free(records);
					if(i < 0) {
						printf(" ╔═════════════════════════════╗\n");
						printf(" ║  No has registros libres.   ║\n");
						printf(" ╚═════════════════════════════╝\n");
					}else {
					records[i] = cargar_persona();
						if(records[i] == NULL) {
							printf(" ╔═══════════════════════════════════╗\n");
							printf(" ║  No se pudo agregar el registro.  ║\n");
							printf(" ╚═══════════════════════════════════╝\n");
						}
					}

				break;
				case 2:
					
					scanf(" %s", &search_name);
					buscar_nombre(records);
				break;
				case 3:
					printf("Ingrese edad a buscar: ");
					scanf(" %d", &i);
					buscar_edad(records, i);
				break;
				case 4:
					liberar(records);
				break;
				case 5:
					salir();
				break;

			} 
			//scanf(" %i", &option);
			//Clear menu option
			option = 0;
		} else {
					printf ("\033[2J");
					printf("\033[2;5f");
					printf("\n");
					printf(" ╔═════════════════════════════╗\n");
					printf(" ║  MENU                       ║\n");
					printf(" ╟─────────────────────────────╢\n");
					printf(" ║  1) INGRESAR PERSONA        ║\n");
					printf(" ║  2) BUSCAR POR NOMBRE       ║\n");
					printf(" ║  3) BUSCAR POR EDAD         ║\n");
					printf(" ║  4) ELIMINAR PERSONA        ║\n");
					printf(" ║                             ║\n");
					printf(" ║                             ║\n");
					printf(" ║  5) Salir                   ║\n");
					printf(" ╚═════════════════════════════╝\n");
					printf("  ==> ");
					scanf(" %i", &option);
			}
	}
	return 0;
}

void imprimir_persona (ST_PEOPLE* people) {

        printf ("\033[2J");
	printf("\033[2;1f");
	printf(" Nombre: %s\n", people->name);
	printf(" Edad: %i\n\n", people->age);
	printf(" ╔═════════════════════════════╗\n");
	printf(" ║ FICHA MEDICA                ║\n");
	printf(" ╟─────────────────────────────╢\n");
	printf(" ║  Altura:                    ║\n");
	printf(" ║  Peso:                      ║\n");
	printf(" ╚═════════════════════════════╝\n");
	printf("\033[8;13f");
	printf(" %f", people->health->height);
	printf("\033[9;13f");
	printf(" %f", people->health->weight);
	printf("\033[13;1f");

}

ST_PEOPLE* cargar_persona() {
        printf ("\033[2J");
	printf("\033[2;1f");
	printf(" ╔═════════════════════════════╗\n");
	printf(" ║  Ingresar Persona           ║\n");
	printf(" ╚═════════════════════════════╝\n");

	ST_PEOPLE *people = malloc(sizeof(ST_PEOPLE));
	if(people == NULL) {
		printf("No ha sido posible alocar memoria para el registro.");
	}
	char str_tmp[MAX_BUFF_NAME] = {};

	printf(" Ingrese Nombre: ");
	scanf(" %s", str_tmp);
	//fgets(str_tmp, MAX_BUFF_NAME, stdin);
	people->name = malloc(strlen(str_tmp)*sizeof(char));
	if(people->name == NULL) {
		printf("No ha sido posible alocar memoria para el registro.");
		return NULL;
	}
	strcpy(people->name,str_tmp);

	printf(" Ingrese edad: ");
	scanf(" %s", &people->age);

	people->health = malloc(sizeof(ST_HEALTH));
	if(people->health == NULL) {
		printf("No ha sido posible alocar memoria para el registro.");
		return NULL;
	}

	printf(" Ingrese altura: ");
	scanf(" %f", &people->health->height);

	printf(" Ingrese peso: ");
	scanf(" %f", &people->health->weight);

	return people;		

}

void buscar_nombre (ST_PEOPLE **records) {

}

void buscar_edad (ST_PEOPLE **records, int age) {
	int i;
	for(i = 0; i < RECORDS_MAX;i++) {
		if(records[i] == NULL) continue;
		if(records[i]->age == age) {
			printf(" %i. %s %i\n",i, records[i]->name,records[i]->age);
		}
	}
	if(i < 1) {
		printf(" ╔═════════════════════════════════════════╗\n");
		printf(" ║  No hay registros con la edad buscada.  ║\n");
		printf(" ╚═════════════════════════════════════════╝\n");
	} else {
		printf("Ingrese el id del registro a mostrar (): ");	
		scanf(" %d", &i);
		imprimir_persona(records[i]);
	}
}

void liberar (ST_PEOPLE **records) {
	int i;
	for(i = 0; i < RECORDS_MAX;i++) {
		printf(" %i. %s %i\n",i, records[i]->name,records[i]->age);
	}
	if(i < 1) {
		printf(" ╔═════════════════════╗\n");
		printf(" ║  No hay registros.  ║\n");
		printf(" ╚═════════════════════╝\n");
	} else {
		printf("Ingrese el id del registro a borrar (): ");	
		scanf(" %d", &i);
		imprimir_persona(records[i]);
		free(records[i]->health);
		free(records[i]);
		records[i] = NULL;
	}
}

void salir() {
        printf ( "\033[2J");
        printf("\033[2;5f");
	printf("\n");
        printf(" ╔════════════════════╗\n");
        printf(" ║    ┌─┐┬ ┬┌─┐┬ ┬    ║\n");
        printf(" ║    │  ├─┤├─┤│ │    ║\n");
        printf(" ║    └─┘┴ ┴┴ ┴└─┘    ║\n");
        printf(" ╚════════════════════╝\n");
	printf("\n");
	printf("\n");
	exit(0);
}

int get_arr_next_free(ST_PEOPLE **records) {
	for(int i = 0; i < RECORDS_MAX;i++) {
		if(records[i] == NULL)
			return i;
	}
	return -1;
}
